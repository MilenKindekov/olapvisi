﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Base class for all ViewModel classes displayed by TreeViewItems.  
    /// This acts as an adapter between a raw data object and a TreeViewItem.
    /// </summary>
    public class TreeViewItemViewModel : INotifyPropertyChanged
    {
        #region Data

        private static readonly TreeViewItemViewModel DummyChild = new TreeViewItemViewModel();

        private readonly TreeViewItemViewModel _parent;
        public ObservableCollection<TreeViewItemViewModel> _children;

        private bool _isExpanded;
        private bool _isSelected;
        private IEnumerator<TreeViewItemViewModel> _matchingItemsEnumerator;

        #endregion // Data

        #region Constructors

        protected TreeViewItemViewModel(TreeViewItemViewModel parent, bool lazyLoadChildren)
        {
            _parent = parent;

            _children = new ObservableCollection<TreeViewItemViewModel>();

            if (lazyLoadChildren)
                _children.Add(DummyChild);
        }

        // This is used to create the DummyChild instance.
        private TreeViewItemViewModel()
        {
        }

        #endregion // Constructors

        #region Children

        /// <summary>
        /// Returns the logical child items of this object.
        /// </summary>
        public ObservableCollection<TreeViewItemViewModel> Children
        {
            get { return _children; }
            set
            {
                _children = value;
                OnPropertyChanged("Children");
            }
        }

        #endregion // Children

        #region IsExpanded

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }

                // Expand all the way up to the root.
                if (_isExpanded && _parent != null)
                    _parent.IsExpanded = true;

                VerifyChildren();
            }
        }

        #endregion // IsExpanded

        #region IsSelected

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is selected.
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        #endregion // IsSelected

        #region LoadChildren

        /// <summary>
        /// Invoked when the child items need to be loaded on demand.
        /// Subclasses can override this to populate the Children collection.
        /// </summary>
        protected virtual void LoadChildren()
        {
        }

        #endregion // LoadChildren

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public void UnloadAllChildren()
        {
            _children.Clear();
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #region UnloadChild

        /// <summary>
        /// Invoked when a specific child needs to be removed.
        /// </summary>
        public void UnloadChild(string childName)
        {
            TreeViewItemViewModel childToUnload = null;

            foreach (TreeViewItemViewModel child in Children)
            {
                if (child.GetDisplayText() == childName)
                {
                    childToUnload = child;
                    break;
                }
            }

            if (childToUnload != null)
            {
                Children.Remove(childToUnload);
            }
        }

        #endregion // UnloadChild

        #region Parent

        public TreeViewItemViewModel Parent
        {
            get { return _parent; }
        }

        #endregion // Parent

        #region VerifyChildren

        protected internal void VerifyChildren()
        {
            // Lazy load the child items, if necessary.
            if (Children.Contains(DummyChild))
            {
                Children.Remove(DummyChild);
                LoadChildren();
            }
        }

        #endregion // VerifyChildren

        #region Search Logic

        private bool USE_CUSTOM_ENUMERATOR = true;

        public void PerformSearch()
        {
            if (_matchingItemsEnumerator == null || !_matchingItemsEnumerator.MoveNext())
                VerifySearchEnumerator();

            TreeViewItemViewModel item = _matchingItemsEnumerator.Current;

            if (item == null)
                return;

            // Ensure that this item is in view.
            if (item.Parent != null)
                item.Parent.IsExpanded = true;

            item.IsSelected = true;
        }

        private void VerifySearchEnumerator()
        {
            string searchText = GetSearchText();

            IEnumerable<TreeViewItemViewModel> matches;
            if (USE_CUSTOM_ENUMERATOR)
                matches = FindMatchesCustomEnumerator(searchText);
            else
                matches = FindMatchesYield(searchText, this);

            _matchingItemsEnumerator = matches.GetEnumerator();

            if (!_matchingItemsEnumerator.MoveNext())
            {
                MessageBox.Show(
                    "No matching items were found.",
                    "Try Again",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information
                    );
            }
        }

        private IEnumerable<TreeViewItemViewModel> FindMatchesYield(
            string searchText, TreeViewItemViewModel current)
        {
            if (current.DisplayTextContainsSearchText(searchText))
                yield return current;

            current.VerifyChildren();

            foreach (TreeViewItemViewModel child in current.Children)
                foreach (TreeViewItemViewModel match in FindMatchesYield(searchText, child))
                    yield return match;
        }

        private IEnumerable<TreeViewItemViewModel> FindMatchesCustomEnumerator(string searchText)
        {
            return TreeViewItemViewModelSearch.Search(
                (item) => item.DisplayTextContainsSearchText(searchText),
                this);
        }

        private bool DisplayTextContainsSearchText(string text)
        {
            string displayText = GetDisplayText();

            if (String.IsNullOrEmpty(text) || String.IsNullOrEmpty(displayText))
                return false;

            return displayText.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) > -1;
        }

        /// <summary>
        /// Subclasses override this method to return their display text.
        /// This is used when searching the tree.
        /// </summary>
        protected virtual string GetDisplayText()
        {
            return String.Empty;
        }

        /// <summary>
        /// Subclasses can override this to return the text used
        /// to search the items.  
        /// </summary>
        protected virtual string GetSearchText()
        {
            return String.Empty;
        }

        /// <summary>
        /// Subclasses call this when a new search string is provided by the user.
        /// </summary>
        protected void ResetSearch()
        {
            _matchingItemsEnumerator = null;
        }

        #endregion // Search Logic
    }
}