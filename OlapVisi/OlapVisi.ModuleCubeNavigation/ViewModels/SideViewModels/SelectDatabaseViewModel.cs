﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.InteractionRequests;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// View model of the popup window used to select a database at the specified server address.
    /// </summary>
    public class SelectDatabaseViewModel : NotificationObject, IPopupWindowActionAware, IRegionManagerAware
    {
        private readonly EventAggregator eventAggregator;

        public SelectDatabaseViewModel()
        {
            // Change to retrieve connection selected from ServerConnectionView
            IList<ConnectionInfo> databases = ConnectionHelper.GetDatabasesConnectionList();

            Databases = new ObservableCollection<ConnectionInfoViewModel>();
            foreach (ConnectionInfo database in databases)
            {
                Databases.Add(new ConnectionInfoViewModel(database));
            }

            OkCommand = new DelegateCommand(OkAction);
            CancelCommand = new DelegateCommand(CancelAction);

            RaisePropertyChanged(String.Empty);

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
        }

        public ObservableCollection<ConnectionInfoViewModel> Databases { get; set; }
        public ObservableCollection<ConnectionInfoViewModel> SelectedDatabases { get; set; }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        // IPopupWindowActionAware implementation

        #region IPopupWindowActionAware Members

        public Window HostWindow { get; set; }
        public Notification HostNotification { get; set; }

        #endregion

        // IRegionManagerAware implementation

        #region IRegionManagerAware Members

        public IRegionManager RegionManager { get; set; }

        #endregion

        protected void OkAction()
        {
            PopulateSelectedDatabases();
            eventAggregator.GetEvent<DatabaseSelectedEvent>().Publish(SelectedDatabases);

            if (HostWindow != null)
            {
                HostWindow.Close();
            }
        }

        protected void CancelAction()
        {
            eventAggregator.GetEvent<DatabaseSelectedEvent>().Publish(null);

            if (HostWindow != null)
            {
                HostWindow.Close();
            }
        }

        private void PopulateSelectedDatabases()
        {
            SelectedDatabases = new ObservableCollection<ConnectionInfoViewModel>();
            foreach (ConnectionInfoViewModel database in Databases)
            {
                if (database.IsChecked)
                {
                    SelectedDatabases.Add(database);
                }
            }
        }
    }
}