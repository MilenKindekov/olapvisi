﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.InteractionRequests;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// View model for the popup window used to select a cube to browse.
    /// </summary>
    public class SelectCubeViewModel : NotificationObject, IPopupWindowActionAware, IRegionManagerAware
    {
        private readonly EventAggregator eventAggregator;
        private ObservableCollection<ConnectionInfoViewModel> databases;
        private CubeInfoViewModel selectedItem;

        public SelectCubeViewModel()
        {
            OkCommand = new DelegateCommand(OkAction);
            CancelCommand = new DelegateCommand(CancelAction);

            //RaisePropertyChanged(String.Empty);

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<DatabaseChangedEvent>().Subscribe(DatabasesChanged);
        }

        public ObservableCollection<ConnectionInfoViewModel> Databases
        {
            get { return databases; }
            set
            {
                databases = value;
                RaisePropertyChanged(() => Databases);
            }
        }

        public CubeInfoViewModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = null;
                RaisePropertyChanged(() => SelectedItem);

                selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        // IPopupWindowActionAware implementation

        #region IPopupWindowActionAware Members

        public Window HostWindow { get; set; }
        public Notification HostNotification { get; set; }

        #endregion

        // IRegionManagerAware implementation

        #region IRegionManagerAware Members

        public IRegionManager RegionManager { get; set; }

        #endregion

        protected void OkAction()
        {
            if(SelectedItem == null) CancelAction();

            eventAggregator.GetEvent<CubeSelectedEvent>().Publish(SelectedItem);
            if (HostWindow != null)
            {
                HostWindow.Close();
            }
        }

        protected void CancelAction()
        {
            if (HostWindow != null)
            {
                HostWindow.Close();
            }
        }

        // Populate the database collection with all selected databases.
        public void DatabasesChanged(ObservableCollection<ConnectionInfoViewModel> SelectedDatabases)
        {
            if (SelectedDatabases != null)
            {
                Databases = SelectedDatabases;
            }
        }
    }
}