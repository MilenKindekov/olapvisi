﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.InteractionRequests;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using OlapVisi.Infrastructure.Tools;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// View model of the popup window used to browse the members of a specific level.
    /// </summary>
    public class MemberBrowseSideHierarchyViewModel : NotificationObject, IPopupWindowActionAware, IRegionManagerAware
    {

        private readonly EventAggregator eventAggregator;
        private bool isOpen;

        public ICommand CloseCommand { get; private set; }

        public MemberBrowseSideHierarchyViewModel()
        {
            CloseCommand = new DelegateCommand(CloseAction);

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<MemberBrowseEvent>().Subscribe(GetMembers);

            isOpen = false;
        }

        protected void CloseAction()
        {
            if (HostWindow != null)
            {
                MemberNames = null;
                isOpen = false;
                SearchText = null;
                HostWindow.Close();
            }
        }

        private string searchText;
        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                RaisePropertyChanged(() => SearchText);
                RaisePropertyChanged(() => MemberNames);
            }
        }

        private IEnumerable<string> memberNames;
        public IEnumerable<string> MemberNames
        {
            get
            {
                if (SearchText == null) return memberNames;
                if (memberNames != null) return memberNames.Where(x => x.ToUpper().Contains(SearchText.ToUpper()));
                return null;
            }
            set
            {
                if (this.memberNames != value)
                {
                    this.memberNames = value;
                    RaisePropertyChanged(() => MemberNames);
                }            
            }
        }

        private void GetMembers(Tuple<string,string,ConnectionInfo> membersCase)
        {
            if(!isOpen)
            {
                List<string> collection = new List<string>();
                var members = MemberLoaderCache.Instance.GetMemberCollection(membersCase.Item1, membersCase.Item2, membersCase.Item3);

                foreach (var member in members)
                {
                    collection.Add(member.Item1);
                }

                MemberNames = collection;
                isOpen = true;
            }         
        }


        // IPopupWindowActionAware implementation

        #region IPopupWindowActionAware Members

        public Window HostWindow { get; set; }
        public Notification HostNotification { get; set; }

        #endregion

        // IRegionManagerAware implementation

        #region IRegionManagerAware Members

        public IRegionManager RegionManager { get; set; }

        #endregion
    }
}