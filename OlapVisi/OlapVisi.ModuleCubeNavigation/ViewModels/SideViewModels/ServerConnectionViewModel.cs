﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using OlapVisi.ModuleCubeNavigation.Interfaces;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Main view model for the server connection toolbar to the side of the UI.
    /// </summary>
    public class ServerConnectionViewModel : NotificationObject, ISideViewModel
    {
        private readonly EventAggregator eventAggregator;
        private ObservableCollection<ConnectionInfoViewModel> databaseCollection;
        private string name;
        private string toolTip;

        public ServerConnectionViewModel()
        {
            Name = "Database Connection";
            ToolTip =
                "Add a database connection from the configured server instance. This will be used to pull OLAP cubes for analysis.";

            RaiseSelectDatabase = new DelegateCommand(OnRaiseSelectDatabase);
            SelectDatabaseRequest = new InteractionRequest<Notification>();

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<DatabaseSelectedEvent>().Subscribe(DatabasesSelected);
        }

        public InteractionRequest<Notification> SelectDatabaseRequest { get; private set; }
        public ICommand RaiseSelectDatabase { get; private set; }

        public ObservableCollection<ConnectionInfoViewModel> DatabaseCollection
        {
            get { return databaseCollection; }
            set
            {
                databaseCollection = value;
                RaisePropertyChanged(() => DatabaseCollection);
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public string ToolTip
        {
            get { return toolTip; }
            set
            {
                toolTip = value;
                RaisePropertyChanged(() => ToolTip);
            }
        }

        public void DatabasesSelected(ObservableCollection<ConnectionInfoViewModel> SelectedDatabases)
        {
            if (SelectedDatabases != null)
            {
                DatabaseCollection = SelectedDatabases;
                eventAggregator.GetEvent<DatabaseChangedEvent>().Publish(DatabaseCollection);
            }
        }

        // When button is clicked, popup child view is initialised.
        private void OnRaiseSelectDatabase()
        {
            SelectDatabaseRequest.Raise(
                new Notification {Title = "Server Databases"});
        }
    }
}