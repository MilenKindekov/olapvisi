﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using OlapVisi.ModuleCubeNavigation.Interfaces;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// The main view model for the side cube browser toolbar.
    /// </summary>
    public class CubeHierarchyViewModel : TreeViewItemViewModel, ISideViewModel
    {
        private readonly Dictionary<CubeInfoViewModel, CubeViewModel> CubeTreeViewCache;

        private readonly SearchCubeCommand _searchCommand;
        private readonly EventAggregator eventAggregator;
        private readonly string toolTip = "Hierarchical view of a selected OLAP cube.";
        private readonly string viewName = "OLAP Cubes";
        private string _searchText = String.Empty;
        public CubeInfoViewModel currentCube;
        public ObservableCollection<ConnectionInfoViewModel> CurrentDatabases { get; set; }

        public CubeHierarchyViewModel() :
            base(null, true)
        {
            RaiseSelectCube = new DelegateCommand(OnRaiseSelectCube);
            SelectCubeRequest = new InteractionRequest<Notification>();

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<DatabaseChangedEvent>().Subscribe(DatabasesChanged);
            eventAggregator.GetEvent<CubeSelectedEvent>().Subscribe(CubeSelected);

            base.Children.RemoveAt(0);

            // If cache is enabled.
            CubeTreeViewCache = new Dictionary<CubeInfoViewModel, CubeViewModel>();

            _searchCommand = new SearchCubeCommand(this);
        }

        public InteractionRequest<Notification> SelectCubeRequest { get; private set; }
        public ICommand RaiseSelectCube { get; private set; }

        public string ViewName
        {
            get { return viewName; }
        }

        public string ToolTip
        {
            get { return toolTip; }
        }

        #region Properties

        #region SearchCommand

        /// <summary>
        /// Returns the command used to execute a search in the places in the cube.
        /// </summary>
        public ICommand SearchCommand
        {
            get { return _searchCommand; }
        }

        private class SearchCubeCommand : ICommand
        {
            private readonly CubeHierarchyViewModel _cubeHierarchy;

            public SearchCubeCommand(CubeHierarchyViewModel cubeHierarchy)
            {
                _cubeHierarchy = cubeHierarchy;
            }

            #region ICommand Members

            public bool CanExecute(object parameter)
            {
                return true;
            }

            event EventHandler ICommand.CanExecuteChanged
            {
                // I intentionally left these empty because
                // this command never raises the event, and
                // not using the WeakEvent pattern here can
                // cause memory leaks.  WeakEvent pattern is
                // not simple to implement, so why bother.
                add { }
                remove { }
            }

            public void Execute(object parameter)
            {
                _cubeHierarchy.PerformSearch();
            }

            #endregion
        }

        #endregion // SearchCommand

        #region SearchText

        /// <summary>
        /// Gets/sets a fragment of the name to search for.
        /// </summary>
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (value == _searchText)
                    return;

                _searchText = value;

                base.ResetSearch();
            }
        }

        #endregion // SearchText

        #endregion // Properties

        #region Methods

        // Populate the database collection with all selected databases.
        public void DatabasesChanged(ObservableCollection<ConnectionInfoViewModel> SelectedDatabases)
        {
            if (SelectedDatabases != null)
            {
                CurrentDatabases = SelectedDatabases;
                if (currentCube != null && !CurrentDatabases.Contains(currentCube.ParentConnection))
                {
                    base.Children.RemoveAt(0);
                }
            }
        }

        // Add selected cube to child collection.
        public void CubeSelected(CubeInfoViewModel SelectedCube)
        {
            if (SelectedCube != null)
            {
                CubeSelectionCacheSingleton.Instance.SelectedCube = SelectedCube;

                if (base.Children.Count > 0)
                {
                    CubeTreeViewCache[currentCube] = (CubeViewModel) base.Children[0];
                    base.Children.RemoveAt(0);
                }

                currentCube = SelectedCube;

                if (CubeTreeViewCache.ContainsKey(currentCube))
                {
                    CubeViewModel cubeModel;
                    CubeTreeViewCache.TryGetValue(currentCube, out cubeModel);
                    base.Children.Add(cubeModel);
                }
                else
                {
                    var olapMetadataProvider = new OlapMetadataProvider(currentCube.Connection);
                    currentCube.CubeModel = olapMetadataProvider.GetCubeMetadata(currentCube.CubeName,
                                                                                 MetadataQueryType.GetCubeMetadata);

                    base.Children.Add(new CubeViewModel(SelectedCube.CubeModel, this));
                    CubeTreeViewCache.Add(currentCube, (CubeViewModel) base.Children[0]);
                }
            }
        }

        // When button is clicked, popup child view is initialised.
        private void OnRaiseSelectCube()
        {
            SelectCubeRequest.Raise(new Notification {Title = "Cubes"});
        }

        protected override string GetSearchText()
        {
            return SearchText;
        }

        #endregion // Methods
    }
}