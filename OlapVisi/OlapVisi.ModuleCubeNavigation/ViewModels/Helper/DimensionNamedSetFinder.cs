﻿using OlapVisi.DataLayer.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Helper class, named sets of a specified dimension.
    /// </summary>
    public static class DimensionNamedSetFinder
    {
        public static List<NamedSetInfo> FindNamedSetsForDimension(string dimension, List<NamedSetInfo> sets)
        {
            var namedSets = new List<NamedSetInfo>();
            foreach (var set in sets)
            {
                var name = GetDimension(set.GetPropertyValue("DIMENSIONS").ToString());
                if (name == dimension)
                {
                    namedSets.Add(set);
                }
            }

            if (namedSets.Count > 0) return namedSets;
            
            return null;
        }

        private static string GetDimension(string property)
        {
            var dimension = property.Split('.').First();
            return Regex.Replace(dimension, @"[^\w]", String.Empty);
        }
    }
}
