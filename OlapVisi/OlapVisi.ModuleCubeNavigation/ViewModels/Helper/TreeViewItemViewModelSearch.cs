﻿using System;
using System.Collections.Generic;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    // NOTE: This class only exists for the sake of seeing an
    // alternate way to implement a deferred enumerator.  The
    // enumerator created by the C# 'yield' keyword is automatically
    // deferred.  

    /// <summary>
    /// Provides a generic means of searching a tree of 
    /// TreeViewItemViewModel objects.  The search logic
    /// works with TreeViewItemViewModel objects that 
    /// lazy-load their child items, and only forces that
    /// the minimum number of items are loaded for a 
    /// search to complete.
    /// </summary>
    public static class TreeViewItemViewModelSearch
    {
        #region PerformSearch

        public static IEnumerable<TreeViewItemViewModel> Search(
            Predicate<TreeViewItemViewModel> predicate,
            TreeViewItemViewModel initialItem)
        {
            SearchContext context =
                new SearchContext(predicate, initialItem);

            return new SearchResult(context);
        }

        #endregion // PerformSearch

        #region SearchResult [nested class]

        private class SearchResult : IEnumerable<TreeViewItemViewModel>
        {
            #region Data

            readonly SearchContext _prototype;

            #endregion // Data

            #region Constructor

            public SearchResult(SearchContext prototype)
            {
                _prototype = prototype;
            }

            #endregion // Constructor

            #region IEnumerable<TreeViewItemViewModel> Members

            public IEnumerator<TreeViewItemViewModel> GetEnumerator()
            {
                SearchContext context = new SearchContext(_prototype.Predicate, _prototype.Current);
                return new SearchEnumerator(context);
            }

            #endregion

            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            #endregion
        }

        #endregion // SearchResult [nested class]

        #region SearchEnumerator [nested class]

        private class SearchEnumerator : IEnumerator<TreeViewItemViewModel>
        {
            #region Data

            SearchContext _context;

            #endregion // Data

            #region Constructor

            public SearchEnumerator(SearchContext context)
            {
                _context = context;
            }

            #endregion // Constructor

            #region IEnumerator<TreeViewItemViewModel> Members

            public TreeViewItemViewModel Current
            {
                get { return _context.MatchingItem; }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                _context = null;
            }

            #endregion

            #region IEnumerator Members

            object System.Collections.IEnumerator.Current
            {
                get { return this.Current; }
            }

            public bool MoveNext()
            {
                this.LocateNextMatchingItem();
                return _context.MatchingItem != null;
            }

            public void Reset()
            {
                throw new NotImplementedException("Reset not supported");
            }

            #endregion

            #region Search Logic

            void LocateNextMatchingItem()
            {
                bool foundMatch =
                    _context.MatchingItem == null &&
                    _context.Predicate(_context.Current);

                if (foundMatch)
                {
                    _context.MatchingItem = _context.Current;
                }
                else
                {
                    _context.MatchingItem = null;
                    this.VisitDescendants();
                    this.VisitSiblingsAndAncestors();
                }
            }

            void VisitDescendants()
            {
                _context.Current.VerifyChildren();
                var children = _context.Current.Children;
                if (0 < children.Count)
                {
                    ParentInfo pi = new ParentInfo(_context.Current);
                    _context.ParentInfoStack.Push(pi);

                    for (
                        int i = 0;
                        i < children.Count && _context.MatchingItem == null;
                        ++i)
                    {
                        _context.Current = children[i];
                        this.LocateNextMatchingItem();
                    }
                }
            }

            void VisitSiblingsAndAncestors()
            {
                while (
                     0 < _context.ParentInfoStack.Count &&
                    _context.MatchingItem == null)
                {
                    ParentInfo pi = _context.ParentInfoStack.Peek();
                    var children = pi.Parent.Children;
                    if (++pi.ChildIndex < children.Count)
                    {
                        _context.Current = children[pi.ChildIndex];
                        this.LocateNextMatchingItem();
                    }
                    else
                    {
                        _context.ParentInfoStack.Pop();
                    }
                }
            }

            #endregion // Search Logic
        }

        #endregion // [nested class]

        #region SearchContext [nested class]

        private class SearchContext
        {
            public TreeViewItemViewModel Current;
            public TreeViewItemViewModel MatchingItem;
            public readonly Stack<ParentInfo> ParentInfoStack;
            public readonly Predicate<TreeViewItemViewModel> Predicate;

            public SearchContext(
                Predicate<TreeViewItemViewModel> predicate,
                TreeViewItemViewModel initialItem)
            {
                this.Predicate = predicate;
                this.Current = initialItem;
                this.ParentInfoStack = new Stack<ParentInfo>();
            }
        }

        #endregion // SearchContext [nested class]

        #region ParentInfo [nested class]

        private class ParentInfo
        {
            public int ChildIndex;
            public readonly TreeViewItemViewModel Parent;

            public ParentInfo(TreeViewItemViewModel parent)
            {
                Parent = parent;
            }
        }

        #endregion // ParentInfo [nested class]
    }
}
