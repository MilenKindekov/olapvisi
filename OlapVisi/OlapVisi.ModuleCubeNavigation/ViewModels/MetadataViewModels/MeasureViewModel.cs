﻿using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Wrapper view model class for the measure info object model.
    /// </summary>
    public class MeasureViewModel : TreeViewItemViewModel
    {
        private readonly MeasureInfo measure;

        /// <summary>
        /// Receives a measure model and a reference to the parent view model.
        /// Lowest level of hierarchy so lazy load set to false.
        /// </summary>
        /// <param name="measure"></param>
        /// <param name="model"></param>
        public MeasureViewModel(MeasureInfo measure, TreeViewItemViewModel model)
            : base(model, false)
        {
            this.measure = measure;
            RaiseSelectCurrentMeasure = new DelegateCommand(SelectCurrentMeasureAction);
            RaiseSelectCurrent3DMeasure = new DelegateCommand(SelectCurrent3DMeasureAction);
        }

        public ICommand RaiseSelectCurrentMeasure { get; private set; }
        public ICommand RaiseSelectCurrent3DMeasure { get; private set; }

        public string MeasureName
        {
            get { return measure.Name; }
        }

        protected override void LoadChildren()
        {
        }

        protected override string GetDisplayText()
        {
            return MeasureName;
        }

        /// <summary>
        /// Method which adds the measure model to the 2D selection cache. An event is published to notify of this addition.
        /// </summary>
        private void SelectCurrentMeasureAction()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            if (cache.AddMeasureInfo(this.measure)) return;

            EventAggregator eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<MeasureSelectedEvent>().Publish(true);
        }

        /// <summary>
        /// Method which adds the measure model to the 3D selection cache. An event is published to notify of this addition.
        /// </summary>
        private void SelectCurrent3DMeasureAction()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            if (cache.Add3DMeasureInfo(this.measure)) return;

            EventAggregator eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<Measure3DSelectedEvent>().Publish(true);
        }
    }
}