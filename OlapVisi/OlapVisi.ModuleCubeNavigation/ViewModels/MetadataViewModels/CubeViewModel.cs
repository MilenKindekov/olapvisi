﻿using System.Collections.Generic;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleCubeNavigation.Interfaces;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Metadata view model to be displayed in the cube browser window. Part of the cube browser hierarchy.
    /// </summary>
    public class CubeViewModel : TreeViewItemViewModel, IHierarchyItemViewModel
    {
        private readonly CubeDefInfo cubeModel;
        private readonly string cubeName;

        /// <summary>
        /// Constructor receives a reference to the cube definition model and a reference to its parent TreeViewItemViewModel.
        /// Lazy load is set to true.
        /// </summary>
        /// <param name="cube"></param>
        /// <param name="model"></param>
        public CubeViewModel(CubeDefInfo cube, TreeViewItemViewModel model) :
            base(model, true)
        {
            cubeModel = cube;
            cubeName = cube.Name;
        }

        public string CubeName
        {
            get { return cubeName; }
        }

        /// <summary>
        /// Method used to load all child view models, since the specific level has been expanded.
        /// </summary>
        protected override void LoadChildren()
        {
            // Each cube has a measure combo group at the top.
            base.Children.Add(new MeasureGroupComboViewModel(cubeModel.MeasureGroups, this));

            // 3 parent nodes for Dimensions, Measures and KPIs
            var olapNamesList = (List<string>) OlapNames.GetOlapNamesAsList();
            foreach (string olapName in olapNamesList)
            {
                base.Children.Add(new ParentNodeViewModel(olapName, this, cubeModel));
            }
        }

        protected override string GetDisplayText()
        {
            return CubeName;
        }
    }
}