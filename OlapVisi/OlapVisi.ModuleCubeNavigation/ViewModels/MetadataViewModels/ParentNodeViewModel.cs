﻿using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// View model to server as a logical seperation in the UI for the different categories in the OLAP cube.
    /// </summary>
    public class ParentNodeViewModel : TreeViewItemViewModel
    {
        private string name;
        private CubeDefInfo cubeModel;
        private bool childrenLoaded;
        private EventAggregator eventAggregator;
        private bool subscriptionStatus;

        /// <summary>
        /// Receives a category name, a reference to the parent cube view model.
        /// Sets up subscription events to handle changes in cube selection and selection of measure groups.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="model"></param>
        /// <param name="cubeModel"></param>
        public ParentNodeViewModel(string name, TreeViewItemViewModel model, CubeDefInfo cubeModel)
            : base(model, true)
        {
            this.name = name;
            this.cubeModel = cubeModel;
            this.childrenLoaded = false;
            this.eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            this.eventAggregator.GetEvent<CubeSelectedEvent>().Subscribe(this.CubeSelected);
            this.eventAggregator.GetEvent<MeasureGroupSelectedEvent>().Subscribe(this.MeasureGroupSelected);
            this.subscriptionStatus = true;
        }

        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Event which handles cube changes. When a cube changes, the selected measure group needs to be selected or deselected.
        /// </summary>
        /// <param name="cube"></param>
        public void CubeSelected(CubeInfoViewModel cube)
        {
            if(cube != null)
            {
                if (cubeModel != cube.CubeModel)
                {
                    if (subscriptionStatus)
                    {
                        this.eventAggregator.GetEvent<MeasureGroupSelectedEvent>().Unsubscribe(this.MeasureGroupSelected);
                        subscriptionStatus = false;
                    }
                }
                else
                {
                    if (!subscriptionStatus)
                    {
                        this.eventAggregator.GetEvent<MeasureGroupSelectedEvent>().Subscribe(this.MeasureGroupSelected);
                        subscriptionStatus = true;
                    }
                }
            }     
        }

        /// <summary>
        /// On a measure group selection, event handler refreshes the children of the tree cube browser.
        /// </summary>
        /// <param name="mg"></param>
        public void MeasureGroupSelected(MeasureGroupInfo mg)
        {
            if (childrenLoaded)
            {
                this.UnloadAllChildren();
                childrenLoaded = false;
            }

            // The <All> parameter has been passed, load normally.
            if (mg == null)
            {
                this.LoadChildren();
                return;
            }

            if (mg.CubeName == cubeModel.Name)
            {
                switch (this.name)
                {
                    case (OlapNames.Dimensions):
                    {
                        List<DimensionInfo> dimensionList = this.cubeModel.Dimensions;
                        foreach (DimensionInfo dimension in dimensionList)
                        {
                            if(mg.Dimensions.Contains("[" + dimension.Name + "]"))
                            {
                                base.Children.Add(new DimensionViewModel(dimension, this, cubeModel.NamedSets));
                            }               
                        }
                        break;
                    }
                    case (OlapNames.Measures):
                    {
                        List<MeasureGroupInfo> measureGroupList = this.cubeModel.MeasureGroups;
                        foreach (MeasureGroupInfo measureGroup in measureGroupList)
                        {
                            if (mg.Name == measureGroup.Name)
                            {
                                base.Children.Add(new MeasureGroupViewModel(measureGroup, this.cubeModel.Measures, this));
                            }
                        }
                        break;
                    }
                    case (OlapNames.Kpis):
                    {
                        List<KpiInfo> kpiList = this.cubeModel.Kpis;
                        foreach (KpiInfo kpi in kpiList)
                        {
                            if (mg.Kpis.Contains(kpi.Name))
                            {
                                base.Children.Add(new KpiViewModel(kpi, this));
                            }
                        }
                        break;
                    }
                }

                childrenLoaded = true;
            }
        }

        /// <summary>
        /// The children for the type of parent node are added respectively.
        /// </summary>
        protected override void LoadChildren()
        {
            if (!childrenLoaded)
            {
                switch (this.name)
                {
                    case (OlapNames.Dimensions):
                        {
                            List<DimensionInfo> dimensionList = this.cubeModel.Dimensions;
                            foreach (DimensionInfo dimension in dimensionList)
                            {
                                base.Children.Add(new DimensionViewModel(dimension, this, cubeModel.NamedSets));
                            }
                            break;
                        }
                    case (OlapNames.Measures):
                        {
                            List<MeasureGroupInfo> measureGroupList = this.cubeModel.MeasureGroups;
                            foreach (MeasureGroupInfo measureGroup in measureGroupList)
                            {
                                base.Children.Add(new MeasureGroupViewModel(measureGroup, this.cubeModel.Measures, this));
                            }
                            break;
                        }
                    case (OlapNames.Kpis):
                        {
                            List<KpiInfo> kpiList = this.cubeModel.Kpis;
                            foreach (KpiInfo kpi in kpiList)
                            {
                                base.Children.Add(new KpiViewModel(kpi, this));
                            }
                            break;
                        }
                    //case (OlapNames.NamedSets):
                    //    {
                    //        List<NamedSetInfo> namedSetList = this.cubeModel.NamedSets;
                    //        foreach (NamedSetInfo namedSet in namedSetList)
                    //        {
                    //            base.Children.Add(new NamedSetViewModel(namedSet, this));
                    //        }
                    //        break;
                    //    }
                }
                childrenLoaded = true;
            }
        }

        protected override string GetDisplayText()
        {
            return this.Name;
        }
    }
}
