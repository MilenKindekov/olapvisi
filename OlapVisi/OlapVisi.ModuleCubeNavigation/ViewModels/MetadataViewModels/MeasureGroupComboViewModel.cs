﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Events;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Class that represents the drop-down menu of the Cube Browser - used to select an appropriate measure group that refreshes the data
    /// seen in the tree hierarchy.
    /// </summary>
    public class MeasureGroupComboViewModel : TreeViewItemViewModel
    {
        private readonly EventAggregator eventAggregator;
        private readonly Dictionary<string, MeasureGroupInfo> measureGroupDict;
        private readonly ObservableCollection<string> measureGroupList;
        private string selectedGroup;

        /// <summary>
        /// Recieves a list of measure group models to process and a reference to the parent view model (the cube view model).
        /// Lazy load is set to false as there are no children available since the form of a drop-down menu is taken.
        /// </summary>
        /// <param name="measureGroups"></param>
        /// <param name="model"></param>
        public MeasureGroupComboViewModel(IEnumerable<MeasureGroupInfo> measureGroups, TreeViewItemViewModel model)
            : base(model, false)
        {
            // A dictionary to hold all unique measure groups and their names.
            measureGroupDict = new Dictionary<string, MeasureGroupInfo>();
            measureGroupDict.Add("<All>", null);
            foreach (MeasureGroupInfo measure in measureGroups)
            {
                measureGroupDict.Add(measure.Name, measure);
            }

            measureGroupList = new ObservableCollection<string>(measureGroupDict.Keys);
            selectedGroup = "<All>";

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
        }

        public ObservableCollection<string> MeasureGroupNames
        {
            get { return measureGroupList; }
        }

        /// <summary>
        /// On selection, the selected group model is published as an event. Consumed in the ParentNodeViewModel in order to refresh 
        /// </summary>
        public string SelectedGroup
        {
            get { return selectedGroup; }
            set
            {
                selectedGroup = value;
                OnPropertyChanged(selectedGroup);
                eventAggregator.GetEvent<MeasureGroupSelectedEvent>().Publish(measureGroupDict[selectedGroup]);
            }
        }

        protected override void LoadChildren()
        {
        }
    }
}