﻿using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Class that serves as a view model wrapper of the LevelInfo model objects. Stores a single child, which represents a view model summary
    /// of the members of the specific level in the cube.
    /// </summary>
    public class LevelViewModel : TreeViewItemViewModel
    {
        private readonly LevelInfo level;

        /// <summary>
        /// Receives a level info object and a reference to the parent view model of the hierarchy.
        /// Lazy load is set to true.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="model"></param>
        public LevelViewModel(LevelInfo level, TreeViewItemViewModel model)
            : base(model, true)
        {
            this.level = level;
        }

        public LevelInfo Level
        {
            get { return level; }
        }

        public string LevelName
        {
            get { return level.Name; }
        }
        
        public int LevelNumber
        {
            get { return level.LevelNumber; }
        }

        public LevelInfoTypeEnum LevelType
        {
            get { return level.LevelType; }
        }

        public long MemberCount
        {
            get { return level.MemberCount; }
        }

        /// <summary>
        /// Populates the single member summary view model on user expansion.
        /// </summary>
        protected override void LoadChildren()
        {
            base.Children.Add(new MemberSummaryViewModel(MemberCount, this));
        }

        protected override string GetDisplayText()
        {
            return LevelName;
        }
    }
}