﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// A class that has no model in the cube hierarchy structure. Serves as a way to represent a summary of all the members in the corresponding parent level.
    /// Provides a way to browse the members of the level.
    /// </summary>
    public class MemberSummaryViewModel : TreeViewItemViewModel
    {

        public InteractionRequest<Notification> BrowseMemberRequest { get; private set; }
        public ICommand RaiseBrowseMembers { get; private set; }
        private readonly EventAggregator eventAggregator;

        private LevelViewModel parent;

        /// <summary>
        /// Receives a count of the members of the parent level object and a reference to the parent view model wrapper.
        /// </summary>
        /// <param name="memberCount"></param>
        /// <param name="model"></param>
        public MemberSummaryViewModel(long memberCount, TreeViewItemViewModel model)
            : base(model, false)
        {
            this.memberCount = memberCount - 1;
            parent = (LevelViewModel)model;
            RaiseBrowseMembers = new DelegateCommand(OnRaiseBrowseMember);
            BrowseMemberRequest = new InteractionRequest<Notification>();

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
        }

        private const string Members = "Members:";
        private readonly long memberCount;
        public long MembersCount
        {
            get { return memberCount; }
        }

        protected override void LoadChildren()
        {
        }

        protected override string GetDisplayText()
        {
            return Members;
        }

        // When button is clicked, popup child view is initialised to view the members of the level.
        private void OnRaiseBrowseMember()
        {
           CubeInfoViewModel cube = CubeSelectionCacheSingleton.Instance.SelectedCube;
           eventAggregator.GetEvent<MemberBrowseEvent>().Publish(new Tuple<string, string, ConnectionInfo>(parent.Level.UniqueName, cube.CubeName, cube.ParentConnection.Connection));
           BrowseMemberRequest.Raise(new Notification { Title = "Member Viewer" });
        }
    }
}