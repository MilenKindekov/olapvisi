﻿using System.Collections.Generic;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// View Model for a dimension object model in the cube browser hierarchy.
    /// </summary>
    public class DimensionViewModel : TreeViewItemViewModel
    {
        private readonly DimensionInfo dimension;
        private readonly List<NamedSetInfo> namedSets;

        /// <summary>
        /// Receives a dimensioninfo model, a reference to its parent in the hierarchy and a list of named sets.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="model"></param>
        /// <param name="sets"></param>
        public DimensionViewModel(DimensionInfo dimension, TreeViewItemViewModel model, List<NamedSetInfo> sets)
            : base(model, true)
        {
            this.dimension = dimension;
            namedSets = DimensionNamedSetFinder.FindNamedSetsForDimension(DimensionName, sets);
        }

        public string DimensionName
        {
            get { return dimension.Name; }
        }

        /// <summary>
        /// Method used to load all child view models, since the specific level has been expanded.
        /// </summary>
        protected override void LoadChildren()
        {
            // Children of each dimension can be hierarchies and named sets. Hierarchies can be part of a hierarchy folder.
            // There is no such distinction in the model objects, so one needs to be created when being presented on screen.
            List<HierarchyInfo> hierarchyList = dimension.Hierarchies;
            // A dictionary is used to store all hierarchies that are part of a hierarchy folder, since they will need to be loaded as the folders
            // children when the level is expanded.
            var displayFolders = new Dictionary<string, List<HierarchyInfo>>();

            // Populate all the folders with their respective hierarchies.
            foreach (HierarchyInfo hierarchy in hierarchyList)
            {
                if (hierarchy.DisplayFolder != "")
                {
                    if (displayFolders.ContainsKey(hierarchy.DisplayFolder))
                    {
                        displayFolders[hierarchy.DisplayFolder].Add(hierarchy);
                    }
                    else
                    {
                        displayFolders.Add(hierarchy.DisplayFolder, new List<HierarchyInfo> {hierarchy});
                    }
                }
            }

            // Add these folders as children of the current view model, creating appropriate displayfolder view model instances.
            foreach (string folder in displayFolders.Keys)
            {
                base.Children.Add(new DisplayFolderViewModel(folder, displayFolders[folder], this, this.dimension.DimensionType.ToString()));
            }

            // Populate hierarchies that have no display folders.
            foreach (HierarchyInfo hierarchy in hierarchyList)
            {
                if (hierarchy.DisplayFolder == "")
                {
                    base.Children.Add(new HierarchyViewModel(hierarchy, this, this.dimension.DimensionType.ToString()));
                }
            }

            // Populate named sets at end.
            if (namedSets != null)
            {
                foreach (NamedSetInfo namedSet in namedSets)
                {
                    base.Children.Add(new NamedSetViewModel(namedSet, this));
                }
            }
        }

        protected override string GetDisplayText()
        {
            return DimensionName;
        }
    }
}