﻿using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Wrapper view model for the named set model objects.
    /// </summary>
    public class NamedSetViewModel : TreeViewItemViewModel
    {
        private readonly NamedSetInfo namedSet;

        /// <summary>
        /// Receives a named set object model and a reference to the parent view model of the hierarchy (dimension view model).
        /// </summary>
        /// <param name="namedSet"></param>
        /// <param name="model"></param>
        public NamedSetViewModel(NamedSetInfo namedSet, TreeViewItemViewModel model)
            : base(model, false)
        {
            this.namedSet = namedSet;
            RaiseSelectCurrentNamedSet = new DelegateCommand(SelectCurrentNamedSetAction);
        }

        public ICommand RaiseSelectCurrentNamedSet { get; private set; }

        public string NamedSetName
        {
            get { return namedSet.Name; }
        }

        protected override void LoadChildren()
        {
        }

        protected override string GetDisplayText()
        {
            return NamedSetName;
        }

        /// <summary>
        /// Method which adds the named set to the 2D selection cache. An event is published to notify of this addition.
        /// </summary>
        private void SelectCurrentNamedSetAction()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            if (cache.AddNamedSetInfo(this.namedSet)) return;

            EventAggregator eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<NamedSetSelectedEvent>().Publish(true);
        }
    }
}