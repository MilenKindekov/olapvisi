﻿using System.Collections.Generic;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Measure group view model, represents a directory folder in the UI cube browser, holds all measures part of the measure group.
    /// </summary>
    public class MeasureGroupViewModel : TreeViewItemViewModel
    {
        private readonly MeasureGroupInfo measureGroup;
        private readonly List<MeasureInfo> measureList;

        /// <summary>
        /// Receives a measure group model object, a list of measures that are part of the measure group and a reference to the parent view model (ParentNodeViewModel)
        /// </summary>
        /// <param name="measureGroup"></param>
        /// <param name="measures"></param>
        /// <param name="model"></param>
        public MeasureGroupViewModel(MeasureGroupInfo measureGroup, List<MeasureInfo> measures,
                                     TreeViewItemViewModel model)
            : base(model, true)
        {
            this.measureGroup = measureGroup;
            measureList = measures;
        }

        public string MeasureGroupName
        {
            get { return measureGroup.Name; }
        }

        /// <summary>
        /// Loads the measures which are part of this unique measure group.
        /// </summary>
        protected override void LoadChildren()
        {
            foreach (MeasureInfo measure in measureList)
            {
                if (measure.MeasureGroup == measureGroup.Name)
                {
                    base.Children.Add(new MeasureViewModel(measure, this));
                }
            }
        }

        protected override string GetDisplayText()
        {
            return MeasureGroupName;
        }
    }
}