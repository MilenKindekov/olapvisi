﻿using System.Collections.Generic;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Class that represents the display folder view model, used as an intermediate level between the Dimension and Hierarchy objects.
    /// </summary>
    public class DisplayFolderViewModel : TreeViewItemViewModel
    {
        private readonly string folderName;
        private readonly List<HierarchyInfo> hierarcyList;
        private readonly string dimensionType;

        /// <summary>
        /// Receives a folder name, a list of hierarchies to be set as children, a reference to the parent view model and a dimension type.
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="hierarchies"></param>
        /// <param name="model"></param>
        /// <param name="dimensionType"></param>
        public DisplayFolderViewModel(string folderName, List<HierarchyInfo> hierarchies, TreeViewItemViewModel model, string dimensionType)
            : base(model, true)
        {
            this.folderName = folderName;
            this.dimensionType = dimensionType;
            hierarcyList = hierarchies;
        }

        public string FolderName
        {
            get { return folderName; }
        }

        /// <summary>
        /// Loads all children (only hierarchy objects), when level is expanded.
        /// </summary>
        protected override void LoadChildren()
        {
            foreach (HierarchyInfo hierarchy in hierarcyList)
            {
                base.Children.Add(new HierarchyViewModel(hierarchy, this, this.dimensionType));
            }
        }

        protected override string GetDisplayText()
        {
            return FolderName;
        }
    }
}