﻿using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// Class that represents the hierarchy view model. Stores Level View Model wrappers of LevelInfo objects as children.
    /// </summary>
    public class HierarchyViewModel : TreeViewItemViewModel
    {
        private readonly HierarchyInfo hierarchy;
        private readonly string dimensionType;

        /// <summary>
        /// Recieves a hierarchy model object, a reference to the parent view model in the hierarchy and the dimension type.
        /// Two types of commands are registered, to serve as the click handlers for the context menu for the hierarchy objects.
        /// Lazy load is set to true.
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="model"></param>
        /// <param name="dimensionType"></param>
        public HierarchyViewModel(HierarchyInfo hierarchy, TreeViewItemViewModel model, string dimensionType)
            : base(model, true)
        {
            this.hierarchy = hierarchy;
            this.dimensionType = dimensionType;
            RaiseSelectCurrentHierarchy = new DelegateCommand(SelectCurrentHierarchy2DAction);
            RaiseSelectCurrent3DHierarchy = new DelegateCommand(SelectCurrentHierarchy3DAction);
        }

        public ICommand RaiseSelectCurrentHierarchy { get; private set; }
        public ICommand RaiseSelectCurrent3DHierarchy { get; private set; }

        public string HierarchyName
        {
            get { return hierarchy.Caption; }
        }

        /// <summary>
        /// Loads the children for the hierarchy object, on a user expansion. Children are LevelInfo objects in view model wrappers.
        /// </summary>
        protected override void LoadChildren()
        {
            foreach (LevelInfo level in hierarchy.Levels)
            {
                if (level.LevelNumber > 0)
                    base.Children.Add(new LevelViewModel(level, this));
            }
        }
        
        protected override string GetDisplayText()
        {
            return HierarchyName;
        }

        /// <summary>
        /// Method which adds the hierarchy model to the 2D selection cache. An event is published to notify of this addition.
        /// </summary>
        private void SelectCurrentHierarchy2DAction()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            EventAggregator eventAggregator;

            if(dimensionType.ToLower() == "time")
            {
                if (cache.AddTimeHierarchyInfo(this.hierarchy)) return;

                eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
                eventAggregator.GetEvent<TimeHierarchySelectedEvent>().Publish(true);
            }
            else
            {
                if (cache.AddHierarchyInfo(this.hierarchy)) return;

                eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
                eventAggregator.GetEvent<HierarchySelectedEvent>().Publish(true);
            }
        }

        /// <summary>
        /// Method which adds the hierarchy model to the 3D selection cache. An event is published to notify of this addition.
        /// </summary>
        private void SelectCurrentHierarchy3DAction()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            EventAggregator eventAggregator;

            if (cache.Add3DHierarchyInfo(this.hierarchy)) return;

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<Hierarchy3DSelectedEvent>().Publish(true);
        }

        
    }
}