﻿using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.ModuleCubeNavigation.ViewModels
{
    /// <summary>
    /// View Model that represents the KPIInfo model.
    /// </summary>
    public class KpiViewModel : TreeViewItemViewModel
    {
        private readonly KpiInfo kpi;

        public KpiViewModel(KpiInfo kpi, TreeViewItemViewModel model)
            : base(model, false)
        {
            this.kpi = kpi;
        }

        public string KpiName
        {
            get { return kpi.Name; }
        }

        /// <summary>
        /// No children exist for KPIs
        /// </summary>
        protected override void LoadChildren()
        {
        }

        protected override string GetDisplayText()
        {
            return KpiName;
        }
    }
}