﻿using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.ModuleCubeNavigation.ViewModels;

namespace OlapVisi.ModuleCubeNavigation.Views
{
    /// <summary>
    /// Interaction logic for ServerSelectPopupView.xaml
    /// </summary>
    public partial class SelectDatabaseView : UserControl
    {
        public SelectDatabaseView()
        {
            DataContext = ServiceLocator.Current.GetInstance<SelectDatabaseViewModel>();
            InitializeComponent();
        }
    }
}