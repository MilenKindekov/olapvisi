﻿using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.ModuleCubeNavigation.ViewModels;

namespace OlapVisi.ModuleCubeNavigation.Views
{
    /// <summary>
    /// Interaction logic for ServerSelectPopupView.xaml
    /// </summary>
    public partial class SelectCubeView : UserControl
    {
        public SelectCubeView()
        {
            DataContext = ServiceLocator.Current.GetInstance<SelectCubeViewModel>();
            InitializeComponent();
        }
    }
}