﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleCubeNavigation.Interfaces;

namespace OlapVisi.ModuleCubeNavigation.Views
{
    /// <summary>
    /// Interaction logic for HierarchicalSideView.xaml
    /// </summary>
    public partial class HierarchicalSideView : UserControl, IView
    {
        public HierarchicalSideView(ISideViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;
        }

        #region IView Members

        public IViewModel ViewModel
        {
            get { return (IViewModel) DataContext; }
            set { DataContext = value; }
        }

        #endregion

        private void TreeViewSelectedItemChanged(object sender, RoutedEventArgs e)
        {
            var item = sender as TreeViewItem;
            if (item != null)
            {
                item.BringIntoView();
                e.Handled = true;
            }
        }


        // TreeViewItem selection on right click.
        private void OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);

            if (treeViewItem != null)
            {
                treeViewItem.Focus();
                e.Handled = true;
            }
        }

        static TreeViewItem VisualUpwardSearch(DependencyObject source)
        {
            while (source != null && !(source is TreeViewItem))
                source = VisualTreeHelper.GetParent(source);

            return source as TreeViewItem;
        }
    }
}