﻿using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.ModuleCubeNavigation.ViewModels;

namespace OlapVisi.ModuleCubeNavigation.Views
{
    /// <summary>
    /// Interaction logic for ServerSelectPopupView.xaml
    /// </summary>
    public partial class MemberBrowseSideHierarchyView : UserControl
    {
        public MemberBrowseSideHierarchyView()
        {
            DataContext = ServiceLocator.Current.GetInstance<MemberBrowseSideHierarchyViewModel>();
            InitializeComponent();
        }
    }
}