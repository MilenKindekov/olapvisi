﻿using System.Windows.Controls;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleCubeNavigation.Interfaces;

namespace OlapVisi.ModuleCubeNavigation.Views
{
    /// <summary>
    /// Interaction logic for ListSideView.xaml
    /// </summary>
    public partial class ListSideView : UserControl, IView
    {
        public ListSideView(ISideViewModel viewModel)
        {
            InitializeComponent();

            ViewModel = viewModel;
        }

        #region IView Members

        public IViewModel ViewModel
        {
            get { return (IViewModel) DataContext; }
            set { DataContext = value; }
        }

        #endregion
    }
}