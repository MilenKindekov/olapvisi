﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleCubeNavigation.ViewModels;
using OlapVisi.ModuleCubeNavigation.Views;

namespace OlapVisi.ModuleCubeNavigation
{
    /// <summary>
    /// Main initialization class in PRISM. Loads appropriate views and their view models. Done first thing at runtime.
    /// </summary>
    [Module(ModuleName = "ModuleCubeNavigation")]
    public class ModuleCubeNavigation : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _manager;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ModuleCubeNavigation(IUnityContainer container, IRegionManager manager)
        {
            _container = container;
            _manager = manager;
        }

        #region IModule Members

        public void Initialize()
        {
            _container.RegisterType<HierarchicalSideView>();
            _container.RegisterType<ListSideView>();
            _container.RegisterType<SelectDatabaseView>();
            _container.RegisterType<CubeHierarchyViewModel>();
            _container.RegisterType<ServerConnectionViewModel>();
            _container.RegisterType<SelectDatabaseViewModel>();

            IRegion sideRegionDatabases = _manager.Regions[RegionNames.SideRegionDatabases];
            IRegion sideRegionCubes = _manager.Regions[RegionNames.SideRegionCubes];

            ListSideView sideView = new ListSideView(_container.Resolve<ServerConnectionViewModel>());
            HierarchicalSideView hierarchicalSideView =
                new HierarchicalSideView(_container.Resolve<CubeHierarchyViewModel>());

            sideRegionDatabases.Add(sideView, "ServerConnection");
            sideRegionCubes.Add(hierarchicalSideView, "CubeHierarchy");

            sideRegionDatabases.Activate(sideView);
            sideRegionCubes.Activate(hierarchicalSideView);

            log.Debug("Module Cube Navigation Loaded");

            //var sideRegion = _manager.Regions(RegionNames.SideRegion);

            //_manager.RegisterViewWithRegion(RegionNames.SideRegion, typeof(HierarchicalSideView));
            //_manager.RegisterViewWithRegion(RegionNames.SideRegion, typeof(HierarchicalSideView));
        }

        #endregion
    }
}