﻿namespace OlapVisi.Infrastructure
{
    /// <summary>
    /// Names of regions to be used in the main OlapVisi shell view.
    /// </summary>
    public static class RegionNames
    {
        public const string SideRegionDatabases = "SideRegionDatabases";
        public const string SideRegionCubes = "SideRegionCubes";
        public const string TabRegion = "TabRegion";
        public const string MainRegion = "MainRegion";
        public const string ToolbarRegion = "ToolbarRegion";
        public const string StatusRegion = "StatusRegion";
    }
}
