﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.Infrastructure.Cache
{
    public class CubeSelectionCacheSingleton
    {
        #region Singleton Instance
        /// <summary>
        /// Singleton instance creation.
        /// </summary>
        private static CubeSelectionCacheSingleton instance;

        private CubeSelectionCacheSingleton() {}

        public static CubeSelectionCacheSingleton Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new CubeSelectionCacheSingleton();
                }
                return instance;
            }
        }
        #endregion

        public CubeInfoViewModel SelectedCube;

        private HashSet<HierarchyInfo> Selected3DHierarchies;

        public bool Add3DHierarchyInfo(HierarchyInfo hierarchy)
        {
            if (Selected3DHierarchies == null)
            {
                Selected3DHierarchies = new HashSet<HierarchyInfo>();
            }
            if (Selected3DHierarchies.Contains(hierarchy))
                return true;
            Selected3DHierarchies.Add(hierarchy);
            return false;
        }

        public HashSet<HierarchyInfo> GetSelected3DHierarchies()
        {
            return Selected3DHierarchies;
        }

        public void RemoveSelected3DHierarchy(HierarchyInfo hierarchy)
        {
            Selected3DHierarchies.Remove(hierarchy);
        }

        private HashSet<HierarchyInfo> SelectedHierarchies;

        public bool AddHierarchyInfo(HierarchyInfo hierarchy)
        {
            if(SelectedHierarchies == null)
            {
                SelectedHierarchies = new HashSet<HierarchyInfo>();
            }
            if (SelectedHierarchies.Contains(hierarchy))
                return true;
            SelectedHierarchies.Add(hierarchy);
            return false;
        }

        public HashSet<HierarchyInfo> GetSelectedHierarchies()
        {
            return SelectedHierarchies;
        }

        public void RemoveSelectedHierarchy(HierarchyInfo hierarchy)
        {
            SelectedHierarchies.Remove(hierarchy);
        }

        private HashSet<HierarchyInfo> SelectedTimeHierarchies;

        public bool AddTimeHierarchyInfo(HierarchyInfo hierarchy)
        {
            if (SelectedTimeHierarchies == null)
            {
                SelectedTimeHierarchies = new HashSet<HierarchyInfo>();
            }
            if (SelectedTimeHierarchies.Contains(hierarchy))
                return true;
            SelectedTimeHierarchies.Add(hierarchy);
            return false;
        }

        public HashSet<HierarchyInfo> GetSelectedTimeHierarchies()
        {
            return SelectedTimeHierarchies;
        }

        public void RemoveSelectedTimeHierarchy(HierarchyInfo hierarchy)
        {
            SelectedTimeHierarchies.Remove(hierarchy);
        }

        private HashSet<NamedSetInfo> SelectedNamedSets;

        public bool AddNamedSetInfo(NamedSetInfo namedSet)
        {
            if (SelectedNamedSets == null)
            {
                SelectedNamedSets = new HashSet<NamedSetInfo>();
            }
            if (SelectedNamedSets.Contains(namedSet))
                return true;
            SelectedNamedSets.Add(namedSet);
            return false;
        }

        public HashSet<NamedSetInfo> GetSelectedNamedSets()
        {
            return SelectedNamedSets;
        }

        public void RemoveSelectedNamedSet(NamedSetInfo namedSet)
        {
            SelectedNamedSets.Remove(namedSet);
        }

        private HashSet<MeasureInfo> SelectedMeasures;

        public bool AddMeasureInfo(MeasureInfo measure)
        {
            if (SelectedMeasures == null)
            {
                SelectedMeasures = new HashSet<MeasureInfo>();
            }
            if (SelectedMeasures.Contains(measure))
                return true;
            SelectedMeasures.Add(measure);
            return false;
        }

        public HashSet<MeasureInfo> GetSelectedMeasures()
        {
            return SelectedMeasures;
        }

        public void RemoveSelectedMeasure(MeasureInfo measure)
        {
            SelectedMeasures.Remove(measure);
        }

        private HashSet<MeasureInfo> Selected3DMeasures;

        public bool Add3DMeasureInfo(MeasureInfo measure)
        {
            if (Selected3DMeasures == null)
            {
                Selected3DMeasures = new HashSet<MeasureInfo>();
            }
            if (Selected3DMeasures.Contains(measure))
                return true;
            Selected3DMeasures.Add(measure);
            return false;
        }

        public HashSet<MeasureInfo> GetSelected3DMeasures()
        {
            return Selected3DMeasures;
        }

        public void RemoveSelected3DMeasure(MeasureInfo measure)
        {
            Selected3DMeasures.Remove(measure);
        }

        public void ClearCurrentSelections()
        {
            if(SelectedHierarchies != null) SelectedHierarchies.Clear();
            if (SelectedTimeHierarchies != null) SelectedTimeHierarchies.Clear();
            if (SelectedMeasures != null) SelectedMeasures.Clear();
            if (SelectedNamedSets != null) SelectedNamedSets.Clear();
        }
    }
}
