﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AnalysisServices.AdomdClient;
using OlapVisi.DataLayer;
using OlapVisi.DataLayer.Data;
using OlapVisi.DataLayer.MDX;

namespace OlapVisi.Infrastructure.Cache
{
    public class MemberLoaderCache
    {
        #region Singleton Instance

        /// <summary>
        /// Singleton instance creation.
        /// </summary>
        private static MemberLoaderCache instance;

        private MemberLoaderCache()
        {
        }

        public static MemberLoaderCache Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MemberLoaderCache();
                    MemberCollectionLookupCache = new Dictionary<string, List<Tuple<string, string>>>();
                    MemberLookupCache = new Dictionary<string, string>();
                }
                return instance;
            }
        }

        private static Dictionary<string, List<Tuple<string,string>>> MemberCollectionLookupCache;
        private static Dictionary<string, string> MemberLookupCache; 

        public List<Tuple<string, string>> GetMemberCollection(string level, string cube, ConnectionInfo connection, List<FilterMDXObject> filters)
        {
            string key = GetKey(level, cube, filters);

            if (MemberCollectionLookupCache.ContainsKey(key))
                return MemberCollectionLookupCache[key];
            else
            {
                LoadMemberData(key, connection);
                return MemberCollectionLookupCache[key];
            }
        }

        public List<Tuple<string, string>> GetMemberCollection(string level, string cube, ConnectionInfo connection)
        {
            string key = GetKey(level, cube);

            if (MemberCollectionLookupCache.ContainsKey(key))
                return MemberCollectionLookupCache[key];
            else
            {
                LoadMemberData(key, connection);
                return MemberCollectionLookupCache[key];
            }
        }

        public List<Tuple<string,string>> GetMemberCollection(string key, ConnectionInfo connection)
        {
            if (MemberCollectionLookupCache.ContainsKey(key))
                return MemberCollectionLookupCache[key];
            else
            {
                LoadMemberData(key, connection);
                return MemberCollectionLookupCache[key];
            }
        }

        public string GetMemberUniqueName(string memberName)
        {
            return MemberLookupCache[memberName];
        }

        public void LoadMemberData(string level, string cube, ConnectionInfo connection, List<FilterMDXObject> filters)
        {
            string memberQuery = GetKey(level, cube, filters);

            if (!MemberCollectionLookupCache.ContainsKey(memberQuery))
            {
                var queryExecuter = new DefaultQueryExecuter(connection);
                CellSet queryResults = queryExecuter.ExecuteQuery(memberQuery);

                List<Tuple<string, string>> memberData = new List<Tuple<string, string>>();
                foreach (var t in queryResults.Axes[1].Set.Tuples)
                {
                    memberData.Add(new Tuple<string, string>(t.Members[0].Caption, t.Members[0].UniqueName));
                    if (!MemberLookupCache.ContainsKey(t.Members[0].Caption))
                        MemberLookupCache.Add(t.Members[0].Caption, t.Members[0].UniqueName);
                }

                MemberCollectionLookupCache.Add(memberQuery, memberData);
            }
        }

        public void LoadMemberData(string level, string cube, ConnectionInfo connection)
        {
            string memberQuery = GetKey(level, cube);

            if (!MemberCollectionLookupCache.ContainsKey(memberQuery))
            {
                var queryExecuter = new DefaultQueryExecuter(connection);
                CellSet queryResults = queryExecuter.ExecuteQuery(memberQuery);

                List<Tuple<string, string>> memberData = new List<Tuple<string, string>>();
                foreach (var t in queryResults.Axes[1].Set.Tuples)
                {
                    memberData.Add(new Tuple<string,string>(t.Members[0].Caption, t.Members[0].UniqueName));
                    if (!MemberLookupCache.ContainsKey(t.Members[0].Caption))
                        MemberLookupCache.Add(t.Members[0].Caption, t.Members[0].UniqueName);
                }

                MemberCollectionLookupCache.Add(memberQuery, memberData);
            }
        }

        public void LoadMemberData(string key, ConnectionInfo connection)
        {
            if (!MemberCollectionLookupCache.ContainsKey(key))
            {
                var queryExecuter = new DefaultQueryExecuter(connection);
                CellSet queryResults = queryExecuter.ExecuteQuery(key);

                List<Tuple<string, string>> memberData = new List<Tuple<string, string>>();
                foreach (var t in queryResults.Axes[1].Set.Tuples)
                {
                    memberData.Add(new Tuple<string, string>(t.Members[0].Caption, t.Members[0].UniqueName));
                    if (!MemberLookupCache.ContainsKey(t.Members[0].Caption))
                        MemberLookupCache.Add(t.Members[0].Caption, t.Members[0].UniqueName);
                }

                MemberCollectionLookupCache.Add(key, memberData);
            }
        }

        private string GetKey(string level, string cube)
        {
            SelectMDXObject getMembersQuery = new SelectMDXObject(level, cube);
            return getMembersQuery.GetMemberSelectQuery();
        }

        private string GetKey(string level, string cube, List<FilterMDXObject> filters)
        {
            SelectMDXObject getMembersQuery = new SelectMDXObject(level, cube, filters);
            return getMembersQuery.GetMemberSelectFiltersQuery();
        }

        #endregion
    }
}
