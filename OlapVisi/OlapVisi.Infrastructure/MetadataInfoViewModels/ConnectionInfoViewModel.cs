﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.ViewModel;
using OlapVisi.DataLayer;
using OlapVisi.DataLayer.Data;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.Infrastructure.MetadataInfoViewModels
{
    /// <summary>
    /// View Model for the connection info object model. The way it gets displayed on the UI.
    /// </summary>
    public class ConnectionInfoViewModel : NotificationObject
    {
        private ConnectionInfo connection;
        private ObservableCollection<CubeInfoViewModel> cubeObjects;
        private bool isChecked;
        private string itemName;

        public ConnectionInfoViewModel()
        {
        }

        public ConnectionInfoViewModel(ConnectionInfo conn)
        {
            connection = conn;
            itemName = conn.ItemName;
            cubeObjects = new ObservableCollection<CubeInfoViewModel>();
            PopulateCubeObjects();
        }

        /// <summary>
        /// Name of connection.
        /// </summary>
        public string ItemName
        {
            get { return itemName; }
            set
            {
                itemName = value;
                RaisePropertyChanged(() => ItemName);
            }
        }

        /// <summary>
        /// Connection object model.
        /// </summary>
        public ConnectionInfo Connection
        {
            get { return connection; }
            set
            {
                connection = value;
                RaisePropertyChanged(() => Connection);
            }
        }

        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                RaisePropertyChanged(() => IsChecked);
            }
        }

        /// <summary>
        /// Collection of cube objects displayed on the cube selection screen.
        /// </summary>
        public ObservableCollection<CubeInfoViewModel> CubeObjects
        {
            get { return cubeObjects; }
            set
            {
                cubeObjects = value;
                RaisePropertyChanged(() => CubeObjects);
            }
        }

        /// <summary>
        /// Retrieve the string names of the cubes and create view models for each, to be displayed on the UI.
        /// </summary>
        private void PopulateCubeObjects()
        {
            Dictionary<string, CubeDefInfo> cubeDict = connection.CubeDictionary;
            foreach (string cube in cubeDict.Keys)
            {
                CubeObjects.Add(new CubeInfoViewModel(cube, cubeDict[cube], this));
            }
        }
    }
}