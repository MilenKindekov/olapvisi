﻿using Microsoft.Practices.Prism.ViewModel;
using OlapVisi.DataLayer;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.Infrastructure.MetadataInfoViewModels
{
    /// <summary>
    /// View Model class for the CubeInfo model object. Used to display cubes on the UI.
    /// </summary>
    public class CubeInfoViewModel : NotificationObject
    {
        private readonly ConnectionInfoViewModel parentConnection;
        private CubeDefInfo cubeModel;
        private string cubeName;
        private bool isSelected;

        public CubeInfoViewModel(string name, CubeDefInfo cube, ConnectionInfoViewModel parent)
        {
            cubeName = name;
            cubeModel = cube;
            parentConnection = parent;
            isSelected = false;
        }

        public string CubeName
        {
            get { return cubeName; }
            set
            {
                cubeName = value;
                RaisePropertyChanged(() => CubeName);
            }
        }

        public CubeDefInfo CubeModel
        {
            get { return cubeModel; }
            set
            {
                cubeModel = value;
                RaisePropertyChanged(() => CubeModel);
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        public ConnectionInfoViewModel ParentConnection
        {
            get { return parentConnection; }
        }

        public ConnectionInfo Connection
        {
            get { return parentConnection.Connection; }
        }
    }
}