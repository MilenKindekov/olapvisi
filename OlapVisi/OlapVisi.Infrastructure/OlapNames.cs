﻿using System.Collections.Generic;

namespace OlapVisi.Infrastructure
{
    public static class OlapNames
    {
        public const string Dimensions = "Dimensions";
        public const string Measures = "Measures";
        public const string Kpis = "Kpis";

        public static IList<string> GetOlapNamesAsList()
        {
            IList<string> olapNames = new List<string>();

            olapNames.Add(Dimensions);
            olapNames.Add(Measures);
            olapNames.Add(Kpis);

            return olapNames;
        }
    }
}
