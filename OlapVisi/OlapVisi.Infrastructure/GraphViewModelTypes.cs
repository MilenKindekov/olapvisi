﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlapVisi.Infrastructure
{
    /// <summary>
    /// Types of graph view models. Update when new ones are added.
    /// </summary>
    public static class GraphViewModelTypes
    {
        public const string GenericGraph = "GenericGraph";
        public const string TreeMapViewModelGraph = "TreeMapViewModelGraph";
    }
}
