﻿using Microsoft.Practices.Prism.Regions;

namespace OlapVisi.Infrastructure
{
    public interface IRegionManagerAware
    {
        IRegionManager RegionManager { get; set; }
    }
}
