﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.Infrastructure
{
    /// <summary>
    /// A helper class used for conversion between 2D visualizations and their 3D counterparts.
    /// </summary>
    public class ConverterObject2D3D
    {
        public ConverterObject2D3D()
        {
            XLevelNumber = 1;
            YLevelNumber = 1;
            ZLevelNumber = 1;

            XFilterToApply = null;
            YFilterToApply = null;
            ZFilterToApply = null;
        }

        // Measures to work over in the 3D graphic.
        public MeasureInfo MeasureOne { get; set; }
        public MeasureInfo MeasureTwo { get; set; }

        // Dimensions to work over in the 3D graphic.
        public HierarchyInfo XDimension { get; set; }
        public HierarchyInfo YDimension { get; set; }
        public HierarchyInfo ZDimension { get; set; }

        // Level numbers, used do populate the correct levels of the hierarchies of the 3D visuals.
        public int XLevelNumber { get; set; }
        public int YLevelNumber { get; set; }
        public int ZLevelNumber { get; set; }

        // Unique names here, to pinpoint exact coordinate of 3D.
        public string XFilterToApply { get; set; }
        public string YFilterToApply { get; set; }
        public string ZFilterToApply { get; set; }

    }
}
