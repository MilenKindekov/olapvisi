﻿using Microsoft.Practices.Prism.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.Infrastructure.Events
{
    public class MainViewSelectedEvent : CompositePresentationEvent<string>
    {
    }
}
