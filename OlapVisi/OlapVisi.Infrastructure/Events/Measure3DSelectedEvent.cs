﻿using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class Measure3DSelectedEvent : CompositePresentationEvent<bool>
    {
    }
}
