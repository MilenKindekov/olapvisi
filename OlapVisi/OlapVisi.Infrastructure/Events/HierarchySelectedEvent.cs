﻿using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class HierarchySelectedEvent : CompositePresentationEvent<bool>
    {
    }
}
