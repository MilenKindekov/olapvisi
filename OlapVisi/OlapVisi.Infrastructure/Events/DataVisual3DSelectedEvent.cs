﻿using System.Windows.Media.Media3D;
using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class DataVisual3DSelectedEvent : CompositePresentationEvent<Point3D>
    {
    }
}
