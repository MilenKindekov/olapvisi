﻿using System;
using System.Windows.Media.Media3D;
using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class FilterVisual3DSelectedEvent : CompositePresentationEvent<Tuple<Point3D,string>>
    {
    }
}
