﻿using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class Hierarchy3DSelectedEvent : CompositePresentationEvent<bool>
    {
    }
}
