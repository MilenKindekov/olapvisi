﻿using Microsoft.Practices.Prism.Events;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.Infrastructure.Events
{
    public class MeasureGroupSelectedEvent : CompositePresentationEvent<MeasureGroupInfo>
    {
    }
}
