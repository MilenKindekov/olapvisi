﻿using System;
using System.Windows.Media.Media3D;
using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class LevelVisual3DSelectedEvent : CompositePresentationEvent<Point3D>
    {
    }
}
