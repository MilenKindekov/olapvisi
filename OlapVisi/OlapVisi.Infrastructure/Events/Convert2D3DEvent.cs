﻿using System.Windows.Media.Media3D;
using Microsoft.Practices.Prism.Events;

namespace OlapVisi.Infrastructure.Events
{
    public class Convert2D3DEvent : CompositePresentationEvent<ConverterObject2D3D>
    {
    }
}
