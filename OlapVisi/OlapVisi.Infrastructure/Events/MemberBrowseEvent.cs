﻿using System;
using Microsoft.Practices.Prism.Events;
using OlapVisi.DataLayer;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.Infrastructure.Events
{
    public class MemberBrowseEvent : CompositePresentationEvent<Tuple<string,string,ConnectionInfo>>
    {
    }
}
