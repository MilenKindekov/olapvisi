﻿using Microsoft.Practices.Prism.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using System.Collections.ObjectModel;

namespace OlapVisi.Infrastructure.Events
{
    public class DatabaseSelectedEvent : CompositePresentationEvent<ObservableCollection<ConnectionInfoViewModel>>
    {
    }
}
