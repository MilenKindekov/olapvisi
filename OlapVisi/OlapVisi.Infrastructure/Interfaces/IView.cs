﻿namespace OlapVisi.Infrastructure
{
    public interface IView
    {
        IViewModel ViewModel { get; set; }
    }
}
