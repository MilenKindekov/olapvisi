﻿using System.Windows;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;

namespace OlapVisi.Infrastructure.InteractionRequests
{
    public interface IPopupWindowActionAware
    {
        Window HostWindow { get; set; }

        Notification HostNotification { get; set; }
    }
}
