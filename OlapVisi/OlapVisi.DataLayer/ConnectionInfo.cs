﻿using System.Collections.Generic;
using System.Linq;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.DataLayer
{
    /// <summary>
    /// Connection info object, stores the connection string for a database along with all OLAP cubes in that database.
    /// </summary>
    public class ConnectionInfo
    {
        private string connectionString;
        private Dictionary<string, CubeDefInfo> cubeDictionary;


        public ConnectionInfo()
        {
            cubeDictionary = new Dictionary<string, CubeDefInfo>();
        }

        public ConnectionInfo(string item, string connection)
        {
            ItemName = item;
            ConnectionString = connection;
            UpdateCubesDictionary();
        }

        public string ItemName { get; set; }

        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = AppendFullString(ItemName, value); }
        }

        /// <summary>
        /// The cube dictionary, storing all cubes for this connection info objects in memory.
        /// </summary>
        public Dictionary<string, CubeDefInfo> CubeDictionary
        {
            get { return cubeDictionary; }
            set { cubeDictionary = value; }
        }

        /// <summary>
        /// Retrieve all cube names stored in memory for this connection info object.
        /// </summary>
        /// <returns></returns>
        public IList<string> GetCubeNames()
        {
            return cubeDictionary.Keys.ToList();
        }

        /// <summary>
        /// Retrieve all cubes stored in memory for this connection info object.
        /// </summary>
        /// <returns></returns>
        public IList<CubeDefInfo> GetCubeDefObjects()
        {
            return cubeDictionary.Values.ToList();
        }

        /// <summary>
        /// Retrieve all cubes for this connection info object and store them in memory.
        /// </summary>
        public void UpdateCubesDictionary()
        {
            var provider = new OlapMetadataProvider(this);
            cubeDictionary = provider.GetCubes();
        }

        /// <summary>
        /// Creates the final connection string, to be used when connecting to the database.
        /// </summary>
        /// <param name="catalog"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        private string AppendFullString(string catalog, string connection)
        {
            return connection + ";Catalog=" + catalog + ";";
        }
    }
}