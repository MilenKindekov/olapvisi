﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlapVisi.DataLayer.MDX
{
    /// <summary>
    /// Object used for generation of the appropriate filter MDX query strings.
    /// </summary>
    public class FilterMDXObject : IMDXObject
    {
        private String queryString;
        public String QueryString
        {
            get { return queryString; }
            set { queryString = value; }
        }
        private string filterType;

        /// <summary>
        /// Create filter query from passed in list of filters and a filter type.
        /// </summary>
        /// <param name="filterString"></param>
        /// <param name="filterType"></param>
        public FilterMDXObject(List<String> filterString, string filterType)
        {
            this.filterType = filterType;
            QueryString = GenerateQueryStringFromList(filterString);
            GenerateCorrectFilterQuery();
        }

        /// <summary>
        /// Create filter query from a passed in filter string and filter type.
        /// </summary>
        /// <param name="filterString"></param>
        /// <param name="filterType"></param>
        public FilterMDXObject(String filterString, string filterType)
        {
            this.filterType = filterType;
            QueryString = filterString;
            GenerateCorrectFilterQuery();
        }

        /// <summary>
        /// Specific filter query that creates a filter from/to two given filter strings and type.
        /// </summary>
        /// <param name="filterStringFrom"></param>
        /// <param name="filterStringTo"></param>
        /// <param name="filterType"></param>
        public FilterMDXObject(String filterStringFrom, String filterStringTo, string filterType)
        {
            this.filterType = filterType;
            QueryString = filterStringFrom + " : " + filterStringTo;
            GenerateCorrectFilterQuery();
        }

        public new String ToString()
        {
            return queryString;
        }

        /// <summary>
        /// Method used to generate a filter query with multiple filters from the same level. This is important as otherwise the MDX will be wrong if
        /// separate filter queries are generated for each filter from the same level.
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        private String GenerateQueryStringFromList(List<String> filters)
        {
            String temporaryStringBuilder = "";
            foreach(var filter in filters)
            {
                temporaryStringBuilder += filter + ", ";
            }
            return temporaryStringBuilder.Trim().TrimEnd(',');
        }

        /// <summary>
        /// Build up the correct MDX filter syntax.
        /// </summary>
        private void GenerateCorrectFilterQuery()
        {
            String FilterString = "";
            if (filterType.ToString().StartsWith("Not")) FilterString = "-";
            if (!queryString.StartsWith("{")) queryString = "{" + queryString;
            if (!queryString.EndsWith("}")) queryString = queryString + "}";
            queryString = FilterString + queryString;
        }
    }
}
