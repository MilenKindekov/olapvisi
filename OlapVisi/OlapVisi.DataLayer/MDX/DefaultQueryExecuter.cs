﻿  using System;
using System.Collections.Generic;
using System.Data;

using Microsoft.AnalysisServices.AdomdClient;

namespace OlapVisi.DataLayer.Data
{
    /// <summary>
    /// Class used to execute given query strings. Initialized from a connection object.
    /// </summary>
    public class DefaultQueryExecuter : IMdxExecuter
    {
        private ConnectionInfo m_Connection = null;

        public ConnectionInfo Connection
        {
            get
            {
                if (m_Connection == null)
                    m_Connection = new ConnectionInfo();
                return m_Connection;
            }
        }

        public DefaultQueryExecuter(ConnectionInfo connection)
        {
            m_Connection = connection;
        }

        public bool Cancel()
        {
            // TODO: Lock
            if (m_CurrentCmd != null)
            {
                try
                {
                    m_CurrentCmd.Cancel();
                }
                catch (AdomdErrorResponseException)
                {
                }
                finally
                {
                    m_CurrentCmd = null;
                }
            }

            return true;
        }

        private AdomdCommand m_CurrentCmd;

        public int ExecuteNonQuery(String query)
        {
            try
            {
                if (Connection != null)
                {
                    using(AdomdConnection conn = new AdomdConnection(Connection.ConnectionString))
                    {
                        conn.Open();
                            using (AdomdCommand cmd = new AdomdCommand(query, conn))
                            {
                                return cmd.ExecuteNonQuery();
                            }
                        
                    }          
                }
                return 0;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(
                    "{0} ExecuteNonQuery ERROR: {1}\r\n Connection String: {2} \r\n Query: {3}\r\n",
                    DateTime.Now.ToString(), ex.ToString(), Connection.ConnectionString, query);
                throw;
            }
        }

        public DataTable ExecuteReader(string query)
        {
            try
            {
                if (Connection != null)
                {
                    using (AdomdConnection conn = new AdomdConnection(Connection.ConnectionString))
                    {
                        conn.Open();
                            using (AdomdCommand cmd = new AdomdCommand(query, conn))
                            {
                                DateTime start = DateTime.Now;
                                using (var reader = cmd.ExecuteReader())
                                {
                                    if (reader != null)
                                    {
                                        DataTable table = new DataTable();
                                        var metadata_table = reader.GetSchemaTable();
                                        if (metadata_table != null)
                                        {
                                            foreach (DataRow row in metadata_table.Rows)
                                            {
                                                table.Columns.Add(new DataColumn(row[0].ToString()));
                                            }
                                        }

                                        if (table.Columns.Count >= reader.FieldCount)
                                        {
                                            while (reader.Read())
                                            {
                                                var values = new object[reader.FieldCount];
                                                for (int i = 0; i < reader.FieldCount; i++)
                                                {
                                                    values[i] = reader[i];
                                                }
                                                table.Rows.Add(values);
                                            }
                                        }
                                        System.Diagnostics.Debug.WriteLine("ExecuteReader executimg time: " +
                                                                           (DateTime.Now - start).ToString());
                                        return table;
                                    }
                                }
                            }
                        
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(
                    "{0} ExecuteReader ERROR: {1}\r\n Connection String: {2} \r\n Query: {3}\r\n",
                    DateTime.Now.ToString(), ex.ToString(), Connection.ConnectionString, query);
                throw ex;
            }
        }

        public CellSet ExecuteQuery(string query)
        {
            try
            {
                if (Connection != null)
                {
                    using (AdomdConnection conn = new AdomdConnection(Connection.ConnectionString))
                    {

                        conn.Open();
                            using (AdomdCommand cmd = new AdomdCommand(query, conn))
                            {
                                DateTime start = DateTime.Now;
                                CellSet cs = cmd.ExecuteCellSet();
                                System.Diagnostics.Debug.WriteLine("MDX query executimg time: " +
                                                                   (DateTime.Now - start).ToString());

                                return cs;
                            }
                        
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(
                    "{0} ExecuteQuery ERROR: {1}\r\n Connection String: {2} \r\n Query: {3}\r\n",
                    DateTime.Now.ToString(), ex.ToString(), Connection.ConnectionString, query);
                throw ex;
            }
        }

    //    public String GetCellSetDescription(string query)
    //    {
    //        try
    //        {
    //            CellSet cs = ExecuteQuery(query);
    //            if (cs != null)s
    //            {
    //                CellSetDescriptionProvider provider = new CellSetDescriptionProvider(cs);
    //                provider.Connection.ConnectionString = Connection.ConnectionString;

    //                DateTime start1 = DateTime.Now;
    //                var res = provider.Serialize();
    //                System.Diagnostics.Debug.WriteLine("MDX query serialization time: " +
    //                                                   (DateTime.Now - start1).ToString());
    //                return res;
    //            }
    //            return string.Empty;
    //        }
    //        catch (Exception ex)
    //        {
    //            System.Diagnostics.Trace.TraceError(
    //                "{0} GetCellSetDescription ERROR: {1}\r\n Connection String: {2} \r\n Query: {3}\r\n",
    //                DateTime.Now.ToString(), ex.ToString(), Connection.ConnectionString, query);
    //            throw;
    //        }
    //    }
    }
}