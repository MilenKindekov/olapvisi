﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlapVisi.DataLayer.MDX
{
    public static class MDXFilterTypes
    {
        public const string Equal = "Equals";
        public const string NotEquals = "Not Equals";
        public const string Between = "Between";
        public const string NotBetween = "Not Between";
        public const string In = "In";
        public const string NotIn = "Not In";

        public static IList<string> GetMDXFilterTypesAsList()
        {
            IList<string> filterTypes = new List<string>();

            filterTypes.Add(Equal);
            filterTypes.Add(NotEquals);
            filterTypes.Add(Between);
            filterTypes.Add(NotBetween);
            filterTypes.Add(In);
            filterTypes.Add(NotIn);

            return filterTypes;
        }
    }

}
