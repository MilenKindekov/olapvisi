﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlapVisi.DataLayer.MDX
{
    /// <summary>
    /// Class that encompasses logic to create proper MDX Select statements based on different parameters supplied.
    /// </summary>
    public class SelectMDXObject : IMDXObject
    {
        #region Parameters
        private List<String> measures; 
        public List<String> Measures
        {
            get
            {
                return measures;
            }
            set
            {
                measures = value;
                GenerateMeasuresQueryString();
            }
        }

        private List<String> axiss;
        public List<String> Axis
        {
            get
            {
                return axiss;
            }
            set
            {
                axiss = value;
                GenerateAxisQueryString();
            }
        }

        private String cubeName;
        public String CubeName
        {
            get { return cubeName; }
            set
            {
                cubeName = value;
                CheckCubeName();
            }
        }

        private String namedSet;
        public String NamedSet
        {
            get { return namedSet; }
            set
            {
                namedSet = value;
                GenerateNamedSetQueryString();
            }
        }

        public string LevelName { get; set; }

        public string NamedSetFiltertype { get; set; }

        private List<FilterMDXObject> filters;
        public List<FilterMDXObject> Filter
        {
            get { return filters; }
            set
            {
                filters = value;
                GenerateFilterQueryString();
            }
        }

        public String MeasureQueryString = "";
        public String AxisQueryString = "";
        public String NamedSetQueryString = "";
        public String FilterQueryString = "";

        public readonly bool StrictSyntax;
        #endregion

        /// <summary>
        /// NonStrict Syntax Select Constructor.
        /// Can be used to quickly generate Select statements, without Filtering and NamedSets.
        /// </summary>
        public SelectMDXObject()
        {
            Measures = new List<String>() {""};
            Axis = new List<String>() {""};
            CubeName = "";
            StrictSyntax = false;
        }

        /// <summary>
        /// The generic select object constructor, applying measures, axeses, cube name, filters, named sets.
        /// </summary>
        /// <param name="measureList"></param>
        /// <param name="axisList"></param>
        /// <param name="cubeName"></param>
        /// <param name="filters"></param>
        /// <param name="namedSetOptional"></param>
        /// <param name="namedSetFilterType"></param>
        public SelectMDXObject(List<String> measureList, List<String> axisList, String cubeName, List<FilterMDXObject> filters = null, String namedSetOptional = null, String namedSetFilterType = MDXFilterTypes.In)
        {
            Measures = measureList;
            Axis = axisList;
            CubeName = cubeName;
            Filter = filters;
            NamedSetFiltertype = namedSetFilterType;
            NamedSet = namedSetOptional;            
            StrictSyntax = true;
        }

        /// <summary>
        /// Select object from a cube name and a level name caption.
        /// </summary>
        /// <param name="levelCaption"></param>
        /// <param name="cubeName"></param>
        public SelectMDXObject(String levelCaption, String cubeName)
        {
            CubeName = cubeName;
            LevelName = levelCaption;
        }

        /// <summary>
        /// Select object from a cube name, a level name caption and a list of filter objects.
        /// </summary>
        /// <param name="levelCaption"></param>
        /// <param name="cubeName"></param>
        /// <param name="filters"></param>
        public SelectMDXObject(String levelCaption, String cubeName, List<FilterMDXObject> filters)
        {
            CubeName = cubeName;
            LevelName = levelCaption;
            Filter = filters;
        }

        /// <summary>
        /// Retrieve a query string to return only members from a specified level and cube name.
        /// </summary>
        /// <returns></returns>
        public string GetMemberSelectQuery()
        {
            return String.Format("SELECT {{}} ON COLUMNS, {0}.ALLMEMBERS ON ROWS  FROM {1}", LevelName, CubeName);
        }

        /// <summary>
        /// Retrieve a query string to return only members from a specified level and filter query part.
        /// </summary>
        /// <returns></returns>
        public string GetMemberSelectFiltersQuery()
        {
            return String.Format("SELECT {{}} ON COLUMNS, {0}.ALLMEMBERS ON ROWS FROM {1}", LevelName, FilterQueryString);
        }

        /// <summary>
        /// Retrieve the query string.
        /// </summary>
        /// <returns></returns>
        public new String ToString()
        {
            if (!StrictSyntax) return "SELECT NON EMPTY {" + MeasureQueryString + "} ON COLUMNS, NON EMPTY {" + AxisQueryString + "} ON ROWS FROM " + CubeName;
            return GenerateFinalQueryString();
        }

        #region Helper Methods

        /// <summary>
        /// Final query string generation, which puts all parts together.
        /// </summary>
        /// <returns></returns>
        private String GenerateFinalQueryString()
        {
            String finalQuery = "SELECT NON EMPTY {" + MeasureQueryString + "} ON COLUMNS, NON EMPTY {" +
                                AxisQueryString + "} DIMENSION PROPERTIES MEMBER_CAPTION, MEMBER_UNIQUE_NAME ON ROWS FROM ";
            if (NamedSet != null) finalQuery += NamedSetQueryString + FilterQueryString + ")";
            else finalQuery += FilterQueryString;
            return finalQuery;
        }

        /// <summary>
        /// Generates the measure part of the query string. Measures are separate from one another with commas.
        /// </summary>
        private void GenerateMeasuresQueryString()
        {
            if(!String.IsNullOrEmpty(MeasureQueryString)) MeasureQueryString = "";
            foreach(String measure in Measures)
            {
                MeasureQueryString += measure + ",";
            }
            MeasureQueryString = MeasureQueryString.Remove(MeasureQueryString.Length - 1);
        }

        /// <summary>
        /// Generates the axis part of the query string. Axeses are appended to each other in the format: Axis1 * Axis2 * Axis3 ...
        /// </summary>
        private void GenerateAxisQueryString()
        {
            if (!String.IsNullOrEmpty(AxisQueryString)) AxisQueryString = "";
            foreach (String axis in Axis)
            {
                AxisQueryString += axis + "*";
            }
            AxisQueryString = AxisQueryString.Remove(AxisQueryString.Length - 1);

            if(StrictSyntax && String.IsNullOrEmpty(AxisQueryString)) throw new ArgumentException("Strict Syntax is activated, there should be a valid Axis specified!");
        }

        /// <summary>
        /// Check if an appropriate cube name has been specified. Adds brackets if needed.
        /// </summary>
        private void CheckCubeName()
        {
            if (!cubeName.StartsWith("[")) cubeName = "[" + cubeName;
            if (!cubeName.EndsWith("]")) cubeName = cubeName + "]";

            if (StrictSyntax && cubeName == "[]") throw new ArgumentException("Strict Syntax is activated, Cube Name should not be empty!");
        }

        /// <summary>
        /// Creates a string from a named set filter.
        /// </summary>
        private void GenerateNamedSetQueryString()
        {
            if (!String.IsNullOrEmpty(NamedSetQueryString)) NamedSetQueryString = "";
            if (NamedSet != null)
            {
                String FilterType = "";
                if (NamedSetFiltertype.ToString().StartsWith("Not")) FilterType = "-";
                NamedSetQueryString = "(SELECT " + FilterType + NamedSet + " ON COLUMNS FROM ";
            }
        }
        
        /// <summary>
        /// Method to generate appropriate filter query string from a FilterMDXObject.
        /// </summary>
        private void GenerateFilterQueryString()
        {
            if (!String.IsNullOrEmpty(NamedSetQueryString)) FilterQueryString = "";

            if(Filter != null && Filter.Count > 0)
            {
                // Recurses multiple filters into each other.
                foreach (FilterMDXObject filter in Filter)
                {
                    FilterQueryString += "(SELECT (" + filter.ToString() + ") ON COLUMNS FROM ";
                }
                // Adds the cube name and closes all the opened parantheses.
                FilterQueryString += CubeName;
                for (int index = 0; index < Filter.Count; index++)
                {
                    FilterQueryString += ")";
                }
            }
            else
            {
                FilterQueryString = CubeName;
            }
        }
        #endregion
    }
}
