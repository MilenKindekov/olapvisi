﻿using System;
using System.Collections.Generic;
using Microsoft.AnalysisServices.AdomdClient;
using System.Collections.ObjectModel;
using System.Configuration;

namespace OlapVisi.DataLayer
{
    /// <summary>
    /// Static class, which retrieves a list of databases from the specified server address in the app.config file.
    /// </summary>
    public static class ConnectionHelper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Retrieves databases in the form of a list of strings.
        /// </summary>
        /// <returns></returns>
        public static IList<string> GetDatabasesNameList()
        {

            var myConnectionString = ConfigurationManager.AppSettings["ServerAddress"];

            log.Debug("Retrieving set IP address: " + myConnectionString);
            //const string myConnectionString = "Data Source=192.168.4.83;";
            IList<string> databases = new ObservableCollection<string>();

            log.Debug("Making ADOMD Client Connection");
            using (AdomdConnection myConnection = new AdomdConnection(myConnectionString))
            {
                log.Debug("Connection Made.");
                try
                {
                    myConnection.Open();
                    log.Debug("Connection Open");
                    log.Debug("Qyuerying DB Schema");
                    AdomdCommand query = new AdomdCommand("SELECT * FROM $SYSTEM.DBSCHEMA_CATALOGS", myConnection);
                    var result = query.ExecuteReader();
                    while (result.Read())
                    {
                        var catalogName = result["CATALOG_NAME"].ToString();
                        databases.Add(catalogName);
                    }

                    log.Debug("Cubes retrieved from DB");
                    myConnection.Close();
                    return databases;
                }
                catch (Exception ex)
                {
                    myConnection.Close();
                    log.Error("Error when retrieving OLAP Cubes.");
                    log.Error("Exception Message: " + ex.Message);
                    log.Error("Exception Stack Trace: " + ex.StackTrace);
                    return null;
                    // Log
                }
            }           
        }

        /// <summary>
        /// Retrieves databases in the form of a list of connection objects.
        /// </summary>
        /// <returns></returns>
        public static IList<ConnectionInfo> GetDatabasesConnectionList()
        {
            var myConnectionString = "Provider=MSOLAP;" + ConfigurationManager.AppSettings["ServerAddress"];

            log.Debug("Retrieving set IP address: " + myConnectionString);
            //const string myConnectionString = "Data Source=192.168.4.83;";
            IList<ConnectionInfo> databases = new ObservableCollection<ConnectionInfo>();

            log.Debug("Making ADOMD Client Connection");
            using (AdomdConnection myConnection = new AdomdConnection(myConnectionString))
            {
                log.Debug("Connection Made.");
                try
                {
                    myConnection.Open();
                    log.Debug("Connection Open");
                    log.Debug("Qyuerying DB Schema");
                    AdomdCommand query = new AdomdCommand("SELECT * FROM $SYSTEM.DBSCHEMA_CATALOGS", myConnection);
                    var result = query.ExecuteReader();
                    while (result.Read())
                    {
                        var catalogName = result["CATALOG_NAME"].ToString();
                        databases.Add(new ConnectionInfo(catalogName, myConnectionString));
                    }
                    log.Debug("Cubes retrieved from DB");
                    myConnection.Close();
                    return databases;
                }
                catch (Exception ex)
                {
                    myConnection.Close();
                    log.Error("Error when retrieving OLAP Cubes.");
                    log.Error("Exception Message: " + ex.Message);
                    log.Error("Exception Stack Trace: " + ex.StackTrace);
                    return null;
                    // Log
                }
            }
        }

        /// <summary>
        /// Helper method to retrieve the server address from the app.config file.
        /// </summary>
        /// <returns></returns>
        public static string GetServerString()
        {
            var myConnectionString = ConfigurationManager.AppSettings["ServerAddress"];
            return myConnectionString;
        }
    }
}
