﻿using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using OlapVisi.Resources;

namespace OlapVisi
{
    class Bootstrapper : UnityBootstrapper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override IModuleCatalog CreateModuleCatalog()
        {
            log.Debug("Creating Module Catalog");
            var moduleCatalog = new DirectoryModuleCatalog {ModulePath = @".\Modules"};

            log.Debug("Returning Module Directory Path: " + moduleCatalog.ModulePath);
            return moduleCatalog;
        }

        protected override DependencyObject CreateShell()
        {
            log.Debug("Resolving Container Shell");
            return Container.Resolve<Shell>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();
            log.Debug("Shell Initialized");

            App.Current.MainWindow = (CustomWindow)this.Shell;
            App.Current.MainWindow.Show();
            log.Debug("Shell Main Window Displayed");
        }
    }
}
