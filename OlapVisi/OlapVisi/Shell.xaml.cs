﻿using System.Windows;
using OlapVisi.Resources;

namespace OlapVisi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Shell : CustomWindow
    {
        public Shell()
        {
            InitializeComponent();
        }
    }
}
