﻿using System.Windows;

namespace OlapVisi
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnStartup(StartupEventArgs e)
        {
            log.Debug("Application Start-Up");

            base.OnStartup(e);
            var bootstrapper = new Bootstrapper();

            log.Debug("Running Bootstrapper");

            bootstrapper.Run();
        }
    }
}
