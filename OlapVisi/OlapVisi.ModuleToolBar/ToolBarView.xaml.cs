﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OlapVisi.Infrastructure;

namespace OlapVisi.ModuleToolBar
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ToolBarView : UserControl, IView
    {
        public ToolBarView(IViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        }

        #region IView Members

        public IViewModel ViewModel
        {
            get { return (IViewModel)DataContext; }
            set { DataContext = value; }
        }

        #endregion
    }
}
