﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleToolBar
{
    /// <summary>
    /// View Model for top toolbar.
    /// </summary>
    public class ToolBarViewModel : NotificationObject, IViewModel
    {

        public ICommand Select2D { get; private set; }
        public ICommand Select3D { get; private set; }
        public ICommand SelectGuide { get; private set; }
        public ICommand SelectSettings { get; private set; }
        private readonly EventAggregator eventAggregator;

        public ToolBarViewModel()
        {
            Select2D = new DelegateCommand(Select2DAction);
            Select3D = new DelegateCommand(Select3DAction);
            SelectSettings = new DelegateCommand(SelectSettingsAction);
            SelectGuide = new DelegateCommand(SelectGuideAction);

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
        }

        // Each button has its own command, it publishes an event via the event aggregator, consumed by the Main Module view controller, which decides
        // the view to display.


        private void Select2DAction()
        {
            eventAggregator.GetEvent<MainViewSelectedEvent>().Publish("MainGraphView");
        }

        private void Select3DAction()
        {
            eventAggregator.GetEvent<MainViewSelectedEvent>().Publish("Cube3DView");
        }

        private void SelectGuideAction()
        {
            eventAggregator.GetEvent<MainViewSelectedEvent>().Publish("GuideView");
        }

        private void SelectSettingsAction()
        {
            eventAggregator.GetEvent<MainViewSelectedEvent>().Publish("SettingsView");
        }

    }
}