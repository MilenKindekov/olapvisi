﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OlapVisi.Infrastructure;

namespace OlapVisi.ModuleToolBar
{
    /// <summary>
    /// PRISM module initialisation and view creation for the top toolbar.
    /// </summary>
    [Module(ModuleName = "ModuleToolBar")]
    public class ModuleMainVisualisation : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _manager;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ModuleMainVisualisation(IUnityContainer container, IRegionManager manager)
        {
            _container = container;
            _manager = manager;
        }

        #region IModule Members

        public void Initialize()
        {
            _container.RegisterType<ToolBarView>();

            IRegion toolBarRegion = _manager.Regions[RegionNames.ToolbarRegion];

            ToolBarView view = new ToolBarView(_container.Resolve<ToolBarViewModel>());

            toolBarRegion.Add(view, "ToolBarView");
            toolBarRegion.Activate(view);

            log.Debug("Module Tool Bar Loaded");
        }

        #endregion
    }
}