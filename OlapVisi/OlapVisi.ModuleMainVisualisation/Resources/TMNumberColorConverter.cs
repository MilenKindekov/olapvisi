﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation
{
    /// <summary>
    /// The main class used to convert a value to a colour. Used by the tree map to select appropriate colour for the data elements.
    /// </summary>
    class TMNumberColorConverter: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            {
                try
                {
                    TreeMapGraphViewModel mainGraphModel = (TreeMapGraphViewModel)values[0];
                    decimal v1 = 0;
                    decimal v2 = 0;


                    decimal number = (decimal)values[1];

                    if (mainGraphModel.CurrentColorSetting == "Logarithmic")
                    {
                        decimal log = (decimal)Math.Log((double)number);

                        decimal max = mainGraphModel.ColorLogMax;
                        decimal min;

                        if (mainGraphModel.ColorLogMin - 2 > 0)
                        {
                            min = mainGraphModel.ColorLogMin - 2;
                        }
                        else
                        {
                            min = 0;
                        }
                            
                        v1 = log - min;
                        v2 = max - min;
                    }
                    else
                    {
                        decimal max = mainGraphModel.ColorMax;
                        decimal min = 0;

                        v1 = (decimal)number - min;
                        v2 = max - min;
                    }

                    

                    decimal percentage = v1 / v2;
                    byte alpha = System.Convert.ToByte(percentage * 255);

                    var maxColor = System.Drawing.ColorTranslator.FromHtml(mainGraphModel.HexMaxColor);

                    return new SolidColorBrush(Color.FromArgb(alpha, maxColor.R, maxColor.G, maxColor.B));
                }
                catch
                {
                    return new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
                }
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
