﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// A Singleton Factory used to create and initialize a graph view model for the tabular view of the main 2D visualisation page.
    /// </summary>
    public class GraphViewModelFactory
    {
        #region Singleton Instance
        /// <summary>
        /// Singleton instance creation.
        /// </summary>
        private static GraphViewModelFactory instance;

        private GraphViewModelFactory() { }

        public static GraphViewModelFactory Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new GraphViewModelFactory();
                }
                return instance;
            }
        }
        #endregion

        public GraphViewModelBase getGraphViewModel(string type, int defaultValue, CubeInfoViewModel cube)
        {
            switch(type)
            {
                case (GraphViewModelTypes.GenericGraph):
                    return new GenericGraphViewModel(defaultValue, GraphViewModelTypes.GenericGraph);
                case (GraphViewModelTypes.TreeMapViewModelGraph):
                    return new TreeMapGraphViewModel(defaultValue, GraphViewModelTypes.TreeMapViewModelGraph, cube);
                default:
                    throw new InvalidOperationException("Invalid Graph View Model Type");
            }
        }
    }
}
