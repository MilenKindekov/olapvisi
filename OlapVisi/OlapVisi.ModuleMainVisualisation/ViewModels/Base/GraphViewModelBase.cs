﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// Abstract class for all 2D graph view models.
    /// </summary>
    public abstract class GraphViewModelBase : NotificationObject, IGraphViewModel
    {
        private string graphType;
        public string GraphType
        {
            get { return graphType; }
            set
            {
                graphType = value;
                RaisePropertyChanged(() => GraphType);
            }
        }

        private string viewName;
        public string ViewName
        {
            get { return viewName; }
            set
            {
                viewName = value;
                Edit = false;
                RaisePropertyChanged(() => ViewName);
            }
        }

        private bool edit;
        public bool Edit
        {
            get { return edit; }
            set
            {
                edit = value;
                RaisePropertyChanged(() => Edit);
            }
        }

        private int tabIndex;
        public int TabIndex
        {
            get { return tabIndex; }
            set
            {
                tabIndex = value;
                RaisePropertyChanged(() => TabIndex);
            }
        }

        public string SetDefaultViewName(int number)
        {
            return "Default Graph View " + number;
        }
    }
}
