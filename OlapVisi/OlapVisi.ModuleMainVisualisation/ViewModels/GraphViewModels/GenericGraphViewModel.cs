﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleMainVisualisation.Views;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// A generic instance of the graph view model. Used as the default view when adding new tabs to the main view.
    /// </summary>
    public class GenericGraphViewModel : GraphViewModelBase
    {

        public GenericGraphViewModel(int viewNumber, string type)
        {
            ViewName = SetDefaultViewName(viewNumber);
            GraphType = type;
            TabIndex = viewNumber;
        }
    }
}
