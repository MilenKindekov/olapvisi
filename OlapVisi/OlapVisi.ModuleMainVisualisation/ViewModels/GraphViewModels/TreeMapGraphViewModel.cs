﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer;
using OlapVisi.DataLayer.MDX;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using OlapVisi.Infrastructure.Tools;
using OlapVisi.ModuleMainVisualisation.Models;


namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// Class representing the treemap graph view model. Includes all the properties and actions that the view uses to bind to.
    /// As well as the source collection for the treemap graph itself.
    /// </summary>
    public class TreeMapGraphViewModel : GraphViewModelBase
    {
        #region Parameters

        private readonly EventAggregator eventAggregator;
        public ICommand PlotGraph { get; private set; }
        public ICommand ConvertTo3D { get; private set; }
        public ICommand LevelUp { get; private set; }
        public ICommand Maximize { get; private set; }
        public ICommand SelectItem { get; private set; }

        public ICommand SlideLeft { get; private set; }
        public ICommand SlideRight { get; private set; }
        public ICommand SlideUp { get; private set; }
        public ICommand SlideDown { get; private set; }

        public ICommand AddFilterItem { get; private set; }
        public ICommand RemoveFilterItem { get; private set; }
        

        public InteractionRequest<Notification> BrowseMemberRequest { get; private set; }
        public ICommand RaiseBrowseDateMembers { get; private set; }
        public ICommand RaiseBrowseHierarchyMembers { get; private set; }

        private List<Tuple<string, string>> currentMemberList;
        private List<Tuple<string, string>> currentHierarchyFilterMemberList; 
        private bool initialPlot;
        private TreeMapActionController actionController;

        #endregion

        /// <summary>
        /// Initializes main commands, subscriptions, collections, parameters.
        /// </summary>
        /// <param name="viewNumber"></param>
        /// <param name="type"></param>
        /// <param name="cube"></param>
        public TreeMapGraphViewModel(int viewNumber, string type, CubeInfoViewModel cube)
        {
            ViewName = SetDefaultViewName(viewNumber);
            GraphType = type;
            TabIndex = viewNumber;
            CurrentlySelectedCube = cube;

            Collection = new List<TreeMapGraphDataViewModel>();

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<CubeSelectedEvent>().Subscribe(CubeSelected);

            PlotGraph = new DelegateCommand(PlotGraphAction);
            ConvertTo3D = new DelegateCommand(ConvertTo3DAction);
            LevelUp = new DelegateCommand(LevelUpAction);
            Maximize = new DelegateCommand<TreeMapGraphDataViewModel>(MaximizeAction);
            SelectItem = new DelegateCommand<TreeMapGraphDataViewModel>(SelectItemAction);

            SlideLeft = new DelegateCommand(SlideLeftAction);
            SlideRight = new DelegateCommand(SlideRightAction);
            SlideUp = new DelegateCommand(SlideUpAction);
            SlideDown = new DelegateCommand(SlideDownAction);

            AddFilterItem = new DelegateCommand(AddFilterItemAction);
            RemoveFilterItem = new DelegateCommand<FilterItem>(RemoveFilterItemAction);

            RaiseBrowseDateMembers = new DelegateCommand(OnRaiseBrowseDateMember);
            RaiseBrowseHierarchyMembers = new DelegateCommand(OnRaiseBrowseHierarchyMember);
            BrowseMemberRequest = new InteractionRequest<Notification>();

            ShowSettings = true;
            initialPlot = false;

            CurrentColorSetting = "Logarithmic";
            HexMaxColor = "#003480";
            HexHoverColor = "#800080";
            HexTextColor = "#FFFFFF";

            SelectedDateNotNullOrEmpty = false;
            SelectedHierarchyFilterNotNullOrEmpty = false;

            UseDateFilterSlider = true;
            UseVerticalFilterSlider = true;
            

            Filters = new List<FilterMDXObject>();
            FilterItems = new ObservableCollection<FilterItem>();
            FilterItems.Add(new FilterItem());

            actionController = new TreeMapActionController(this);
        }

        #region Currently Selected Values

        /// <summary>
        /// Action that sets the currently selected cube.
        /// </summary>
        /// <param name="cube"></param>
        private void CubeSelected(CubeInfoViewModel cube)
        {
            CurrentlySelectedCube = cube;   
        }

        /// <summary>
        /// Parameter of the currently selected cube.
        /// </summary>
        private CubeInfoViewModel currentlySelectedCube;
        public CubeInfoViewModel CurrentlySelectedCube
        {
            get { return this.currentlySelectedCube; }
            set
            {
                this.currentlySelectedCube = value;
                RaisePropertyChanged(() => CurrentlySelectedCube);
            }
        }

        /// <summary>
        /// The currently selected hierarchy by the user (drop down menu)
        /// </summary>
        private HierarchyInfo selectedHierarchy;
        public HierarchyInfo SelectedHierarchy
        {
            get { return this.selectedHierarchy; }
            set
            {
                this.selectedHierarchy = value;
                RaisePropertyChanged(() => SelectedHierarchy);
            }
        }

        /// <summary>
        /// The currently selected date hierarchy by the user (drop down menu)
        /// </summary>
        private HierarchyInfo selectedDate;
        public HierarchyInfo SelectedDate
        {
            get { return this.selectedDate; }
            set
            {
                this.selectedDate = value;
                RaisePropertyChanged(() => SelectedDate);
                // If an initial plot has been completed, activates the level selection drop down menu and updates the levels for the hierarchy.
                // Updates the 3D conversion allowance flag as well.
                if(initialPlot)
                {
                    SelectedDateNotNullOrEmpty = true;
                    UpdateDateLevelCollection();
                    Update3DConversionFlag();
                }
            }
        }

        /// <summary>
        /// The currently selected second hierarchy for filtering by the user (drop down menu)
        /// </summary>
        private HierarchyInfo selectedHierarchyFilter;
        public HierarchyInfo SelectedHierarchyFilter
        {
            get { return this.selectedHierarchyFilter; }
            set
            {
                this.selectedHierarchyFilter = value;
                RaisePropertyChanged(() => SelectedHierarchyFilter);
                // If an initial plot has been completed, activates the level selection drop down menu and updates the levels for the hierarchy.
                // Updates the 3D conversion allowance flag as well.
                if (initialPlot)
                {
                    SelectedHierarchyFilterNotNullOrEmpty = true;
                    UpdateHierarchyFilterLevelCollection();
                    Update3DConversionFlag();
                }
            }
        }

        /// <summary>
        /// Property of the first selected measure by the user (Color).
        /// </summary>
        private MeasureInfo selectedColor;
        public MeasureInfo SelectedColor
        {
            get { return this.selectedColor; }
            set
            {
                this.selectedColor = value;
                RaisePropertyChanged(() => SelectedColor);
            }
        }

        /// <summary>
        /// Property of the second selected measure by the user (Size).
        /// </summary>
        private MeasureInfo selectedSize;
        public MeasureInfo SelectedSize
        {
            get { return this.selectedSize; }
            set
            {
                this.selectedSize = value;
                RaisePropertyChanged(() => SelectedSize);
            }
        }

        /// <summary>
        /// The selected level of the horizontal filter slider.
        /// </summary>
        private LevelInfo selectedDateLevel;
        public LevelInfo SelectedDateLevel
        {
            get { return this.selectedDateLevel; }
            set
            {
                this.selectedDateLevel = value;
                RaisePropertyChanged(() => SelectedDateLevel);
                // Checks if the value being set is not null, updates the members of the selected level and their member counts.
                // Calls a refresh on the treemap graph (updating the data).
                if(this.selectedDateLevel != null)
                {
                    SelectedDateLevelMemberCount = this.selectedDateLevel.MemberCount;        
                    LoadMembersForDateLevelSelected();
                    RefreshAction("Horizontal");
                }
                else
                {
                    // Removes the filters associated with the horizontal filter slider and refreshes the data.
                    SelectedDateLevelMemberCount = 0;
                    currentMemberList = null;
                    MinDate = null;
                    MaxDate = null;
                    CurrentDate = null;
                    FilterGraphAction();
                }
            }
        }

        /// <summary>
        /// The selected level of the vertical filter slider.
        /// </summary>
        private LevelInfo selectedHierarchyFilterLevel;
        public LevelInfo SelectedHierarchyFilterLevel
        {
            get { return this.selectedHierarchyFilterLevel; }
            set
            {
                this.selectedHierarchyFilterLevel = value;
                RaisePropertyChanged(() => SelectedHierarchyFilterLevel);
                // Checks if the value being set is not null, updates the members of the selected level and their member counts.
                // Calls a refresh on the treemap graph (updating the data).
                if (this.selectedHierarchyFilterLevel != null)
                {
                    SelectedHierarchyFilterLevelMemberCount = this.selectedHierarchyFilterLevel.MemberCount;
                    LoadMembersForHierarchyLevelSelected();
                    RefreshAction("Vertical");
                }
                else
                {
                    // Removes the filters associated with the vertical filter slider and refreshes the data.
                    SelectedHierarchyFilterLevelMemberCount = 0;
                    currentHierarchyFilterMemberList = null;
                    MinHierarchyFilter = null;
                    MaxHierarchyFilter = null;
                    CurrentHierarchyFilter = null;
                    FilterGraphAction();
                }
            }
        }

        /// <summary>
        /// Boolean flag property that toggles the settings drop down dynamic menu.
        /// </summary>
        private bool showSettings;
        public bool ShowSettings
        {
            get { return showSettings; }
            set
            {
                showSettings = value;
                RaisePropertyChanged(() => ShowSettings);
            }
        }

        /// <summary>
        /// Boolean flag property that toggles the vertical filter slider. If untoggled, updates the selected level for the slider to null (removing any filters).
        /// </summary>
        private bool useVerticalFilterSlider;
        public bool UseVerticalFilterSlider
        {
            get { return useVerticalFilterSlider; }
            set
            {
                useVerticalFilterSlider = value;
                RaisePropertyChanged(() => UseVerticalFilterSlider);
                if (!useVerticalFilterSlider) SelectedHierarchyFilterLevel = null;
            }
        }

        /// <summary>
        /// Boolean flag property that toggles the horizontal filter slider. If untoggled, updates the selected level for the slider to null (removing any filters).
        /// </summary>
        private bool useDateFilterSlider;
        public bool UseDateFilterSlider
        {
            get { return useDateFilterSlider; }
            set
            {
                useDateFilterSlider = value;
                RaisePropertyChanged(() => UseDateFilterSlider);
                if (!useDateFilterSlider) SelectedDateLevel = null;
            }
        }

        #endregion Currently Selected Values

        #region Date Slider Member Items

        /// <summary>
        /// The current date string (sits in middle of slider)
        /// </summary>
        private string currentDate;
        public string CurrentDate
        {
            get { return this.currentDate; }
            set
            {
                this.currentDate = value;           
                RaisePropertyChanged(() => CurrentDate);
                FilterGraphAction();
            }
        }

        /// <summary>
        /// Current date unique name, used for filtering.
        /// </summary>
        public string CurrentDateUniqueName
        {
            get;
            set;
        }

        /// <summary>
        /// Minimum date (left of slider).
        /// </summary>
        private string minDate;
        public string MinDate
        {
            get { return this.minDate; }
            set
            {
                this.minDate = value;
                RaisePropertyChanged(() => MinDate);
            }
        }

        /// <summary>
        /// Maximum date (right of slider).
        /// </summary>
        private string maxDate;
        public string MaxDate
        {
            get { return this.maxDate; }
            set
            {
                this.maxDate = value;
                RaisePropertyChanged(() => MaxDate);
            }
        }

        /// <summary>
        /// Tick count property of data items.
        /// </summary>
        private int maxTickCount;
        public int MaxTickCount
        {
            get { return this.maxTickCount; }
            set
            {
                this.maxTickCount = value;
                RaisePropertyChanged(() => MaxTickCount);
            }
        }

        /// <summary>
        /// Currently selected tick integer.
        /// </summary>
        private int selectedTick;
        public int SelectedTick
        {
            get { return this.selectedTick; }
            set
            {
                this.selectedTick = value;   
                RaisePropertyChanged(() => SelectedTick);
                CurrentDate = currentMemberList[selectedTick].Item1;
                CurrentDateUniqueName = currentMemberList[selectedTick].Item2;
            }
        }

        /// <summary>
        /// Action to slide slider to the left (decrease of selected tick).
        /// </summary>
        private void SlideLeftAction()
        {
            if(CurrentDate != null)
            {
                if(SelectedTick > 0)
                {
                    SelectedTick = SelectedTick - 1;
                }
            }
        }

        /// <summary>
        /// Action to slide slider to the right (increase of selected tick).
        /// </summary>
        private void SlideRightAction()
        {
            if (CurrentDate != null)
            {
                if (SelectedTick < MaxTickCount)
                {
                    SelectedTick = SelectedTick + 1;
                }
            }
        }

        /// <summary>
        /// Number of members of currently selcted level.
        /// </summary>
        private long selectedDateLevelMemberCount;
        public long SelectedDateLevelMemberCount
        {
            get { return this.selectedDateLevelMemberCount; }
            set
            {
                this.selectedDateLevelMemberCount = value;
                RaisePropertyChanged(() => SelectedDateLevelMemberCount);
            }
        }

        /// <summary>
        /// Property used to specify whether the date hierarchy has been selected by the user.
        /// </summary>
        private bool selectedDateNotNullOrEmpty;
        public bool SelectedDateNotNullOrEmpty
        {
            get { return selectedDateNotNullOrEmpty; }
            set
            {
                selectedDateNotNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedDateNotNullOrEmpty);
            }
        }

        /// <summary>
        /// Collection of members of the selected level. (Slider slides through them).
        /// </summary>
        private RangeObservableCollection<LevelInfo> datesLevelCollection;
        public RangeObservableCollection<LevelInfo> DatesLevelCollection
        {
            get { return this.datesLevelCollection; }
            set
            {
                this.datesLevelCollection = value;
                RaisePropertyChanged(() => DatesLevelCollection);
            }
        }

        #endregion Date Slider Member Items    

        #region Vertical Slider Member Items
        /// <summary>
        /// Region includes similar duplicated properties to the Horizontal (Date) Slider
        /// </summary>
        /// 
        private string currentHierarchyFilter;
        public string CurrentHierarchyFilter
        {
            get { return this.currentHierarchyFilter; }
            set
            {
                this.currentHierarchyFilter = value;
                RaisePropertyChanged(() => CurrentHierarchyFilter);
                FilterGraphAction();
            }
        }

        public string CurrentHierarchyFilterUniqueName
        {
            get;
            set; 
        }

        private string minHierarchyFilter;
        public string MinHierarchyFilter
        {
            get { return this.minHierarchyFilter; }
            set
            {
                this.minHierarchyFilter = value;
                RaisePropertyChanged(() => MinHierarchyFilter);
            }
        }

        private string maxHierarchyFilter;
        public string MaxHierarchyFilter
        {
            get { return this.maxHierarchyFilter; }
            set
            {
                this.maxHierarchyFilter = value;
                RaisePropertyChanged(() => MaxHierarchyFilter);
            }
        }

        private int maxHierarchyFilterTickCount;
        public int MaxHierarchyFilterTickCount
        {
            get { return this.maxHierarchyFilterTickCount; }
            set
            {
                this.maxHierarchyFilterTickCount = value;
                RaisePropertyChanged(() => MaxHierarchyFilterTickCount);
            }
        }

        private int selectedHierarchyFilterTick;
        public int SelectedHierarchyFilterTick
        {
            get { return this.selectedHierarchyFilterTick; }
            set
            {
                this.selectedHierarchyFilterTick = value;
                RaisePropertyChanged(() => SelectedHierarchyFilterTick);
                CurrentHierarchyFilter = currentHierarchyFilterMemberList[selectedHierarchyFilterTick].Item1;
                CurrentHierarchyFilterUniqueName = currentHierarchyFilterMemberList[selectedHierarchyFilterTick].Item2;
            }
        }

        private void SlideUpAction()
        {           
            if (CurrentHierarchyFilter != null)
            {
                if (SelectedHierarchyFilterTick < MaxHierarchyFilterTickCount)
                {
                    SelectedHierarchyFilterTick = SelectedHierarchyFilterTick + 1;
                }
            }
        }

        private void SlideDownAction()
        {
            if (CurrentHierarchyFilter != null)
            {
                if (SelectedHierarchyFilterTick > 0)
                {
                    SelectedHierarchyFilterTick = SelectedHierarchyFilterTick - 1;
                }
            }
        }

        private long selectedHierarchyFilterLevelMemberCount;
        public long SelectedHierarchyFilterLevelMemberCount
        {
            get { return this.selectedHierarchyFilterLevelMemberCount; }
            set
            {
                this.selectedHierarchyFilterLevelMemberCount = value;
                RaisePropertyChanged(() => SelectedHierarchyFilterLevelMemberCount);
            }
        }

        private bool selectedHierarchyFilterNotNullOrEmpty;
        public bool SelectedHierarchyFilterNotNullOrEmpty
        {
            get { return selectedHierarchyFilterNotNullOrEmpty; }
            set
            {
                selectedHierarchyFilterNotNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedHierarchyFilterNotNullOrEmpty);
            }
        }

        private RangeObservableCollection<LevelInfo> hierarchyFilterLevelCollection;
        public RangeObservableCollection<LevelInfo> HierarchyFilterLevelCollection
        {
            get { return this.hierarchyFilterLevelCollection; }
            set
            {
                this.hierarchyFilterLevelCollection = value;
                RaisePropertyChanged(() => HierarchyFilterLevelCollection);
            }
        }
        #endregion   

        #region Collection Specific Values

        private decimal _colorMin = 0;
        public decimal ColorMin
        {
            get { return _colorMin; }
            set
            {
                _colorMin = value;
                RaisePropertyChanged(() => ColorMin);
            }
        }


        private decimal _colorMax = 0;
        public decimal ColorMax
        {
            get { return _colorMax; }
            set
            {
                _colorMax = value;
                RaisePropertyChanged(() => ColorMax);
            }
        }

        private decimal _colorLogMin = 0;
        public decimal ColorLogMin
        {
            get { return _colorLogMin; }
            set
            {
                _colorLogMin = value;
                RaisePropertyChanged(() => ColorLogMin);
            }
        }

        private decimal _colorLogMax = 0;
        public decimal ColorLogMax
        {
            get { return _colorLogMax; }
            set
            {
                _colorLogMax = value;
                RaisePropertyChanged(() => ColorLogMax);
            }
        }

        private string currentColorSetting;
        public string CurrentColorSetting
        {
            get { return currentColorSetting; }
            set
            {
                currentColorSetting = value;
                RaisePropertyChanged(() => CurrentColorSetting);
            }
        }

        private string hexMaxColor;
        public string HexMaxColor
        {
            get { return hexMaxColor; }
            set
            {
                hexMaxColor = value;
                RaisePropertyChanged(() => HexMaxColor);
            }
        }

        private string hexHoverColor;
        public string HexHoverColor
        {
            get { return hexHoverColor; }
            set
            {
                hexHoverColor = value;
                RaisePropertyChanged(() => HexHoverColor);
            }
        }

        private string hexTextColor;
        public string HexTextColor
        {
            get { return hexTextColor; }
            set
            {
                hexTextColor = value;
                RaisePropertyChanged(() => HexTextColor);
            }
        }

        private string breadCrumb = "";
        public string BreadCrumb
        {
            get { return this.breadCrumb; }
            set
            {
                this.breadCrumb = value;
                RaisePropertyChanged(() => BreadCrumb);
            }
        }

        private bool levelUpEnabled = false;
        public bool LevelUpEnabled
        {
            get { return this.levelUpEnabled; }
            set
            {
                this.levelUpEnabled = value;
                RaisePropertyChanged(() => LevelUpEnabled);
            }
        }

        /// <summary>
        /// Flag specifies whether appropriate items are selected by the user and ready for conversion to 3D.
        /// </summary>
        private bool convertTo3DEnabled = false;
        public bool ConvertTo3DEnabled
        {
            get { return this.convertTo3DEnabled; }
            set
            {
                this.convertTo3DEnabled = value;
                RaisePropertyChanged(() => ConvertTo3DEnabled);
            }
        }

        /// <summary>
        /// Currently maximised item.
        /// </summary>
        private TreeMapGraphDataViewModel maximisedItem;
        public TreeMapGraphDataViewModel MaximisedItem
        {
            get { return this.maximisedItem; }
            set
            {
                this.maximisedItem = value;
                RaisePropertyChanged(() => MaximisedItem);
            }
        }

        /// <summary>
        /// Currently selected item.
        /// </summary>
        private TreeMapGraphDataViewModel selectedItem;
        public TreeMapGraphDataViewModel SelectedItem
        {
            get { return this.selectedItem; }
            set
            {
                if(this.selectedItem != null)
                {
                    this.selectedItem.IsSelected = false;
                }
                this.selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        /// <summary>
        /// Collection consumed by the UI treemap graph.
        /// </summary>
        private List<TreeMapGraphDataViewModel> collection;
        public List<TreeMapGraphDataViewModel> Collection
        {
            get { return this.collection; }
            set
            {
                this.collection = value;
                RaisePropertyChanged(() => Collection);
            }
        }

        #endregion Collection Specific Values

        #region Private Helpers Methods and Actions
        /// <summary>
        /// Region includes helper actions that call the action controller - selection, maximization, conversion, level up, refresh etc.
        /// </summary>

        private bool CheckAllSelected()
        {
            return selectedHierarchy != null && selectedSize != null && selectedColor != null &&
                   selectedSize != selectedColor;
        }

        // Maximize and create breadcrumb to display above graph.
        private void SelectItemAction(TreeMapGraphDataViewModel vm)
        {
            vm.IsSelected = true;
            this.SelectedItem = vm;
        }


        // Maximize and create breadcrumb to display above graph.
        private void MaximizeAction(TreeMapGraphDataViewModel vm)
        {
            actionController.TreeMapItemMaximizeAction(vm);         
        }

        private void ConvertTo3DAction()
        {
            actionController.TreeMapConvertTo3DAction();
        }

        private void LevelUpAction()
        {
            actionController.TreeMapItemLevelUpAction();
        }

        // Refresh Button Click.
        private void RefreshAction(string type)
        {
            switch(type)
            {
                case("Horizontal"):
                    {
                        SelectMDXObject getMembersQuery = new SelectMDXObject(SelectedDateLevel.UniqueName, CurrentlySelectedCube.CubeName);
                        currentMemberList = MemberLoaderCache.Instance.GetMemberCollection(getMembersQuery.GetMemberSelectQuery(), CurrentlySelectedCube.ParentConnection.Connection);

                        MinDate = currentMemberList.First().Item1;
                        MaxDate = currentMemberList.Last().Item1;
                        SelectedTick = 0;
                        MaxTickCount = currentMemberList.Count - 1;
                        break;
                    }
                case("Vertical"):
                    {
                        SelectMDXObject getMembersQuery = new SelectMDXObject(SelectedHierarchyFilterLevel.UniqueName, CurrentlySelectedCube.CubeName);
                        currentHierarchyFilterMemberList = MemberLoaderCache.Instance.GetMemberCollection(getMembersQuery.GetMemberSelectQuery(), CurrentlySelectedCube.ParentConnection.Connection);

                        MinHierarchyFilter = currentHierarchyFilterMemberList.First().Item1;
                        MaxHierarchyFilter = currentHierarchyFilterMemberList.Last().Item1;
                        SelectedHierarchyFilterTick = 0;
                        MaxHierarchyFilterTickCount = currentHierarchyFilterMemberList.Count - 1;
                        break;
                    }
            }
            
        }

        /// <summary>
        /// Filter action handler. Based on selected properties, updates a filter list passed to the action controller to filter by.
        /// If no filters exist a simple plot is done.
        /// </summary>
        private void FilterGraphAction()
        {
            
            if(Filters == null) Filters = new List<FilterMDXObject>();
            FilterMDXObject filter;
            Filters.Clear();

            if(currentMemberList != null && currentMemberList.Count > 0)
            {
                filter = new FilterMDXObject(currentMemberList[SelectedTick].Item2, MDXFilterTypes.Equal);
                Filters.Add(filter);
            }

            if(currentHierarchyFilterMemberList != null && currentHierarchyFilterMemberList.Count > 0)
            {
                filter = new FilterMDXObject(currentHierarchyFilterMemberList[SelectedHierarchyFilterTick].Item2, MDXFilterTypes.Equal);
                Filters.Add(filter);
            }

            if (Filters.Count == 0)
                PlotGraphAction();
            else
            {
                actionController.TreeMapFilterAction(Filters);
            }        
        } 

        /// <summary>
        /// The plot action event handler, if all needed items are selected by the user, a call to the plot method of the action controller is made.
        /// Updates other necessary collections and flags.
        /// </summary>
        private void PlotGraphAction()
        {
            if (CheckAllSelected())
            {
                actionController.PlotInitialGraph();

                if (SelectedDate != null)
                {
                    SelectedDateNotNullOrEmpty = true;
                    UpdateDateLevelCollection();
                }

                if (SelectedHierarchyFilter != null)
                {
                    SelectedHierarchyFilterNotNullOrEmpty = true;
                    UpdateHierarchyFilterLevelCollection();
                }

                Update3DConversionFlag();
                LevelUpEnabled = false;
                initialPlot = true;
            }
        }

        /// <summary>
        /// A method to load the members of a selected hierarchy.
        /// </summary>
        private void LoadMembersForHierarchyLevelSelected()
        {
            MemberLoaderCache.Instance.LoadMemberData(SelectedHierarchyFilterLevel.UniqueName,
                                                      CurrentlySelectedCube.CubeName,
                                                      CurrentlySelectedCube.ParentConnection.Connection);
        }

        /// <summary>
        /// A method to load the members of a selected hierarchy.
        /// </summary>
        private void LoadMembersForDateLevelSelected()
        {
            MemberLoaderCache.Instance.LoadMemberData(SelectedDateLevel.UniqueName,
                                                      CurrentlySelectedCube.CubeName,
                                                      CurrentlySelectedCube.ParentConnection.Connection);
        }

        /// <summary>
        /// A method to update the collection used by the horizontal slider.
        /// </summary>
        private void UpdateDateLevelCollection()
        {
            RangeObservableCollection<LevelInfo> temporaryCollection = new RangeObservableCollection<LevelInfo>();
            temporaryCollection.AddRange(SelectedDate.Levels);
            temporaryCollection.Remove(temporaryCollection.FirstOrDefault());
            DatesLevelCollection = temporaryCollection;
        }

        /// <summary>
        /// A method to update the collection used by the vertical slider.
        /// </summary>
        private void UpdateHierarchyFilterLevelCollection()
        {
            RangeObservableCollection<LevelInfo> temporaryCollection = new RangeObservableCollection<LevelInfo>();
            temporaryCollection.AddRange(SelectedHierarchyFilter.Levels);
            temporaryCollection.Remove(temporaryCollection.FirstOrDefault());
            HierarchyFilterLevelCollection = temporaryCollection;
        }

        /// <summary>
        /// A helper method to update the conversion allowance flag - if both vertical and horizontal hierarchies are selected - true, else - false.
        /// </summary>
        private void Update3DConversionFlag()
        {
            if(SelectedDate != null && SelectedHierarchyFilter != null)
            {
                ConvertTo3DEnabled = true;
            }
            else
            {
                ConvertTo3DEnabled = false;
            }
        }

        #endregion Private Helpers Methods and Actions

        #region Member Viewer Popup
        // When button is clicked, popup child view is initialised.
        private void OnRaiseBrowseDateMember()
        {
            if(SelectedDateLevel != null)
            {
                eventAggregator.GetEvent<MemberBrowseEvent>().Publish(new Tuple<string, string, ConnectionInfo>(this.SelectedDateLevel.UniqueName, this.CurrentlySelectedCube.CubeName, CurrentlySelectedCube.ParentConnection.Connection));
                BrowseMemberRequest.Raise(new Notification { Title = "Member Viewer" });
            }

        }

        private void OnRaiseBrowseHierarchyMember()
        {
            if (SelectedHierarchyFilterLevel != null)
            {
                eventAggregator.GetEvent<MemberBrowseEvent>().Publish(new Tuple<string, string, ConnectionInfo>(this.SelectedHierarchyFilterLevel.UniqueName, this.CurrentlySelectedCube.CubeName, CurrentlySelectedCube.ParentConnection.Connection));
                BrowseMemberRequest.Raise(new Notification { Title = "Member Viewer" });
            }

        }
        #endregion

        #region Filter Side Menu

        public IList<FilterMDXObject> Filters; 

        private ObservableCollection<FilterItem> filterItems;
        public ObservableCollection<FilterItem> FilterItems
        {
            get { return this.filterItems; }
            set
            {
                this.filterItems = value;
                RaisePropertyChanged(() => FilterItems);
            }
        }

        // Remove Filter Item
        private void RemoveFilterItemAction(FilterItem item)
        {
            FilterItems.Remove(item);
        }

        // Add Filter Item
        private void AddFilterItemAction()
        {
            FilterItems.Add(new FilterItem());
        }

        #endregion Filter SideMenu
    }
}
