﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Data;
using OlapVisi.DataLayer.MDX;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// The main view model that is connected to the Cube3D view. Collects all properties that the view binds to, as well as the logic for their population.
    /// </summary>
    public class Cube3DViewModel : NotificationObject, IViewModel
    {
        private readonly EventAggregator eventAggregator;
        private readonly CubeSelectionCacheSingleton cache;

        // Commands connecting to the UI buttons
        public ICommand MapCommand { get; set; }
        public ICommand DrillLevelCommand { get; set; }
        public ICommand DrillUpLevelCommand { get; set; }
        public ICommand SliceDiceCommand { get; set; }

        /// <summary>
        /// The initial view model constructor. Subscribes to events in the event aggregator coming from other modules.
        /// Initializes variables.
        /// </summary>
        public Cube3DViewModel()
        {
           eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
           eventAggregator.GetEvent<Hierarchy3DSelectedEvent>().Subscribe(Selected3DHierarchiesChanged);
           eventAggregator.GetEvent<Measure3DSelectedEvent>().Subscribe(Selected3DMeasuresChanged);
           eventAggregator.GetEvent<CubeSelectedEvent>().Subscribe(CubeSelected);
           eventAggregator.GetEvent<DataVisual3DSelectedEvent>().Subscribe(SelectedDataVisual);
           eventAggregator.GetEvent<FilterVisual3DSelectedEvent>().Subscribe(SelectedFilterVisual);
           eventAggregator.GetEvent<LevelVisual3DSelectedEvent>().Subscribe(SelectedLevelVisual);
           eventAggregator.GetEvent<Convert2D3DEvent>().Subscribe(Convert2Dto3D);

            currentXLevelNumber = 1;
            currentYLevelNumber = 1;
            currentZLevelNumber = 1;

            filtersToApplyXAxis = new List<FilterMDXObject>();
            filtersToApplyYAxis = new List<FilterMDXObject>();
            filtersToApplyZAxis = new List<FilterMDXObject>();

           cache = CubeSelectionCacheSingleton.Instance;
           DataIsMapped = false;

           MapCommand = new DelegateCommand(InitialMapAction);
           DrillLevelCommand = new DelegateCommand(DrillLevelAction);
           DrillUpLevelCommand = new DelegateCommand(DrillUpLevelAction);
            SliceDiceCommand = new DelegateCommand(SliceDiceAction);
        }

        #region Current Cube

        /// <summary>
        /// On a cube change, an event is published that updates the currently selected cube intance in the view model.
        /// </summary>
        /// <param name="cube">New cube instance</param>
        private void CubeSelected(CubeInfoViewModel cube)
        {
            CurrentlySelectedCube = cube;
        }

        /// <summary>
        /// Currently selected cube property.
        /// </summary>
        private CubeInfoViewModel currentlySelectedCube;
        public CubeInfoViewModel CurrentlySelectedCube
        {
            get { return this.currentlySelectedCube; }
            set
            {
                this.currentlySelectedCube = value;
                RaisePropertyChanged(() => CurrentlySelectedCube);
            }
        }

        #endregion Current Cube

        #region Selected Hierarchies Collection and Population Action

        /// <summary>
        /// Selected 3D Hierarchies property. Populates the appropriate list in the side menu.
        /// </summary>
        private ObservableCollection<HierarchyInfo> selected3DHierarchies;
        public ObservableCollection<HierarchyInfo> Selected3DHierarchies
        {
            get { return selected3DHierarchies; }
            set
            {
                selected3DHierarchies = value;
                RaisePropertyChanged(() => Selected3DHierarchies);
            }
        }

        /// <summary>
        /// Event action logic, on a change in the list (from the cube navigation module e.g. Right Click -> Add 3D Hierarchy), updates the appropriate property.
        /// </summary>
        /// <param name="selectionStatus"></param>
        public void Selected3DHierarchiesChanged(bool selectionStatus)
        {
            if (selectionStatus)
            {
                Selected3DHierarchies = new ObservableCollection<HierarchyInfo>(cache.GetSelected3DHierarchies());
            }
        }

        #endregion Selected Hierarchies Collection and Population Action

        #region Selected Measures Collection and Population Action

        /// <summary>
        /// Holds collection that updates the selected 3D measures list on the side menu.
        /// </summary>
        private ObservableCollection<MeasureInfo> selected3DMeasures;
        public ObservableCollection<MeasureInfo> Selected3DMeasures
        {
            get { return selected3DMeasures; }
            set
            {
                selected3DMeasures = value;
                RaisePropertyChanged(() => Selected3DMeasures);
            }
        }

        /// <summary>
        /// Event action logic, on a change in the list (from the cube navigation module e.g. Right Click -> Add 3D Measure), updates the appropriate collection property.
        /// </summary>
        /// <param name="selectionStatus"></param>
        public void Selected3DMeasuresChanged(bool selectionStatus)
        {
            if (selectionStatus)
            {
                Selected3DMeasures = new ObservableCollection<MeasureInfo>(cache.GetSelected3DMeasures());
            }
        }


        #endregion Selected Measures Collection and Population Action

        #region Selected X,Y,Z Axes

        /// <summary>
        /// Selected hierarchy for X axis. Selection is made from the items in the Selected 3D Hierarchy list.
        /// Updates the currently selected level of the hierarchy to be the top one.
        /// </summary>
        private HierarchyInfo selectedElX;
        public HierarchyInfo SelectedElX
        {
            get
            {
                return selectedElX;
            }
            set
            {
                selectedElX = value;
                RaisePropertyChanged(() => SelectedElX);
                CurrentLevelElX = selectedElX.Levels[currentXLevelNumber];
            }
        }

        /// <summary>
        /// Selected hierarchy for Y axis. Selection is made from the items in the Selected 3D Hierarchy list.
        /// Updates the currently selected level of the hierarchy to be the top one.
        /// </summary>
        private HierarchyInfo selectedElY;
        public HierarchyInfo SelectedElY
        {
            get
            {
                return selectedElY;
            }
            set
            {
                selectedElY = value;
                RaisePropertyChanged(() => SelectedElY);
                CurrentLevelElY = selectedElY.Levels[currentYLevelNumber];
            }
        }

        /// <summary>
        /// Selected hierarchy for Z axis. Selection is made from the items in the Selected 3D Hierarchy list.
        /// Updates the currently selected level of the hierarchy to be the top one.
        /// </summary>
        private HierarchyInfo selectedElZ;
        public HierarchyInfo SelectedElZ
        {
            get
            {
                return selectedElZ;
            }
            set
            {
                selectedElZ = value;
                RaisePropertyChanged(() => SelectedElZ);
                CurrentLevelElZ = selectedElZ.Levels[currentZLevelNumber];
            }
        }

        #endregion Selected X,Y,Z Axes

        #region Current X,Y,Z Data Levels

        /// <summary>
        /// Property that holds the currently selected level number for the X axis as an integer.
        /// When this is changed in any way, the currently selected level us updated.
        /// The method to rerender the 3D visualization on the screen is called.
        /// </summary>
        private int currentXLevelNumber;
        public int CurrentXLevelNumber
        {
            get
            {
                return currentXLevelNumber;
            }
            set
            {
                currentXLevelNumber = value;
                RaisePropertyChanged(() => CurrentXLevelNumber);
                CurrentLevelElX = selectedElX.Levels[currentXLevelNumber];
                MapAction();
            }
        }

        /// <summary>
        /// Property that holds the currently selected level number for the Y axis as an integer.
        /// When this is changed in any way, the currently selected level us updated.
        /// The method to rerender the 3D visualization on the screen is called.
        /// </summary>
        private int currentYLevelNumber;
        public int CurrentYLevelNumber
        {
            get
            {
                return currentYLevelNumber;
            }
            set
            {
                currentYLevelNumber = value;
                RaisePropertyChanged(() => CurrentYLevelNumber);
                CurrentLevelElY = selectedElY.Levels[currentYLevelNumber];
                MapAction();
            }
        }

        /// <summary>
        /// Property that holds the currently selected level number for the Z axis as an integer.
        /// When this is changed in any way, the currently selected level us updated.
        /// The method to rerender the 3D visualization on the screen is called.
        /// </summary>
        private int currentZLevelNumber;
        public int CurrentZLevelNumber
        {
            get
            {
                return currentZLevelNumber;
            }
            set
            {
                currentZLevelNumber = value;
                RaisePropertyChanged(() => CurrentZLevelNumber);
                CurrentLevelElZ = selectedElZ.Levels[currentZLevelNumber];
                MapAction();
            }
        }


        /// <summary>
        /// The current X level object property.
        /// </summary>
        private LevelInfo currentLevelElX;
        public LevelInfo CurrentLevelElX
        {
            get
            {
                return currentLevelElX;
            }
            set
            {
                currentLevelElX = value;
                RaisePropertyChanged(() => CurrentLevelElX);
            }
        }

        /// <summary>
        /// The current Y level object property.
        /// </summary>
        private LevelInfo currentLevelElY;
        public LevelInfo CurrentLevelElY
        {
            get
            {
                return currentLevelElY;
            }
            set
            {
                currentLevelElY = value;
                RaisePropertyChanged(() => CurrentLevelElY);  
            }
        }

        /// <summary>
        /// The current Z level object property.
        /// </summary>
        private LevelInfo currentLevelElZ;
        public LevelInfo CurrentLevelElZ
        {
            get
            {
                return currentLevelElZ;
            }
            set
            {
                currentLevelElZ = value;
                RaisePropertyChanged(() => CurrentLevelElZ);
            }
        }

        #endregion Current X,Y,Z Data Levels

        #region Selected M1, M2 Data Measures, Measures checked flag

        /// <summary>
        /// The currently selected first measure property (colour).
        /// </summary>
        private MeasureInfo selectedMeasureOne;
        public MeasureInfo SelectedMeasureOne
        {
            get
            {
                return selectedMeasureOne;
            }
            set
            {
                selectedMeasureOne = value;
                RaisePropertyChanged(() => SelectedMeasureOne);
            }
        }

        /// <summary>
        /// The currently selected second measure property (size).
        /// </summary>
        private MeasureInfo selectedMeasureTwo;
        public MeasureInfo SelectedMeasureTwo
        {
            get
            {
                return selectedMeasureTwo;
            }
            set
            {
                selectedMeasureTwo = value;
                RaisePropertyChanged(() => SelectedMeasureTwo);
            }
        }

        /// <summary>
        /// Flag for one measure selected.
        /// </summary>
        private bool oneMeasureChecked;
        public bool OneMeasureChecked
        {
            get
            {
                return oneMeasureChecked;
            }
            set
            {
                oneMeasureChecked = value;
                if(value) SelectedMeasureTwo = null;
                RaisePropertyChanged(() => OneMeasureChecked);
            }
        }

        /// <summary>
        /// Flag for two measures selected.
        /// </summary>
        private bool twoMeasuresChecked;
        public bool TwoMeasuresChecked
        {
            get
            {
                return twoMeasuresChecked;
            }
            set
            {
                twoMeasuresChecked = value;
                OneMeasureChecked = false;
                RaisePropertyChanged(() => TwoMeasuresChecked);
            }
        } 

        #endregion Selected M1, M2 Data Measures

        #region Graph Element Counts and Side Plane Centers

        /// <summary>
        /// X, Y, Z properties for the graph element counts.
        /// </summary>
        private long elXCount;
        public long ElXCount
        {
            get
            {
                return elXCount;
            }
            set
            {
                elXCount = value;
                RaisePropertyChanged(() => ElXCount);
            }
        }

        private long elYCount;
        public long ElYCount
        {
            get
            {
                return elYCount;
            }
            set
            {
                elYCount = value;
                RaisePropertyChanged(() => ElYCount);
            }
        }

        private long elZCount;
        public long ElZCount
        {
            get
            {
                return elZCount;
            }
            set
            {
                elZCount = value;
                RaisePropertyChanged(() => ElZCount);
            }
        }

        private Point3D centerA;
        private Point3D centerB;
        private Point3D centerC;

        /// <summary>
        /// Center point properties for the three side grids of the graph.
        /// </summary>
        public Point3D CenterA
        {
            get
            {
                return centerA;
            }
            set
            {
                centerA = value;
                RaisePropertyChanged(() => CenterA);
            }
        }
        public Point3D CenterB
        {
            get
            {
                return centerB;
            }
            set
            {
                centerB = value;
                RaisePropertyChanged(() => CenterB);
            }
        }
        public Point3D CenterC
        {
            get
            {
                return centerC;
            }
            set
            {
                centerC = value;
                RaisePropertyChanged(() => CenterC);
            }
        }

        #endregion Graph Element Counts and Side Plane Centers

        #region Data Elements
        /// <summary>
        /// The main data element collection of data visual objects that UI is bound to. Rerender of elements happens when this collection changes.
        /// Current bottleneck of visualisation - binding takes too long, due to framework inefficiencies.
        /// Rework needed - incrementally populate and render items. Examples in the Helix Toolkit Samples project.
        /// </summary>
        private IList<DataVisual> dataVisuals;
        public IList<DataVisual> DataVisuals
        {
            get { return dataVisuals; }
            set
            {
                dataVisuals = value;
                RaisePropertyChanged("DataVisuals");
            }
        }

        /// <summary>
        /// Currently selected data item (visual) from collection, retrieves the label triangulation to be displayed in the UI legend.
        /// </summary>
        private DataVisual currentlySelectedVisual;
        public DataVisual CurrentlySelectedVisual
        {
            get { return currentlySelectedVisual; }
            set
            {
                currentlySelectedVisual = value;    
                RaisePropertyChanged(() => CurrentlySelectedVisual);

                if (currentlySelectedVisual != null)
                {
                    SelectedXPosition = XAxisLabels[(int)(currentlySelectedVisual.Position.X - 0.5)].Text;
                    SelectedYPosition = YAxisLabels[(int)(currentlySelectedVisual.Position.Y - 0.5)].Text;
                    SelectedZPosition = ZAxisLabels[(int)(currentlySelectedVisual.Position.Z + 0.25)].Text;
                }
            }
        }

        /// <summary>
        /// Action which handles the selection of a data visual event.
        /// Two types of handling is needed.
        /// First case: Visual is already selected and same visual is clicked. This is the same as resetting the collection, all visual are set to selected.
        /// Second case: No visual selected and some visual is clicked. All visuals that have a center different to the one passed into the method are deselected.
        /// </summary>
        /// <param name="center">Consumes a center point, which describes the element.</param>
        private void SelectedDataVisual(Point3D center)
        {
            if(DataVisuals != null)
            {
                if (CurrentlySelectedVisual != null && CurrentlySelectedVisual.Position == center)
                {
                    foreach (var visual in DataVisuals)
                    {
                        visual.IsSelected = true;
                    }

                    CurrentlySelectedVisual = null;
                    VisualIsSelected = false;
                }
                else
                {
                    foreach (var visual in DataVisuals)
                    {
                        if (visual.Position != center)
                        {
                            visual.IsSelected = false;
                        }
                        else
                        {
                            visual.IsSelected = true;
                            CurrentlySelectedVisual = visual;
                            VisualIsSelected = true;
                        }
                    } 
                }    
            }   
        }

        #endregion Data Elements

        #region Graph Labels

        /// <summary>
        /// X, Y, Z label arrays of SpatialTextItems (HelixToolkit type - see HelixToolkit Samples project for example)
        /// </summary>
        private SpatialTextItem[] xAxisLabels;
        public SpatialTextItem[] XAxisLabels
        {
            get { return xAxisLabels; }
            set
            {
                xAxisLabels = value;
                RaisePropertyChanged(() => XAxisLabels);
            }
        }

        private SpatialTextItem[] yAxisLabels;
        public SpatialTextItem[] YAxisLabels
        {
            get { return yAxisLabels; }
            set
            {
                yAxisLabels = value;
                RaisePropertyChanged(() => YAxisLabels);
            }
        }

        private SpatialTextItem[] zAxisLabels;
        public SpatialTextItem[] ZAxisLabels
        {
            get { return zAxisLabels; }
            set
            {
                zAxisLabels = value;
                RaisePropertyChanged(() => ZAxisLabels);
            }
        }

        /// <summary>
        /// Label position and lookup dictionaries, for faster access to specific labels.
        /// </summary>
        private Dictionary<string, int> labelPositionDict;
        private Dictionary<string, string> labelUniqueNameLookupDict;

        #endregion Graph Labels

        #region Legend Elements

        /// <summary>
        /// Gradient brush that is bound to the legend colour element. This is important as the colour measure of the elements are set according and
        /// synchronised with the colours in this legend.
        /// </summary>
        private GradientBrush legendColor;
        public GradientBrush LegendColor
        {
            get { return legendColor; }
            set
            {
                legendColor = value;
                RaisePropertyChanged(() => LegendColor);
            }
        }

        /// <summary>
        /// Minimum value of the legend (lowest point).
        /// </summary>
        private double legendMin;
        public double LegendMin
        {
            get { return legendMin; }
            set
            {
                legendMin = value;
                RaisePropertyChanged(() => LegendMin);
            }
        }

        /// <summary>
        /// Maximum value of the legend (highest point).
        /// </summary>
        private double legendMax;
        public double LegendMax
        {
            get { return legendMax; }
            set
            {
                legendMax = value;
                RaisePropertyChanged(() => LegendMax);
            }
        }

        /// <summary>
        /// A step for the change in the colour of the legend.
        /// </summary>
        private double legendStep;
        public double LegendStep
        {
            get { return legendStep; }
            set
            {
                legendStep = value;
                RaisePropertyChanged(() => LegendStep);
            }
        }

        #endregion Legend Elements

        #region Filter Elements

        /// <summary>
        /// X, Y, Z filter visual collections (small spheres next to labels in graph on the UI bind to these collections)
        /// </summary>
        private IList<FilterVisual> xAxisFilters;
        public IList<FilterVisual> XAxisFilters
        {
            get { return xAxisFilters; }
            set
            {
                xAxisFilters = value;
                RaisePropertyChanged(() => XAxisFilters);
            }
        }

        private IList<FilterVisual> yAxisFilters;
        public IList<FilterVisual> YAxisFilters
        {
            get { return yAxisFilters; }
            set
            {
                yAxisFilters = value;
                RaisePropertyChanged(() => YAxisFilters);
            }
        }

        private IList<FilterVisual> zAxisFilters;
        public IList<FilterVisual> ZAxisFilters
        {
            get { return zAxisFilters; }
            set
            {
                zAxisFilters = value;
                RaisePropertyChanged(() => ZAxisFilters);
            }
        }

        /// <summary>
        /// X, Y, Z collections of filter objects. These are all consolidated into the filterToApply collection
        /// </summary>
        private IList<FilterMDXObject> filtersToApply;
        private IList<FilterMDXObject> filtersToApplyXAxis;
        private IList<FilterMDXObject> filtersToApplyYAxis;
        private IList<FilterMDXObject> filtersToApplyZAxis;
        private string currentDimensionToFilter = "";

        /// <summary>
        /// Helper method to convert a list of Filter Visual objects into FilterMDXObjects for use in the Selection query.
        /// </summary>
        /// <param name="axisFilters"></param>
        private void PopulateFilterObjects(IList<FilterVisual> axisFilters)
        {
            // Retrieves dimension that is to be filtered from the given axis filter collection.
            currentDimensionToFilter = axisFilters.FirstOrDefault().Dimension;

            List<string> temporaryFilterList = new List<string>();

            // Adds all selected filters to a temporary collection.
            foreach (var filter in axisFilters)
            {
                if(filter.IsSelected)
                {
                    temporaryFilterList.Add(filter.UniqueName);
                }
            }

            // Based on the dimension to be filtered, creates and adds a filter object to the appropriate X, Y or Z filter collection.
            switch(currentDimensionToFilter)
            {
                case("X"):
                    filtersToApplyXAxis.Add(new FilterMDXObject(temporaryFilterList,"Equals"));
                    break;
                case("Y"):
                    filtersToApplyYAxis.Add(new FilterMDXObject(temporaryFilterList, "Equals"));
                    break;
                case("Z"):
                    filtersToApplyZAxis.Add(new FilterMDXObject(temporaryFilterList, "Equals"));
                    break;
            }

            // Consolidate filters into one collection.
            AddFilterRanges();
        }

        /// <summary>
        /// On a selection of a filter visual, based on its X, Y or Z dimension, processes the filter at a specified Point in 3D space.
        /// </summary>
        /// <param name="tuple">Tuple of a center point of the filter and the dimension, which it occupies.</param>
        private void SelectedFilterVisual(Tuple<Point3D, string> tuple)
        {

            switch (tuple.Item2)
            {
                case ("X"):
                    SelectXAxisFilterVisual(tuple.Item1);
                    break;
                case ("Y"):
                    SelectYAxisFilterVisual(tuple.Item1);
                    break;
                case ("Z"):
                    SelectZAxisFilterVisual(tuple.Item1);
                    break;
            }
        }

        private int selectedXFiltersCount = 0;
        private int selectedYFiltersCount = 0;
        private int selectedZFiltersCount = 0;

        /// <summary>
        /// The main behaviour of the X, Y, Z SelectAxisFilterVisual helper methods:
        /// All filters are initially selected. On specific filter visual selected, all other filters are deselected for the dimension.
        /// Additional filters can also be selected, this increases the count for the dimension.
        /// When deselecting, if the final selected filter is deselected, the filters are reset (so all are selected again.)
        /// </summary>
        /// <param name="center"></param>
        private void SelectXAxisFilterVisual(Point3D center)
        {
            if (XAxisFilters != null)
            {
                if (selectedXFiltersCount == 0)
                {
                    foreach (var filter in XAxisFilters)
                    {
                        if (filter.Position == center)
                        {
                            selectedXFiltersCount++;
                            filter.IsSelected = true;
                        }
                        else
                        {
                            filter.IsSelected = false;
                        }
                    }
                }
                else if (selectedXFiltersCount == 1)
                {
                    foreach (var filter in XAxisFilters)
                    {
                        if (filter.Position == center && filter.IsSelected)
                        {
                            selectedXFiltersCount = 0;
                            ResetFilterSelection(this.XAxisFilters);
                            break;
                        }
                        else if (filter.Position == center)
                        {
                            if (filter.IsSelected)
                                selectedXFiltersCount--;
                            else
                            {
                                selectedXFiltersCount++;
                            }
                            filter.SwitchSelection();
                        }
                    }
                }
                else
                {
                    foreach (var filter in XAxisFilters)
                    {
                        if (filter.Position == center && filter.IsSelected)
                        {
                            filter.SwitchSelection();
                            selectedXFiltersCount--;
                            break;
                        }

                        if (filter.Position == center)
                        {
                            filter.SwitchSelection();
                            selectedXFiltersCount++;
                            if (selectedXFiltersCount == ElXCount)
                                selectedXFiltersCount = 0;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// See above.
        /// </summary>
        /// <param name="center"></param>
        private void SelectYAxisFilterVisual(Point3D center)
        {
            if (YAxisFilters != null)
            {
                if (selectedYFiltersCount == 0)
                {
                    foreach (var filter in YAxisFilters)
                    {
                        if (filter.Position == center)
                        {
                            selectedYFiltersCount++;
                            filter.IsSelected = true;
                        }
                        else
                        {
                            filter.IsSelected = false;
                        }
                    }
                }
                else if (selectedYFiltersCount == 1)
                {
                    foreach (var filter in YAxisFilters)
                    {
                        if (filter.Position == center && filter.IsSelected)
                        {
                            selectedYFiltersCount = 0;
                            ResetFilterSelection(this.YAxisFilters);
                            break;
                        }
                        else if (filter.Position == center)
                        {
                            if (filter.IsSelected)
                                selectedYFiltersCount--;
                            else
                            {
                                selectedYFiltersCount++;
                            }
                            filter.SwitchSelection();
                        }
                    }
                }
                else
                {
                    foreach (var filter in YAxisFilters)
                    {
                        if (filter.Position == center && filter.IsSelected)
                        {
                            filter.SwitchSelection();
                            selectedYFiltersCount--;
                            break;
                        }

                        if (filter.Position == center)
                        {
                            filter.SwitchSelection();
                            selectedYFiltersCount++;
                            if (selectedYFiltersCount == ElYCount)
                                selectedYFiltersCount = 0;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// See above.
        /// </summary>
        /// <param name="center"></param>
        private void SelectZAxisFilterVisual(Point3D center)
        {
            if (ZAxisFilters != null)
            {
                if (selectedZFiltersCount == 0)
                {
                    foreach (var filter in ZAxisFilters)
                    {
                        if (filter.Position == center)
                        {
                            selectedZFiltersCount++;
                            filter.IsSelected = true;
                        }
                        else
                        {
                            filter.IsSelected = false;
                        }
                    }
                }
                else if (selectedZFiltersCount == 1)
                {
                    foreach (var filter in ZAxisFilters)
                    {
                        if (filter.Position == center && filter.IsSelected)
                        {
                            selectedZFiltersCount = 0;
                            ResetFilterSelection(this.ZAxisFilters);
                            break;
                        }
                        else if (filter.Position == center)
                        {
                            if (filter.IsSelected)
                                selectedZFiltersCount--;
                            else
                            {
                                selectedZFiltersCount++;
                            }
                            filter.SwitchSelection();
                        }
                    }
                }
                else
                {
                    foreach (var filter in ZAxisFilters)
                    {
                        if (filter.Position == center && filter.IsSelected)
                        {
                            filter.SwitchSelection();
                            selectedZFiltersCount--;
                            break;
                        }

                        if (filter.Position == center)
                        {
                            filter.SwitchSelection();
                            selectedZFiltersCount++;
                            if (selectedZFiltersCount == ElZCount)
                                selectedZFiltersCount = 0;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Resets all filters (to selected status) of a passed in collection.
        /// </summary>
        /// <param name="filters"></param>
        private void ResetFilterSelection(IList<FilterVisual> filters)
        {
            foreach (var filter in filters)
            {
                filter.IsSelected = true;
            }
        }

        /// <summary>
        /// Consolidation method, moves all filter for application into one collection.
        /// </summary>
        private void AddFilterRanges()
        {
            ((List<FilterMDXObject>)filtersToApply).AddRange(filtersToApplyXAxis);
            ((List<FilterMDXObject>)filtersToApply).AddRange(filtersToApplyYAxis);
            ((List<FilterMDXObject>)filtersToApply).AddRange(filtersToApplyZAxis);
        }

        #endregion Filter Elements

        #region Information Panel Elements

        private bool dataIsMapped;
        public bool DataIsMapped
        {
            get { return dataIsMapped; }
            set
            {
                dataIsMapped = value;
                RaisePropertyChanged(() => DataIsMapped);
            }
        }

        /// <summary>
        /// Set to true whenever a visual element from the graph is selected. Used to dynamically show and hide the information panel on screen.
        /// (It is not needed to be shown if a data element visual is not selected.
        /// </summary>
        private bool visualIsSelected;
        public bool VisualIsSelected
        {
            get { return visualIsSelected; }
            set
            {
                visualIsSelected = value;
                RaisePropertyChanged(() => VisualIsSelected);
            }
        }

        /// <summary>
        /// X, Y, Z positions retrieved from the labels of the selected element. Properties displayed on the information panel of the UI.
        /// </summary>
        private string selectedXPostion;
        public string SelectedXPosition
        {
            get { return selectedXPostion; }
            set
            {
                selectedXPostion = value;
                RaisePropertyChanged(() => SelectedXPosition);
            }
        }

        private string selectedYPostion;
        public string SelectedYPosition
        {
            get { return selectedYPostion; }
            set
            {
                selectedYPostion = value;
                RaisePropertyChanged(() => SelectedYPosition);
            }
        }

        private string selectedZPostion;
        public string SelectedZPosition
        {
            get { return selectedZPostion; }
            set
            {
                selectedZPostion = value;
                RaisePropertyChanged(() => SelectedZPosition);
            }
        }

        #endregion Information Panel Elements

        #region Selected Level Axis Information Panel

        /// <summary>
        /// The currently selected X,Y,Z level visual (Big sphere at corner).
        /// </summary>
        private LevelVisual currentSelected3DLevelVisual;
        public LevelVisual CurrentSelected3DLevelVisual
        {
            get
            {
                return currentSelected3DLevelVisual;
            }
            set
            {
                currentSelected3DLevelVisual = value;
                RaisePropertyChanged(() => CurrentSelected3DLevelVisual);
            }
        }

        /// <summary>
        /// Set to true whenever one of the three dimension filter visuals (Big spheres) from the graph are selected.
        /// Used to dynamically show and hide the filter information panel with the possible OLAP actions to performed (buttons).
        /// (It is not needed to be shown if a filter level visual is not selected.)
        /// </summary>
        private bool levelVisualIsSelected;
        public bool LevelVisualIsSelected
        {
            get { return levelVisualIsSelected; }
            set
            {
                levelVisualIsSelected = value;
                RaisePropertyChanged(() => LevelVisualIsSelected);
            }
        }

        /// <summary>
        /// Action that handles the selection of a level visual (Big sphere).
        /// Logic is similar to the way data visuals are selected and deselected.
        /// Only one of these needs to be selected at any one time.
        /// </summary>
        /// <param name="center"></param>
        private void SelectedLevelVisual(Point3D center)
        {
            if (LevelVisuals != null)
            {
                if (CurrentSelected3DLevelVisual != null && CurrentSelected3DLevelVisual.Position == center)
                {
                    foreach (var level in LevelVisuals)
                    {
                        level.IsSelected = true;
                    }

                    CurrentSelected3DLevelVisual = null;
                    LevelVisualIsSelected = false;
                }
                else
                {
                    foreach (var level in LevelVisuals)
                    {
                        if (level.Position != center)
                        {
                            level.IsSelected = false;
                        }
                        else
                        {
                            level.IsSelected = true;
                            CurrentSelected3DLevelVisual = level;
                            LevelVisualIsSelected = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Drill Level action logic method.
        /// </summary>
        private void DrillLevelAction()
        {
            // Find which level visual (big sphere) is selected and retrieve its dimension.
            var dimension = CurrentSelected3DLevelVisual.Dimension;

            // A check is done to see if lowest level is reached. If it is, method returns without any change.
            // If it is not, the filter visuals are converted to filtermdxobjects through the PopulateFilterObjects method.
            // The level number is increased, this calls the MapAction() that rerenders the elements on the screen.
            switch(dimension)
            {
                case("X"):
                    if (CurrentXLevelNumber == (SelectedElX.Levels.Count - 1))
                        return;
                    else
                    {
                        PopulateFilterObjects(XAxisFilters);
                        CurrentXLevelNumber++;
                        break;
                    }
                case("Y"):
                    if (CurrentYLevelNumber == (SelectedElY.Levels.Count - 1))
                        return;
                    else
                    {
                        PopulateFilterObjects(YAxisFilters);
                        CurrentYLevelNumber++;
                        break;
                    }
                case("Z"):
                    if (CurrentZLevelNumber == (SelectedElZ.Levels.Count - 1))
                        return;
                    else
                    {
                        PopulateFilterObjects(ZAxisFilters);
                        CurrentZLevelNumber++;
                        break;
                    }
            }
        }

        /// <summary>
        /// Drilling Up in levels action logic method.
        /// </summary>
        private void DrillUpLevelAction()
        {
            var dimension = CurrentSelected3DLevelVisual.Dimension;
            // If the current level is 1 (top level), refresh the filters list and remap and rerender the elements on screen.
            // If it isn't, remove the last applied filter object, refresh the filter ranges and decrease the level number.
            // The decrease rerenders the elemens on screen.
            switch (dimension)
            {
                case ("X"):
                    if (CurrentXLevelNumber == 1)
                    {
                        filtersToApplyXAxis = new List<FilterMDXObject>();
                        AddFilterRanges();
                        MapAction();
                        return;
                    }   
                    else
                    {
                        filtersToApplyXAxis.RemoveAt(filtersToApplyXAxis.IndexOf(filtersToApplyXAxis.Last()));
                        AddFilterRanges();
                        CurrentXLevelNumber--;
                        break;
                    }
                case ("Y"):
                    if (CurrentYLevelNumber == 1)
                    {
                        filtersToApplyYAxis = new List<FilterMDXObject>();
                        AddFilterRanges();
                        MapAction();
                        return;
                    }         
                    else
                    {
                        filtersToApplyYAxis.RemoveAt(filtersToApplyYAxis.IndexOf(filtersToApplyYAxis.Last()));
                        AddFilterRanges();
                        CurrentYLevelNumber--;
                        break;
                    }
                case ("Z"):
                    if (CurrentZLevelNumber == 1)
                    {
                        filtersToApplyZAxis = new List<FilterMDXObject>();
                        AddFilterRanges();
                        MapAction();
                        return;
                    }
                    else
                    {
                        filtersToApplyZAxis.RemoveAt(filtersToApplyZAxis.IndexOf(filtersToApplyZAxis.Last()));
                        AddFilterRanges();
                        CurrentZLevelNumber--;
                        break;
                    }
            }
        }

        /// <summary>
        /// The slice and dice action logic. Converts the selected filter visuals into filtermdxobjects. Remaps and rerenders the elements on screen.
        /// </summary>
        private void SliceDiceAction()
        {
            var dimension = CurrentSelected3DLevelVisual.Dimension;
            switch (dimension)
            {
                case ("X"):
                    {
                        PopulateFilterObjects(XAxisFilters);
                        MapAction();
                        break;
                    }
                case ("Y"):
                    {
                        PopulateFilterObjects(YAxisFilters);
                        MapAction();
                        break;
                    }
                case ("Z"):
                    {
                        PopulateFilterObjects(ZAxisFilters);
                        MapAction();
                        break;
                    }
            }
        }
        /// <summary>
        /// The collection of level visuals (Big spheres) that serve to display the appropriate filter panel on screen based on the dimension.
        /// </summary>
        private IList<LevelVisual> levelVisuals;
        public IList<LevelVisual> LevelVisuals
        {
            get { return levelVisuals; }
            set
            {
                levelVisuals = value;
                RaisePropertyChanged(() => LevelVisuals);
            }
        }

        #endregion Selected Level Axis Information Panel

        #region Object Creation and Data Mapping Helper Methods

        /// <summary>
        /// Initial map action to display top levels of hierarchies without any filters applied.
        /// </summary>
        private void InitialMapAction()
        {
            currentXLevelNumber = 1;
            currentYLevelNumber = 1;
            currentZLevelNumber = 1;

            CurrentLevelElX = selectedElX.Levels[currentXLevelNumber];
            CurrentLevelElY = selectedElY.Levels[currentYLevelNumber];
            CurrentLevelElZ = selectedElZ.Levels[currentZLevelNumber];

            filtersToApplyXAxis = new List<FilterMDXObject>();
            filtersToApplyYAxis = new List<FilterMDXObject>();
            filtersToApplyZAxis = new List<FilterMDXObject>();

            MapAction();
        }

        /// <summary>
        /// Method contains main logic for populating necessary collections and rendering elements on screen.
        /// </summary>
        private void MapAction()
        {
            // Retrieve the members for each of the currently selected levels (for each dimension).
            MemberLoaderCache.Instance.LoadMemberData(CurrentLevelElX.UniqueName,
                                                      CurrentlySelectedCube.CubeName,
                                                      CurrentlySelectedCube.ParentConnection.Connection, filtersToApplyXAxis.ToList());
            MemberLoaderCache.Instance.LoadMemberData(CurrentLevelElY.UniqueName,
                                                      CurrentlySelectedCube.CubeName,
                                                      CurrentlySelectedCube.ParentConnection.Connection, filtersToApplyYAxis.ToList());
            MemberLoaderCache.Instance.LoadMemberData(CurrentLevelElZ.UniqueName,
                                                      CurrentlySelectedCube.CubeName,
                                                      CurrentlySelectedCube.ParentConnection.Connection, filtersToApplyZAxis.ToList());

            // Check if any filters will be applied. If yes, retrieves members with the filter applied and the member count.
            // If no, the member count is the level member count. Similar for Y and Z filter checks.
            if(filtersToApplyXAxis.Count > 0)
            {
                var memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElX.UniqueName,
                                                                      CurrentlySelectedCube.CubeName,
                                                                      CurrentlySelectedCube.ParentConnection.Connection, filtersToApplyXAxis.ToList());
                ElXCount = memberCol.Count;
            }
            else
            {
                ElXCount = currentLevelElX.MemberCount - 1;
            }

            if(filtersToApplyYAxis.Count > 0)
            {
                var memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElY.UniqueName,
                                                                      CurrentlySelectedCube.CubeName,
                                                                      CurrentlySelectedCube.ParentConnection.Connection, filtersToApplyYAxis.ToList());
                ElYCount = memberCol.Count;
            }
            else
            {
                ElYCount = currentLevelElY.MemberCount - 1;
            }

            if (filtersToApplyZAxis.Count > 0)
            {
                var memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElZ.UniqueName,
                                                                      CurrentlySelectedCube.CubeName,
                                                                      CurrentlySelectedCube.ParentConnection.Connection, filtersToApplyZAxis.ToList());
                ElZCount = memberCol.Count;
            }
            else
            {
                ElZCount = currentLevelElZ.MemberCount - 1;
            }

            // Sets center points for the three grids (X, Y, Z).
            CenterA = new Point3D((double)elXCount / 2, 0, (double)elZCount / 2);
            CenterB = new Point3D(0, (double)elYCount / 2, (double)elZCount / 2);
            CenterC = new Point3D((double)elXCount / 2, (double)elYCount / 2, 0);

            // Resets the label position lookup dictionary.
            if(labelPositionDict != null)
                labelPositionDict.Clear();

            PopulateMemberLabels();
            PopulateLevelLabels();
            PopulateDataObjects();
        }

        /// <summary>
        /// Method used to populate the collections of the labels along the sides of the three grids along with the filters (unique names) that will
        /// be used in the filtering process later.
        /// </summary>
        private void PopulateMemberLabels()
        {
            // Initializes lookup dictionaries, temporary label arrays and filter collection.
            labelPositionDict = new Dictionary<string, int>();
            labelUniqueNameLookupDict = new Dictionary<string, string>();

            SpatialTextItem[] temps = new SpatialTextItem[elXCount];
            List<FilterVisual> filters = new List<FilterVisual>();
            
            // Logic for X dimension labels and filters.
            double x = 0.25;
            double y = elYCount + 0.5;

            List<Tuple<string, string>> memberCol;

            // Retrieve a collection of tuples - Plain text & Unique Name used for filtering.
            if (filtersToApplyXAxis.Count > 0)
            {
                memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElX.UniqueName,
                                                                               CurrentlySelectedCube.CubeName,
                                                                               CurrentlySelectedCube.ParentConnection.
                                                                                   Connection,
                                                                               filtersToApplyXAxis.ToList());
            }
            else
            {
                memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElX.UniqueName,
                                                                      CurrentlySelectedCube.CubeName,
                                                                      CurrentlySelectedCube.ParentConnection.Connection);
            }
            
            // Populate the labels and filters in appropriate collections.
            int index = 0;
            foreach (var member in memberCol)
            {
                temps[index] = (new SpatialTextItem { Text = member.Item1, Position = new Point3D(x, y, 0), TextDirection = new Vector3D(0, 1, 0), UpDirection = new Vector3D(-1, 0, 0) });
                filters.Add(new FilterVisual(new Point3D(x + 0.25, y - 0.25, 0), "X", member.Item2));

                // Update lookup dictionaries and continue on.
                if(!labelPositionDict.ContainsKey(member.Item1))
                    labelPositionDict.Add(member.Item1, index);
                if(!labelUniqueNameLookupDict.ContainsKey(member.Item1))
                    labelUniqueNameLookupDict.Add(member.Item1, member.Item2);
                x += 1;
                index++;

                if (x > elXCount) break;
            }

            // Populate the properties bound to the UI.
            this.XAxisLabels = temps;
            this.XAxisFilters = filters;
            this.selectedXFiltersCount = 0;

            // Y dimension logic. Similar to X.
            temps = new SpatialTextItem[elYCount];
            filters = new List<FilterVisual>();
            x = elXCount + 0.5;
            y = 0.25;

            if (filtersToApplyYAxis.Count > 0)
            {
                memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElY.UniqueName,
                                                                               CurrentlySelectedCube.CubeName,
                                                                               CurrentlySelectedCube.ParentConnection.
                                                                                   Connection,
                                                                               filtersToApplyYAxis.ToList());
            }
            else
            {
                memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElY.UniqueName,
                                                                      CurrentlySelectedCube.CubeName,
                                                                      CurrentlySelectedCube.ParentConnection.Connection);
            }

            index = 0;
            foreach (var member in memberCol)
            {
                temps[index] = (new SpatialTextItem { Text = member.Item1, Position = new Point3D(x, y, 0), TextDirection = new Vector3D(1, 0, 0), UpDirection = new Vector3D(0, -1, 0) });
                filters.Add(new FilterVisual(new Point3D(x - 0.25, y + 0.25, 0), "Y", member.Item2));

                if (!labelPositionDict.ContainsKey(member.Item1))
                    labelPositionDict.Add(member.Item1, index);
                if (!labelUniqueNameLookupDict.ContainsKey(member.Item1))
                    labelUniqueNameLookupDict.Add(member.Item1, member.Item2);
                y += 1;
                index++;

                if (y > elYCount) break;
            }

            this.YAxisLabels = temps;
            this.YAxisFilters = filters;
            this.selectedYFiltersCount = 0;

            // Z dimension logic. Similar to Y and X.
            temps = new SpatialTextItem[elZCount];
            filters = new List<FilterVisual>();
            double z = 0.75;
            y = elYCount + 0.5;

            if (filtersToApplyZAxis.Count > 0)
            {
                memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElZ.UniqueName,
                                                                               CurrentlySelectedCube.CubeName,
                                                                               CurrentlySelectedCube.ParentConnection.
                                                                                   Connection,
                                                                               filtersToApplyZAxis.ToList());
            }
            else
            {
                memberCol = MemberLoaderCache.Instance.GetMemberCollection(CurrentLevelElZ.UniqueName,
                                                                      CurrentlySelectedCube.CubeName,
                                                                      CurrentlySelectedCube.ParentConnection.Connection);
            }

            index = 0;
            foreach (var member in memberCol)
            {
                temps[index] = (new SpatialTextItem { Text = member.Item1, Position = new Point3D(0, y, z), TextDirection = new Vector3D(0, 1, 0), UpDirection = new Vector3D(0, 0, 1) });
                filters.Add(new FilterVisual(new Point3D(0, y - 0.25, z - 0.25), "Z", member.Item2));
                
                if (!labelPositionDict.ContainsKey(member.Item1))
                    labelPositionDict.Add(member.Item1, index);
                if (!labelUniqueNameLookupDict.ContainsKey(member.Item1))
                    labelUniqueNameLookupDict.Add(member.Item1, member.Item2);
                z += 1;
                index++;

                if (z > elZCount) break;
            }

            this.ZAxisLabels = temps;
            this.ZAxisFilters = filters;
            this.selectedZFiltersCount = 0;
        }

        /// <summary>
        /// Method which creates and populates the three corner level visual elements (Big spheres), used for level filtering actions.
        /// </summary>
        private void PopulateLevelLabels()
        {
            List<LevelVisual> temporary = new List<LevelVisual>();

            var levelVisual = new LevelVisual(new Point3D(0, elYCount, 0), "X");
            levelVisual.CurrentLevelInfo = CurrentLevelElX;
            levelVisual.LevelNumber = CurrentXLevelNumber;
            temporary.Add(levelVisual);

            levelVisual = new LevelVisual(new Point3D(elXCount, 0, 0), "Y");
            levelVisual.CurrentLevelInfo = CurrentLevelElY;
            levelVisual.LevelNumber = CurrentYLevelNumber;
            temporary.Add(levelVisual);

            levelVisual = new LevelVisual(new Point3D(0, elYCount, elZCount), "Z");
            levelVisual.CurrentLevelInfo = CurrentLevelElZ;
            levelVisual.LevelNumber = CurrentZLevelNumber;
            temporary.Add(levelVisual);

            LevelVisuals = temporary;
        }

        /// <summary>
        /// Main method for populating the data elements in the UI bound collections (renderred on screen).
        /// </summary>
        private void PopulateDataObjects()
        {
            // Create a selection query that will be used to retrieve the data elements. Two options - have filters applied or with no filters.
            SelectMDXObject selectQuery;

            if (filtersToApply != null && filtersToApply.Count > 0)
            {
                selectQuery = new SelectMDXObject(GetMeasures(), GetAxes(),
                                                                  CurrentlySelectedCube.CubeName, filtersToApply.ToList());
            }
            else
            {
               selectQuery = new SelectMDXObject(GetMeasures(), GetAxes(),
                                                                  CurrentlySelectedCube.CubeName);
            }
            
            // Object used for the execution of queries.
            DefaultQueryExecuter queryExecuter = new DefaultQueryExecuter(CurrentlySelectedCube.ParentConnection.Connection);

            // Execute the query in the SelectMDXObject in the queryExecuter. Results are stored in a datatable ready for consumption.
            DataTable queryResults = queryExecuter.ExecuteReader(selectQuery.ToString());

            Create3DDataObjects(queryResults);
        }

        /// <summary>
        /// Method to consume the returned query results and populate the appropriate data element collections.
        /// </summary>
        /// <param name="queryResults"></param>
        private void Create3DDataObjects(DataTable queryResults)
        {
            if (queryResults.Rows.Count == 0)
            {
                DataVisuals = new List<DataVisual>();
                CurrentlySelectedVisual = null;
                VisualIsSelected = false;
                CurrentSelected3DLevelVisual = null;
                LevelVisualIsSelected = false;
                DataIsMapped = true;
                filtersToApply = new List<FilterMDXObject>();
                return;
            }

            int xIndex = -1;
            int yIndex = -1;
            int zIndex = -1;
            int m1Index = -1;
            int m2Index = -1;
            bool twoMeasures = false;

            // Get appropriate indexes to search with in rows.
            for (int index = 0; index < queryResults.Columns.Count; index++)
            {
                if (queryResults.Columns[index].ColumnName == (CurrentLevelElX.UniqueName + ".[MEMBER_CAPTION]"))
                {
                    xIndex = index;
                    continue;
                }

                if (queryResults.Columns[index].ColumnName == (CurrentLevelElY.UniqueName + ".[MEMBER_CAPTION]"))
                {
                    yIndex = index;
                    continue;
                }
                if (queryResults.Columns[index].ColumnName == (CurrentLevelElZ.UniqueName + ".[MEMBER_CAPTION]"))
                {
                    zIndex = index;
                    continue;
                }
                if (SelectedMeasureOne != null && queryResults.Columns[index].ColumnName == SelectedMeasureOne.UniqueName)
                {
                    m1Index = index;
                    continue;
                }
                if (SelectedMeasureTwo != null && queryResults.Columns[index].ColumnName == SelectedMeasureTwo.UniqueName)
                {
                    m2Index = index;
                }
            }

            if (m2Index > 0) twoMeasures = true;

            // Initialize temporary element collection and min and max values.
            IList<DataVisual> temp = new List<DataVisual>();

            decimal measure1 = 0;
            decimal measure2 = 0;
            decimal maxM1 = decimal.MinValue;
            decimal minM1 = decimal.MaxValue;
            decimal maxM2 = decimal.MinValue;
            decimal minM2 = decimal.MaxValue;

            // Find mix and max in the query results.
            foreach (DataRow row in queryResults.Rows)
            {
                measure1 = decimal.Parse(row[m1Index].ToString());
                if (measure1 > maxM1) maxM1 = measure1;
                if (measure1 < minM1) minM1 = measure1;

                if (twoMeasures)
                {
                    measure2 = decimal.Parse(row[m2Index].ToString());
                    if (measure2 > maxM2) maxM2 = measure2;
                    if (measure2 < minM2) minM2 = measure2;
                }
            }

            foreach (DataRow row in queryResults.Rows)
            {
                // Find members and populate their coordinates.
                string xMember = row[xIndex].ToString();
                double xLabelCoordinate = labelPositionDict[xMember];

                string yMember = row[yIndex].ToString();
                double yLabelCoordinate = labelPositionDict[yMember];

                string zMember = row[zIndex].ToString();
                double zLabelCoordinate = labelPositionDict[zMember];

                // Retrieve measure values.
                if (m1Index != -1) measure1 = decimal.Parse(row[m1Index].ToString());

                if(twoMeasures) measure2 = decimal.Parse(row[m2Index].ToString());

                // Create new data visual element members, passing in the coordinate and the two measure values.
                DataVisual currentMember = new DataVisual(new Point3D(xLabelCoordinate + 0.5, yLabelCoordinate + 0.5, zLabelCoordinate + 0.5), measure1, measure2);

                // Applying any measure bounds related to the colour (measure 1) and size (measure 2), since they need to be visualised differently.
                if(twoMeasures) currentMember.ApplyTwoMeasureBounds(maxM1, minM1, maxM2, minM2);
                else
                {
                    currentMember.ApplyMeasureBounds(maxM1, minM1);
                }
                temp.Add(currentMember);
            }

            // Update the collection bound to the UI. Major bottleneck point here due to framework inefficiencies.
            DataVisuals = temp;

            // Formulas to calculate the appropriate colours based on the minimum and maximum values. Converts value to colour.
            var test = ColorHelper.HsvToColor(((((((double) (maxM1 - maxM1)*0.2)/((double) (minM1 - maxM1))*360) + 350)%360)/360),
                                   1, 1);

            var test2 = ColorHelper.HsvToColor(((((((double)(minM1 - maxM1) * 0.2) / ((double)(minM1 - maxM1)) * 360) + 350) % 360) / 360),
                                   1, 1);

            // Create a gradient brush from these two min and max colour values, to be used in the legend. Update all remaining variables and flags.
            LegendColor = BrushHelper.CreateGradientBrush(new List<Color>() {test, test2},false);
            LegendMin = (double)minM1;
            LegendMax = (double)maxM1;
            LegendStep = LegendMax;
            CurrentlySelectedVisual = null;
            VisualIsSelected = false;
            DataIsMapped = true;
            CurrentSelected3DLevelVisual = null;
            LevelVisualIsSelected = false;
            filtersToApply = new List<FilterMDXObject>();
        }

        /// <summary>
        /// Method which is used to convert an object that represents the 2D Treemap into the elements needed to populate the and render the 3D
        /// elements on screen.
        /// </summary>
        /// <param name="objectToConvert">Converter object that represents a 2D Treemap</param>
        private void Convert2Dto3D(ConverterObject2D3D objectToConvert)
        {
            // Selected X,Y,Z dimensions.
            SelectedElX = objectToConvert.XDimension;
            SelectedElY = objectToConvert.YDimension;
            SelectedElZ = objectToConvert.ZDimension;

            // Setting two measures template.
            OneMeasureChecked = false;
            TwoMeasuresChecked = true;

            // Populating the selected measures.
            SelectedMeasureOne = objectToConvert.MeasureOne;
            SelectedMeasureTwo = objectToConvert.MeasureTwo;      
            
            // Retrieving the currently selected levels numbers;
            currentXLevelNumber = objectToConvert.XLevelNumber;
            currentYLevelNumber = objectToConvert.YLevelNumber;
            currentZLevelNumber = objectToConvert.ZLevelNumber;

            // Populating the currently selected levels.
            CurrentLevelElX = selectedElX.Levels[currentXLevelNumber];
            CurrentLevelElY = selectedElY.Levels[currentYLevelNumber];
            CurrentLevelElZ = selectedElZ.Levels[currentZLevelNumber];

            // Populating any filters to be applied.
            filtersToApplyXAxis = objectToConvert.XFilterToApply != null ? new List<FilterMDXObject>() { new FilterMDXObject(new List<string>() { objectToConvert.XFilterToApply }, "Equals") } : new List<FilterMDXObject>();
            filtersToApplyYAxis = objectToConvert.YFilterToApply != null ? new List<FilterMDXObject>() { new FilterMDXObject(new List<string>() { objectToConvert.YFilterToApply }, "Equals") } : new List<FilterMDXObject>();
            filtersToApplyZAxis = objectToConvert.ZFilterToApply != null ? new List<FilterMDXObject>() { new FilterMDXObject(new List<string>() { objectToConvert.ZFilterToApply }, "Equals") } : new List<FilterMDXObject>();

            // Initializing the consolidated filter collection.
            filtersToApply = new List<FilterMDXObject>();
            AddFilterRanges();

            MapAction();
        }
        
        private List<string> GetAxes()
        {
            return new List<string>()
                       {
                           CurrentLevelElX.UniqueName + ".ALLMEMBERS",
                           CurrentLevelElY.UniqueName + ".ALLMEMBERS",
                           CurrentLevelElZ.UniqueName + ".ALLMEMBERS"
                       };
        }

        private List<string> GetMeasures()
        {
            if(SelectedMeasureTwo != null)
            {
                return new List<string>()
                       {
                           SelectedMeasureOne.UniqueName,
                           SelectedMeasureTwo.UniqueName,
                       }; 
            }
            else
            {
                return new List<string>()
                       {
                           SelectedMeasureOne.UniqueName
                       }; 
            }

        }

        #endregion
    }
}