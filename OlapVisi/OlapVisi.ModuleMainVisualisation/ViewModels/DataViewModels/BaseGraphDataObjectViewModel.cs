﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// Abstract class for all base graph data objects.
    /// </summary>
    public abstract class BaseGraphDataObjectViewModel : NotificationObject
    {
        private String name;
        public String Name
        {
            get { return this.name; }
            set
            {
                this.name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        private String uniqueName;
        public String UniqueName
        {
            get { return this.uniqueName; }
            set
            {
                this.uniqueName = value;
                RaisePropertyChanged(() => UniqueName);
            }
        }
    }
}
