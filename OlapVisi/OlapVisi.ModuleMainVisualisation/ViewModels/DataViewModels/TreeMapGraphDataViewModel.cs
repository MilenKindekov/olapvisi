﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.Infrastructure.Tools;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// A representation of a node in the Treemap graph. View Model of the data objects, which populate the TreeMap graph.
    /// Bindings used by the View to operate correctly.
    /// </summary>
    public class TreeMapGraphDataViewModel : BaseGraphDataObjectViewModel
    {
        private readonly EventAggregator eventAggregator;

        public ICommand DrillDown { get; private set; }
        public ICommand Maximize { get; private set; }
        public ICommand Select { get; private set; }

        public TreeMapGraphDataViewModel(String name, string uniqueName, decimal colour, decimal size, String colourType, String sizeType, int levelNumber, bool leafNode, TreeMapGraphDataViewModel parent = null, TreeMapActionController actionController = null)
        {
            Name = name;
            Colour = colour;
            Size = size;
            ColourType = colourType;
            SizeType = sizeType;
            LevelNumber = levelNumber;
            IsNotLeaf = !leafNode;
            Parent = parent;
            UniqueName = uniqueName;
            this.actionController = actionController;

            DrillDown = new DelegateCommand(DrillDownAction);
            Maximize = new DelegateCommand<TreeMapGraphViewModel>(MaximizeAction);
            Select = new DelegateCommand<TreeMapGraphViewModel>(SelectAction);

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();

            if(!leafNode)
                Collection = new List<TreeMapGraphDataViewModel>();
        }

        #region Parameters

        private TreeMapActionController actionController;

        private TreeMapGraphDataViewModel parent;
        public TreeMapGraphDataViewModel Parent
        {
            get { return this.parent; }
            set
            {
                this.parent = value;
                RaisePropertyChanged(() => Parent);
            } 
        }

        private bool isNotLeaf;
        public bool IsNotLeaf
        {
            get { return this.isNotLeaf; }
            set
            {
                this.isNotLeaf = value;
                RaisePropertyChanged(() => IsNotLeaf);
            } 
        }

        private int levelNumber;
        public int LevelNumber
        {
            get { return this.levelNumber; }
            set
            {
                this.levelNumber = value;
                RaisePropertyChanged(() => LevelNumber);
            }
        }


        private decimal colour;
        public decimal Colour
        {
            get { return this.colour; }
            set
            {
                this.colour = value;
                RaisePropertyChanged(() => Colour);
            }
        }

        private decimal size;
        public decimal Size
        {
            get { return this.size; }
            set
            {
                this.size = value;
                RaisePropertyChanged(() => Size);
            }
        }

        private String colourType;
        public String ColourType
        {
            get { return this.colourType; }
            set
            {
                this.colourType = value;
                RaisePropertyChanged(() => ColourType);
            }
        }

        private String sizeType;
        public String SizeType
        {
            get { return this.sizeType; }
            set
            {
                this.sizeType = value;
                RaisePropertyChanged(() => SizeType);
            }
        }

        private bool isRecursed = false;
        public bool IsRecursed
        {
            get { return isRecursed; }
            set
            {
                isRecursed = value;
                RaisePropertyChanged(() => IsRecursed);
                if (isRecursed) actionController.TreeMapItemRecursedAction(this);
            }
        }

        private bool isSelected = false;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        #endregion Parameter

        private List<TreeMapGraphDataViewModel> collection;
        public List<TreeMapGraphDataViewModel> Collection
        {
            get { return this.collection; }
            set
            {
                this.collection = value;
                RaisePropertyChanged(() => Collection);
            }
        }

        private void SelectAction(TreeMapGraphViewModel vm)
        {
            IsSelected = true;
            vm.SelectedItem = this;
        }


        private void DrillDownAction()
        {
            if (this.IsRecursed)
            {
                this.IsRecursed = false;
            }
            else
            {
                this.IsRecursed = true;
            }
        }

        // Maximize and create breadcrumb to display above graph.
        private void MaximizeAction(TreeMapGraphViewModel vm)
        {
            actionController.TreeMapItemMaximizeAction(this);
        }
    }
}
