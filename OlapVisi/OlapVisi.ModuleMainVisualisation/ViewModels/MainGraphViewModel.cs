﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;

namespace OlapVisi.ModuleMainVisualisation.ViewModels
{
    /// <summary>
    /// Class that serves as the main visualisation default screen (the tabular control). Different types of graphs can be added to it.
    /// </summary>
    public class MainGraphViewModel : NotificationObject, IGraphViewModel
    {
        #region Parameters
        private readonly EventAggregator eventAggregator;
        private ObservableCollection<HierarchyInfo> selectedHierarchies;
        private ObservableCollection<HierarchyInfo> selectedTimeHierarchies;
        private ObservableCollection<NamedSetInfo> selectedNamedSets;
        private ObservableCollection<MeasureInfo> selectedMeasures;
        private CubeInfoViewModel currentlySelectedCube;
        private int tabIndex;

        public ICommand RemoveSelectedItem { get; private set; }
        public ICommand SelectGraph{ get; private set; }
        public ICommand RemoveTab{ get; private set; }
        public ICommand NewTabCommand { get; private set; }
        #endregion

        /// <summary>
        /// Subscribe to events related to the cube selection control (left side slide-out form), cube and database changes.
        /// Commands related to the tabular control are initilized as well.
        /// </summary>
        public MainGraphViewModel()
        {
            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<HierarchySelectedEvent>().Subscribe(SelectedHierarchiesChanged);
            eventAggregator.GetEvent<TimeHierarchySelectedEvent>().Subscribe(SelectedTimeHierarchiesChanged);
            eventAggregator.GetEvent<NamedSetSelectedEvent>().Subscribe(SelectedNamedSetsChanged);
            eventAggregator.GetEvent<MeasureSelectedEvent>().Subscribe(SelectedMeasuresChanged);

            eventAggregator.GetEvent<CubeSelectedEvent>().Subscribe(CubeSelected);
            eventAggregator.GetEvent<DatabaseChangedEvent>().Subscribe(DatabasesChanged);

            RemoveSelectedItem = new DelegateCommand(RemoveSelectedItemAction);
            SelectGraph = new DelegateCommand<string>(ReplaceWithSelectedGraph);
            RemoveTab = new DelegateCommand<string>(RemoveRightClickedTab);
            NewTabCommand = new DelegateCommand<object>(NewTabCommandAction);

            tabIndex = 0;
        }


        #region Tab Actions

        /// <summary>
        /// The collection of tabpane items.
        /// </summary>
        ObservableCollection<GraphViewModelBase> tabPaneGraphItems;
        public ObservableCollection<GraphViewModelBase> TabPaneGraphItems
        {
            get
            {
                // Initialize first tab pane item to be of a default type.
                if (tabPaneGraphItems == null)
                {
                    tabIndex++;
                    tabPaneGraphItems = new ObservableCollection<GraphViewModelBase>();
                    TabPaneGraphItems.Add(
                        GraphViewModelFactory.Instance.getGraphViewModel(GraphViewModelTypes.GenericGraph,
                                                                         tabIndex, CurrentlySelectedCube));
                    var itemsView = (IEditableCollectionView)CollectionViewSource.GetDefaultView(tabPaneGraphItems);
                    itemsView.NewItemPlaceholderPosition = NewItemPlaceholderPosition.AtEnd;
                    CurrentlySelected = 0;
                }

                return tabPaneGraphItems;
            }
        }

        /// <summary>
        /// Command to add a new tabpane graph item (default one). Increases the number of tabs by one.
        /// </summary>
        /// <param name="parameter"></param>
        private void NewTabCommandAction(object parameter)
        {
            tabIndex++;
            TabPaneGraphItems.Add(GraphViewModelFactory.Instance.getGraphViewModel(GraphViewModelTypes.GenericGraph,
                                                                                   tabIndex, CurrentlySelectedCube));
        }

        /// <summary>
        /// Right click remove of a tab pane, removes the tabpane and its view model. 
        /// </summary>
        /// <param name="parameter"></param>
        private void RemoveRightClickedTab(string parameter)
        {
            foreach(GraphViewModelBase viewModel in TabPaneGraphItems)
            {
                if (viewModel.ViewName == parameter)
                {               
                    if(TabPaneGraphItems[CurrentlySelected] == viewModel)
                    {
                        CurrentlySelected = 0;                    
                    }
                    TabPaneGraphItems.Remove(viewModel);

                    if (TabPaneGraphItems.Count > 0)
                        CurrentlySelectedItem = TabPaneGraphItems[CurrentlySelected];

                    return;
                }
            }
        }
        
        /// <summary>
        /// Integer of the currently selected tabpane item.
        /// </summary>
        private int currentlySelected;
        public int CurrentlySelected
        {
            get { return this.currentlySelected; }
            set
            {
                if(value >= 0)
                {
                    currentlySelected = value;
                    RaisePropertyChanged(() => CurrentlySelected);
                }  
            } 
        }

        /// <summary>
        /// Helper method to remove the default graph view model and place a new type of graph view model at the specified tab pane.
        /// </summary>
        /// <param name="parameter"></param>
        private void ReplaceWithSelectedGraph(string parameter)
        {
            int currentSelection = CurrentlySelected;
            if (TabPaneGraphItems.Count > 0)
            {
                int elementIndex = TabPaneGraphItems[CurrentlySelected].TabIndex;
                TabPaneGraphItems[CurrentlySelected] = GraphViewModelFactory.Instance.getGraphViewModel(parameter,
                                                                                                        elementIndex, CurrentlySelectedCube);
                CurrentlySelected = currentSelection;
                CurrentlySelectedItem = TabPaneGraphItems[CurrentlySelected];
            }
        }
        #endregion Tab Actions


        #region Cube Selection Side Bar

        /// <summary>
        /// The currently selected graph view model.
        /// </summary>
        private GraphViewModelBase currentlySelectedItem;
        public GraphViewModelBase CurrentlySelectedItem
        {
            get { return this.currentlySelectedItem; }
            set
            {
                    currentlySelectedItem = value;
                    RaisePropertyChanged(() => CurrentlySelectedItem);
            }
        }

        /// <summary>
        /// Currently selected parameter item from the side menu.
        /// </summary>
        private InfoBase selectedCubeParameterItem;
        public InfoBase SelectedCubeParameterItem
        {
            get { return this.selectedCubeParameterItem; }
            set
            {
                selectedCubeParameterItem = value;
                RaisePropertyChanged(() => SelectedCubeParameterItem);
            }
        }

        /// <summary>
        /// The currently selected cube.
        /// </summary>
        public CubeInfoViewModel CurrentlySelectedCube
        {
            get { return this.currentlySelectedCube; }
            set
            {
                this.currentlySelectedCube = value;
                RaisePropertyChanged(() => CurrentlySelectedCube);
            }
        }

        /// <summary>
        /// Action used to remove a selected parameter from the side menu.
        /// </summary>
        private void RemoveSelectedItemAction()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            if (SelectedCubeParameterItem is HierarchyInfo)
            {
                if (SelectedHierarchies.Contains(SelectedCubeParameterItem))
                {
                    cache.RemoveSelectedHierarchy((HierarchyInfo)SelectedCubeParameterItem);
                    SelectedHierarchies.Remove((HierarchyInfo)SelectedCubeParameterItem);
                    if (SelectedHierarchies.Count == 0)
                        SelectedHierarchiesNullOrEmpty = false;
                }
                else if (SelectedTimeHierarchies.Contains(SelectedCubeParameterItem))
                {
                    cache.RemoveSelectedTimeHierarchy((HierarchyInfo)SelectedCubeParameterItem);
                    SelectedTimeHierarchies.Remove((HierarchyInfo)SelectedCubeParameterItem);
                    if (SelectedTimeHierarchies.Count == 0)
                        SelectedTimeHierarchiesNullOrEmpty = false;
                }
            }
            else if (SelectedCubeParameterItem is MeasureInfo)
            {
                cache.RemoveSelectedMeasure((MeasureInfo)SelectedCubeParameterItem);
                SelectedMeasures.Remove((MeasureInfo)SelectedCubeParameterItem);
                if (SelectedMeasures.Count == 0)
                    SelectedMeasuresNullOrEmpty = false;
            }
            else if (SelectedCubeParameterItem is NamedSetInfo)
            {
                cache.RemoveSelectedNamedSet((NamedSetInfo)SelectedCubeParameterItem);
                SelectedNamedSets.Remove((NamedSetInfo)SelectedCubeParameterItem);
            }
        }

        #endregion Cube Selection Side Bar

        #region Observable Collections
        /// <summary>
        /// Region encompasses all collections related to the selected parameters from the cube browser.
        /// These are bound to and displayed on the UI.
        /// </summary>
        #region Selected Hierarchies

        public ObservableCollection<HierarchyInfo> SelectedHierarchies
        {
            get { return selectedHierarchies; }
            set
            {
                selectedHierarchies = value;
                SelectedHierarchiesNullOrEmpty = selectedHierarchies != null;
                RaisePropertyChanged(() => SelectedHierarchies);
            }
        }

        private bool selectedHierarchiesNullOrEmpty;
        public bool SelectedHierarchiesNullOrEmpty
        {
            get { return selectedHierarchiesNullOrEmpty; }
            set
            {
                selectedHierarchiesNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedHierarchiesNullOrEmpty);
            }
        }

        #endregion Selected Hierarchies

        #region Selected Time Hierarchies

        public ObservableCollection<HierarchyInfo> SelectedTimeHierarchies
        {
            get { return selectedTimeHierarchies; }
            set
            {
                selectedTimeHierarchies = value;
                SelectedTimeHierarchiesNullOrEmpty = selectedTimeHierarchies != null;
                RaisePropertyChanged(() => SelectedTimeHierarchies);
            }
        }

        private bool selectedTimeHierarchiesNullOrEmpty;
        public bool SelectedTimeHierarchiesNullOrEmpty
        {
            get { return selectedTimeHierarchiesNullOrEmpty; }
            set
            {
                selectedTimeHierarchiesNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedTimeHierarchiesNullOrEmpty);
            }
        }

        #endregion Selected Time Hierarchies

        #region Selected Named Sets
        public ObservableCollection<NamedSetInfo> SelectedNamedSets
        {
            get { return selectedNamedSets; }
            set
            {
                selectedNamedSets = value;
                RaisePropertyChanged(() => SelectedNamedSets);
            }
        }
        #endregion Selected Named Sets

        #region Selected Measures

        public ObservableCollection<MeasureInfo> SelectedMeasures
        {
            get { return selectedMeasures; }
            set
            {
                selectedMeasures = value;
                SelectedMeasuresNullOrEmpty = selectedMeasures != null;
                RaisePropertyChanged(() => SelectedMeasures);
            }
        }

        private bool selectedMeasuresNullOrEmpty;
        public bool SelectedMeasuresNullOrEmpty
        {
            get { return selectedMeasuresNullOrEmpty; }
            set
            {
                selectedMeasuresNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedMeasuresNullOrEmpty);
            }
        }
        #endregion Selected Measures

        #endregion Observable Collections

        #region Event Change Methods
        /// <summary>
        ///  Action event handlers on selection of different parameters from the cube browser
        /// </summary>
        /// <param name="selectionStatus"></param>

        public void SelectedHierarchiesChanged(bool selectionStatus)
        {
            if (selectionStatus)
            {
                CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
                SelectedHierarchies = new ObservableCollection<HierarchyInfo>(cache.GetSelectedHierarchies());
            }
        }

        public void SelectedTimeHierarchiesChanged(bool selectionStatus)
        {
            if (selectionStatus)
            {
                CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
                SelectedTimeHierarchies = new ObservableCollection<HierarchyInfo>(cache.GetSelectedTimeHierarchies());
            }
        }

        public void SelectedNamedSetsChanged(bool selectionStatus)
        {
            if (selectionStatus)
            {
                CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
                SelectedNamedSets = new ObservableCollection<NamedSetInfo>(cache.GetSelectedNamedSets());
            }
        }

        public void SelectedMeasuresChanged(bool selectionStatus)
        {
            if (selectionStatus)
            {
                CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
                SelectedMeasures = new ObservableCollection<MeasureInfo>(cache.GetSelectedMeasures());
            }
        }

        public void CubeSelected(CubeInfoViewModel cube)
        {
            if(CurrentlySelectedCube != cube)
            {
                CurrentlySelectedCube = cube;
                ClearAllSelections();
            }        
        }

        public void DatabasesChanged(ObservableCollection<ConnectionInfoViewModel> SelectedDatabases)
        {
            if (SelectedDatabases != null && CurrentlySelectedCube != null)
            {
                if (!SelectedDatabases.Contains(CurrentlySelectedCube.ParentConnection))
                {
                    CurrentlySelectedCube = null;
                    ClearAllSelections();
                }
            }
        }

        private void ClearAllSelections()
        {
            CubeSelectionCacheSingleton cache = CubeSelectionCacheSingleton.Instance;
            cache.ClearCurrentSelections();
            SelectedHierarchies = null;
            SelectedTimeHierarchies = null;
            SelectedMeasures = null;
            SelectedNamedSets = null;
        }
        #endregion 
    }
}