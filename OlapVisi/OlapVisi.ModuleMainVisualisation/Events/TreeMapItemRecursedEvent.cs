﻿using Microsoft.Practices.Prism.Events;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation
{
    public class TreeMapItemRecursedEvent : CompositePresentationEvent<TreeMapGraphDataViewModel>
    {
    }
}
