﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlapVisi.ModuleMainVisualisation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Markup;
    using System.Windows.Media.Media3D;
    using System.Windows.Threading;

    [ContentProperty("Content")]
    public class DataTemplate3D : DispatcherObject
    {
        public UIElement3D Content { get; set; }

        public UIElement3D CreateItem(object source)
        {
            var type = this.Content.GetType();
            var types = new List<Type>();
            types.Add(type);
            var current = type;
            while (current.BaseType != null)
            {
                types.Add(current.BaseType);
                current = current.BaseType;
            }

            var visual = (UIElement3D)Activator.CreateInstance(type);
            var boundProperties = new HashSet<string>();
            foreach (var t in types)
            {
                foreach (var fi in t.GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    var dp = fi.GetValue(null) as DependencyProperty;
                    if (dp != null)
                    {
                        var binding = BindingOperations.GetBinding(this.Content, dp);
                        if (binding != null)
                        {
                            boundProperties.Add(dp.Name);
                            BindingOperations.SetBinding(
                                visual, dp, new Binding { Path = binding.Path, Source = source });
                        }
                    }
                }
            }

            return visual;
        }
    }
}
