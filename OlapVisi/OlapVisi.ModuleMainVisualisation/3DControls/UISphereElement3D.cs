﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.Infrastructure.Events;

namespace OlapVisi.ModuleMainVisualisation
{
    public class UISphereElement3D : UIElement3D
    {
	
	        /// <summary>
	        /// Identifies the <see cref="Center"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty CenterProperty = DependencyProperty.Register(
                "Center", typeof(Point3D), typeof(UISphereElement3D), new UIPropertyMetadata(new Point3D()));
	
            /// <summary>
            /// Identifies the <see cref="Material"/> dependency property.
            /// </summary>
            public static readonly DependencyProperty MaterialProperty = DependencyProperty.Register(
                "Material", typeof(Material), typeof(UISphereElement3D), new UIPropertyMetadata(new DiffuseMaterial(System.Windows.Media.Brushes.Blue)));


            /// <summary>
            /// Identifies the <see cref="Material"/> dependency property.
            /// </summary>
            public static readonly DependencyProperty DimensionProperty = DependencyProperty.Register(
                "Dimension", typeof(string), typeof(UISphereElement3D), new UIPropertyMetadata(null));

            /// <summary>
            ///   The visibility property.
            /// </summary>
            public static readonly DependencyProperty VisibleProperty = DependencyProperty.Register(
                "Visible",
                typeof(bool),
                typeof(UISphereElement3D),
                new UIPropertyMetadata(true));

            /// <summary>
            ///   The IsSelected property.
            /// </summary>
            public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
                "IsSelected",
                typeof(bool),
                typeof(UISphereElement3D),
                new UIPropertyMetadata(true, SelectedChanged));

            /// <summary>
            /// Identifies the <see cref="PhiDiv"/> dependency property.
            /// </summary>
            public static readonly DependencyProperty PhiDivProperty = DependencyProperty.Register(
                "PhiDiv", typeof(int), typeof(UISphereElement3D), new PropertyMetadata(30));

            /// <summary>
            /// Identifies the <see cref="Radius"/> dependency property.
            /// </summary>
            public static readonly DependencyProperty RadiusProperty = DependencyProperty.Register(
                "Radius", typeof(double), typeof(UISphereElement3D), new PropertyMetadata(1.0));

            /// <summary>
            /// Identifies the <see cref="ThetaDiv"/> dependency property.
            /// </summary>
            public static readonly DependencyProperty ThetaDivProperty = DependencyProperty.Register(
                "ThetaDiv", typeof(int), typeof(UISphereElement3D), new PropertyMetadata(60));



            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="UIBoxElement"/> is selected.
            /// </summary>
            /// <value>
            ///   <c>true</c> if the element is selected; otherwise, <c>false</c>.
            /// </value>
            public bool IsSelected
            {
                get
                {
                    return (bool)this.GetValue(IsSelectedProperty);
                }

                set
                {
                    this.SetValue(IsSelectedProperty, value);
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="UIBoxElement"/> is visible.
            /// </summary>
            /// <value>
            ///   <c>true</c> if the element is visible; otherwise, <c>false</c>.
            /// </value>
            public bool Visible
            {
                get
                {
                    return (bool)this.GetValue(VisibleProperty);
                }

                set
                {
                    this.SetValue(VisibleProperty, value);
                }
            }

            /// <summary>
	        /// Gets or sets the center of the box.
	        /// </summary>
	        /// <value>The center.</value>
	        public Point3D Center
	        {
	            get
	            {
	                return (Point3D)this.GetValue(CenterProperty);
	            }
	
	            set
	            {
	                this.SetValue(CenterProperty, value);
	            }
	        }
	

        ///<summary>
	        /// Gets or sets the Material of the box.
	        /// </summary>
	        /// <value>The width.</value>
	        public Material Material
	        {
	            get
	            {
	                return (Material)this.GetValue(MaterialProperty);
	            }
	
	            set
	            {
                    this.SetValue(MaterialProperty, value);
	            }
	        }

            ///<summary>
            /// Gets or sets the dimension label of the fikter Element.
            /// </summary>
            /// <value>The dimension label.</value>
            public string Dimension
            {
                get
                {
                    return (string)this.GetValue(DimensionProperty);
                }

                set
                {
                    this.SetValue(DimensionProperty, value);
                }
            }

            /// <summary>
            /// Gets or sets the number of divisions in the phi direction (from "top" to "bottom").
            /// </summary>
            /// <value>The phi div.</value>
            public int PhiDiv
            {
                get
                {
                    return (int)this.GetValue(PhiDivProperty);
                }

                set
                {
                    this.SetValue(PhiDivProperty, value);
                }
            }

            /// <summary>
            /// Gets or sets the radius of the sphere.
            /// </summary>
            /// <value>The radius.</value>
            public double Radius
            {
                get
                {
                    return (double)this.GetValue(RadiusProperty);
                }

                set
                {
                    this.SetValue(RadiusProperty, value);
                }
            }

            /// <summary>
            /// Gets or sets the number of divisions in the theta direction (around the sphere).
            /// </summary>
            /// <value>The theta div.</value>
            public int ThetaDiv
            {
                get
                {
                    return (int)this.GetValue(ThetaDivProperty);
                }

                set
                {
                    this.SetValue(ThetaDivProperty, value);
                }
            }

            protected static void SelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
            {
                ((UISphereElement3D)d).OnUpdateModel();
            } 

            protected override void OnUpdateModel()
            {
                GeometryModel3D gm = new GeometryModel3D();
                gm.Geometry = this.Visible ? this.Tessellate() : null;

                if(this.IsSelected)
                {
                    gm.Material = this.Material; 
                }
                else
                {
                    gm.Material = Materials.Gray;
                }
                
                Visual3DModel = gm;
            }

            /// <summary>
            /// Do the tessellation and return the <see cref="MeshGeometry3D"/>.
            /// </summary>
            /// <returns>A triangular mesh geometry.</returns>
            protected internal MeshGeometry3D Tessellate() 
            {
                var builder = new MeshBuilder(true, true);
                builder.AddSphere(this.Center, this.Radius, this.ThetaDiv, this.PhiDiv);
                return builder.ToMesh();
            }

            protected override void OnMouseDown(MouseButtonEventArgs e)
            {
                base.OnMouseDown(e);
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    EventAggregator eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
                    eventAggregator.GetEvent<FilterVisual3DSelectedEvent>().Publish(new Tuple<Point3D,string>(this.Center, this.Dimension));
                    e.Handled = true;
                }
            }
    }
}
