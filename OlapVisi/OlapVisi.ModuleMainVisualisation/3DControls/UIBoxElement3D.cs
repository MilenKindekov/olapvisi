﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.Infrastructure.Events;

namespace OlapVisi.ModuleMainVisualisation
{
    public class UIBoxElement3D : UIElement3D
    {
	
	        /// <summary>
	        /// Identifies the <see cref="Center"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty CenterProperty = DependencyProperty.Register(
                "Center", typeof(Point3D), typeof(UIBoxElement3D), new UIPropertyMetadata(new Point3D()));
	
	        /// <summary>
	        /// Identifies the <see cref="Height"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty HeightProperty = DependencyProperty.Register(
                "Height", typeof(double), typeof(UIBoxElement3D), new UIPropertyMetadata(1.0));
	
	        /// <summary>
	        /// Identifies the <see cref="Length"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty LengthProperty = DependencyProperty.Register(
                "Length", typeof(double), typeof(UIBoxElement3D), new UIPropertyMetadata(1.0));
	
	        /// <summary>
	        /// Identifies the <see cref="Width"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty WidthProperty = DependencyProperty.Register(
                "Width", typeof(double), typeof(UIBoxElement3D), new UIPropertyMetadata(1.0));

            /// <summary>
            /// Identifies the <see cref="Material"/> dependency property.
            /// </summary>
            public static readonly DependencyProperty MaterialProperty = DependencyProperty.Register(
                "Material", typeof(Material), typeof(UIBoxElement3D), new UIPropertyMetadata(new DiffuseMaterial(System.Windows.Media.Brushes.Blue)));

        /// <summary>
	        /// Identifies the <see cref="BottomFace"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty BottomFaceProperty = DependencyProperty.Register(
                "BottomFace", typeof(bool), typeof(UIBoxElement3D), new UIPropertyMetadata(true));

        /// <summary>
	        /// Identifies the <see cref="TopFace"/> dependency property.
	        /// </summary>
	        public static readonly DependencyProperty TopFaceProperty = DependencyProperty.Register(
                "TopFace", typeof(bool), typeof(UIBoxElement3D), new UIPropertyMetadata(true));

            /// <summary>
            ///   The visibility property.
            /// </summary>
            public static readonly DependencyProperty VisibleProperty = DependencyProperty.Register(
                "Visible",
                typeof(bool),
                typeof(UIBoxElement3D),
                new UIPropertyMetadata(true));

            /// <summary>
            ///   The IsSelected property.
            /// </summary>
            public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
                "IsSelected",
                typeof(bool),
                typeof(UIBoxElement3D),
                new UIPropertyMetadata(true, SelectedChanged));



            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="UIBoxElement"/> is selected.
            /// </summary>
            /// <value>
            ///   <c>true</c> if the element is selected; otherwise, <c>false</c>.
            /// </value>
            public bool IsSelected
            {
                get
                {
                    return (bool)this.GetValue(IsSelectedProperty);
                }

                set
                {
                    this.SetValue(IsSelectedProperty, value);
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="UIBoxElement"/> is visible.
            /// </summary>
            /// <value>
            ///   <c>true</c> if the element is visible; otherwise, <c>false</c>.
            /// </value>
            public bool Visible
            {
                get
                {
                    return (bool)this.GetValue(VisibleProperty);
                }

                set
                {
                    this.SetValue(VisibleProperty, value);
                }
            }

            /// <summary>
	        /// Gets or sets the center of the box.
	        /// </summary>
	        /// <value>The center.</value>
	        public Point3D Center
	        {
	            get
	            {
	                return (Point3D)this.GetValue(CenterProperty);
	            }
	
	            set
	            {
	                this.SetValue(CenterProperty, value);
	            }
	        }
	
	        /// <summary>
	        /// Gets or sets the height (along local z-axis).
	        /// </summary>
	        /// <value>The height.</value>
	        public double Height
	        {
	            get
	            {
	                return (double)this.GetValue(HeightProperty);
	            }
	
	            set
	            {
	                this.SetValue(HeightProperty, value);
	            }
	        }
	
	        /// <summary>
	        /// Gets or sets the length of the box (along local x-axis).
	        /// </summary>
	        /// <value>The length.</value>
	        public double Length
	        {
	            get
	            {
	                return (double)this.GetValue(LengthProperty);
	            }
	
	            set
	            {
	                this.SetValue(LengthProperty, value);
	            }
	        }

            /// <summary>
	        /// Gets or sets the width of the box (along local y-axis).
	        /// </summary>
	        /// <value>The width.</value>
	        public double Width
	        {
	            get
	            {
	                return (double)this.GetValue(WidthProperty);
	            }
	
	            set
	            {
	                this.SetValue(WidthProperty, value);
	            }
	        }

        ///<summary>
	        /// Gets or sets the Material of the box.
	        /// </summary>
	        /// <value>The width.</value>
	        public Material Material
	        {
	            get
	            {
	                return (Material)this.GetValue(MaterialProperty);
	            }
	
	            set
	            {
                    this.SetValue(MaterialProperty, value);
	            }
	        }

        /// <summary>
	        /// Gets or sets a value indicating whether to include the top face.
	        /// </summary>
	        public bool TopFace
	        {
	            get
	            {
	                return (bool)this.GetValue(TopFaceProperty);
	            }
	
	            set
	            {
	                this.SetValue(TopFaceProperty, value);
	            }
	        }

        /// <summary>
	        /// Gets or sets a value indicating whether to include the bottom face.
	        /// </summary>
	        public bool BottomFace
	        {
	            get
	            {
	                return (bool)this.GetValue(BottomFaceProperty);
	            }
	
	            set
	            {
	                this.SetValue(BottomFaceProperty, value);
	            }
	        }

            protected static void SelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
            {
                ((UIBoxElement3D)d).OnUpdateModel();
            } 

            protected override void OnUpdateModel()
            {
                GeometryModel3D gm = new GeometryModel3D();
                gm.Geometry = this.Visible ? this.Tessellate() : null;

                if(this.IsSelected)
                {
                    gm.Material = this.Material; 
                }
                else
                {
                    gm.Material = Materials.Gray;
                }
                
                Visual3DModel = gm;
            }


            /// <summary>
	        /// Do the tesselation and return the <see cref="MeshGeometry3D"/>.
	        /// </summary>
	        /// <returns>The mesh geometry.</returns>
	        internal MeshGeometry3D Tessellate()
	        {
	            var b = new MeshBuilder(false,true);
	            b.AddCubeFace(
	                this.Center, new Vector3D(-1, 0, 0), new Vector3D(0, 0, 1), this.Length, this.Width, this.Height);
	            b.AddCubeFace(
	                this.Center, new Vector3D(1, 0, 0), new Vector3D(0, 0, -1), this.Length, this.Width, this.Height);
	            b.AddCubeFace(
	                this.Center, new Vector3D(0, -1, 0), new Vector3D(0, 0, 1), this.Width, this.Length, this.Height);
	            b.AddCubeFace(
	                this.Center, new Vector3D(0, 1, 0), new Vector3D(0, 0, -1), this.Width, this.Length, this.Height);

                if (this.TopFace)
	            {
	                b.AddCubeFace(
	                    this.Center, new Vector3D(0, 0, 1), new Vector3D(0, -1, 0), this.Height, this.Length, this.Width);
	            }
	
	            if (this.BottomFace)
	            {
	                b.AddCubeFace(
	                    this.Center, new Vector3D(0, 0, -1), new Vector3D(0, 1, 0), this.Height, this.Length, this.Width);
	            }	

	            return b.ToMesh();
	        }


            protected override void OnMouseDown(MouseButtonEventArgs e)
            {
                base.OnMouseDown(e);
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    EventAggregator eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
                    eventAggregator.GetEvent<DataVisual3DSelectedEvent>().Publish(this.Center);
                    e.Handled = true;
                }
            }
    }
}
