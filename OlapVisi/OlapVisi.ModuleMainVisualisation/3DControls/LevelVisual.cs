﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using HelixToolkit.Wpf;
using Microsoft.Practices.Prism.ViewModel;
using OlapVisi.DataLayer.Metadata;

namespace OlapVisi.ModuleMainVisualisation
{
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Represents the large sphere for the three main Dimensions at the corners of the grids.
    /// </summary>
    public class LevelVisual : NotificationObject
    {
        /// <summary>
        /// Initializes the visual, based on the dimension tag - sets its colour to one of the three main colours (reg, green, blue).
        /// </summary>
        /// <param name="position">Position of the visual.</param>
        /// <param name="dimensionLabel">Dimension tag.</param>
        public LevelVisual(Point3D position, string dimensionLabel)
        {
            Position = position;
            Dimension = dimensionLabel;

            Radius = 0.3;
            
            switch(dimensionLabel)
            {
                case("X"):
                    Material = new DiffuseMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EC5959")));
                    break;
                case("Y"):
                    Material = new DiffuseMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#30BA21")));
                    break;
                case("Z"):
                    Material = new DiffuseMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#2A9FEE")));
                    break;
                default:
                    Material = new DiffuseMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#0084ff")));
                    break;
            }
            

            Visible = true;
            IsSelected = true;
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Point3D Position { get; set; }

        public Material Material { get; set; }

        public string Dimension { get; set; }

        public double Radius { get; set; }

        public bool Visible { get; set; }

        public int LevelNumber { get; set; }

        public int LevelMemberCount { get; set; }

        private LevelInfo currentLevelInfo;
        public LevelInfo CurrentLevelInfo
        {
            get { return currentLevelInfo; }
            set
            {
                currentLevelInfo = value;
                LevelMemberCount = (int)currentLevelInfo.MemberCount - 1;
            }
        }

        private bool isSelected;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        public void SwitchSelection()
        {
            if(this.IsSelected)
            {
                IsSelected = false;
            } else
            {
                IsSelected = true;
            }
        }
    }
}
