﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HelixToolkit.Wpf;
using Microsoft.Practices.Prism.ViewModel;

namespace OlapVisi.ModuleMainVisualisation
{
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Represents a 3D element (cubes).
    /// </summary>
    public class DataVisual : NotificationObject
    {

        public DataVisual(Point3D position, decimal measure1, decimal measure2)
        {
            Position = position;
            MeasureOne = measure1;
            MeasureTwo = measure2;

            Visible = true;
            IsSelected = true;
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Point3D Position { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double Length { get; set; }

        public decimal MeasureOne { get; set; }

        public decimal MeasureTwo { get; set; }

        public decimal MaxMeasureOne { get; set; }

        public decimal MinMeasureOne { get; set; }

        public decimal MaxMeasureTwo { get; set; }

        public decimal MinMeasureTwo { get; set; }

        public Material Material { get; set; }

        public bool Visible { get; set; }

        /// <summary>
        /// Property which serves to mark the element as selected. Bound to the UI.
        /// </summary>
        private bool isSelected;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        /// <summary>
        /// Applies material and calculation of size based on the elements measures and the minimum and maximum measures of the full collection.
        /// Logic for two measures selected.
        /// </summary>
        /// <param name="max1"></param>
        /// <param name="min1"></param>
        /// <param name="max2"></param>
        /// <param name="min2"></param>
        public void ApplyTwoMeasureBounds(decimal max1, decimal min1, decimal max2, decimal min2)
        {
            MaxMeasureOne = max1;
            MinMeasureOne = min1;

            AssignMaterial();

            MaxMeasureTwo = max2;
            MinMeasureTwo = min2;

            CalculateSize();
        }

        /// <summary>
        /// One measure selected material (colour) assignment based on the elements measure and the minimum and maximum measures of the full collection.
        /// </summary>
        /// <param name="max1"></param>
        /// <param name="min1"></param>
        public void ApplyMeasureBounds(decimal max1, decimal min1)
        {
            MaxMeasureOne = max1;
            MinMeasureOne = min1;

            AssignMaterial();

            Width = 0.8;
            Height = 0.8;
            Length = 0.8;
        }

        /// <summary>
        /// Method that converts values into a material texture for the 3D element.
        /// </summary>
        private void AssignMaterial()
        {
            Material = MaterialHelper.CreateMaterial(ColorHelper.HsvToColor(  (  (((((double)(MeasureOne - MaxMeasureOne)*0.2)/((double)(MinMeasureOne - MaxMeasureOne))*360) + 350) % 360)/360  ) , 1, 1));
        }

        /// <summary>
        /// Method to calculate the size based on the measures of the full collection.
        /// </summary>
        private void CalculateSize()
        {
            var percent = (double) ((MeasureTwo - MinMeasureTwo)/(MaxMeasureTwo - MinMeasureTwo));
            Width = 0.6 * percent + 0.2;
            Height = 0.6 * percent + 0.2;
            Length = 0.6 * percent + 0.2;
        }
    }
}
