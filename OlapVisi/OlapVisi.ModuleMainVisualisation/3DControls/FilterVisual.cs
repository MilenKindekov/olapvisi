﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using HelixToolkit.Wpf;
using Microsoft.Practices.Prism.ViewModel;

namespace OlapVisi.ModuleMainVisualisation
{
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Represents the small spheres running parallel to the member labels in the 3D graph.
    /// </summary>
    public class FilterVisual : NotificationObject
    {
        /// <summary>
        /// Creates the filter visual.
        /// </summary>
        /// <param name="position">Position of the small sphere.</param>
        /// <param name="dimensionLabel">The label of the dimension it corresponds to.</param>
        /// <param name="uniqueName">The unique name of the member it corresponds to.</param>
        public FilterVisual(Point3D position, string dimensionLabel, string uniqueName)
        {
            Position = position;
            Dimension = dimensionLabel;
            UniqueName = uniqueName;

            Radius = 0.12;
            Material = new DiffuseMaterial(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#0084ff")));

            Visible = true;
            IsSelected = true;
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Point3D Position { get; set; }

        public Material Material { get; set; }

        public string Dimension { get; set; }

        public string UniqueName { get; set; }

        public double Radius { get; set; }

        public bool Visible { get; set; }

        private bool isSelected;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        public void SwitchSelection()
        {
            if(this.IsSelected)
            {
                IsSelected = false;
            } else
            {
                IsSelected = true;
            }
        }
    }
}
