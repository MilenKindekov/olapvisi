﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation.Views
{
    /// <summary>
    /// Interaction logic for GraphView.xaml
    /// </summary>
    public partial class MainGraphView : UserControl, IView
    {
        public MainGraphView(MainGraphViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
            ((INotifyCollectionChanged)HierarchiesBox.Items).CollectionChanged += ListBox_CollectionChanged;
            ((INotifyCollectionChanged)MeasuresBox.Items).CollectionChanged += ListBox_CollectionChanged;
            ((INotifyCollectionChanged)DateTypeHierarchiesBox.Items).CollectionChanged += ListBox_CollectionChanged;
            ((INotifyCollectionChanged)NamedSetsBox.Items).CollectionChanged += ListBox_CollectionChanged;
        }

        #region IView Members

        public IViewModel ViewModel
        {
            get { return (IViewModel)DataContext; }
            set { DataContext = value; }
        }

        #endregion

        private void ListBox_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (((ItemCollection) sender).Count > 0)
            {
                if (flyout.Width.Equals(0)) DraftButton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            }
        }
    }
}
