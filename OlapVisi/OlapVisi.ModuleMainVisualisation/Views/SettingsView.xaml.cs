﻿using System.Windows.Controls;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation.Views
{
    /// <summary>
    /// Interaction logic for Cube3DView.xaml
    /// </summary>
    public partial class SettingsView : UserControl, IView
    {
        public SettingsView(SettingsViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        }

        #region IView Members

        public IViewModel ViewModel
        {
            get { return (IViewModel)DataContext; }
            set { DataContext = value; }
        }

        #endregion
    }
}
