﻿using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation.Views
{
    /// <summary>
    /// Interaction logic for ServerSelectPopupView.xaml
    /// </summary>
    public partial class MemberBrowseView : UserControl
    {
        public MemberBrowseView()
        {
            DataContext = ServiceLocator.Current.GetInstance<MemberBrowseViewModel>();
            InitializeComponent();
        }
    }
}