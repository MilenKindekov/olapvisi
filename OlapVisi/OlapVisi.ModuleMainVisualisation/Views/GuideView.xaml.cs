﻿using System.Windows.Controls;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation.Views
{
    /// <summary>
    /// Interaction logic for GuideView.xaml
    /// </summary>
    public partial class GuideView : UserControl, IView
    {
        public GuideView(GuideViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        }

        #region IView Members

        public IViewModel ViewModel
        {
            get { return (IViewModel)DataContext; }
            set { DataContext = value; }
        }

        #endregion
    }
}
