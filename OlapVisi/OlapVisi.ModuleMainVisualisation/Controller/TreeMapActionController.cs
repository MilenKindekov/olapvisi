﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using OlapVisi.DataLayer.Data;
using OlapVisi.DataLayer.MDX;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Cache;
using OlapVisi.Infrastructure.Events;
using OlapVisi.Infrastructure.MetadataInfoViewModels;
using OlapVisi.ModuleMainVisualisation.ViewModels;

namespace OlapVisi.ModuleMainVisualisation
{
    /// <summary>
    /// Controller class for dealing with the action logic of the 2D treemap graph visualisation.
    /// </summary>
    public class TreeMapActionController
    {
        #region Parameters
        private readonly CubeSelectionCacheSingleton cache;
        private readonly EventAggregator eventAggregator;

        private TreeMapGraphViewModel tmViewModel;
        private CubeInfoViewModel cube;
        private DefaultQueryExecuter queryExecuter;
        private int maxLevelDepth;

        private List<FilterMDXObject> selectedFilters;
        private List<TreeMapGraphDataViewModel> originalCollection;
        private Dictionary<string,TreeMapGraphDataViewModel> dataItemDictionary;

        private decimal colorMin;
        private decimal colorMax;

        #endregion

        /// <summary>
        /// Controller used to process all actions ocurring in the TreeMapGraphViewModel, related to the 2D TreeMap - Drilling (Recursing), Slicing, Plotting etc.
        /// </summary>
        /// <param name="vm"></param>
        public TreeMapActionController(TreeMapGraphViewModel vm)
        {
            tmViewModel = vm;
            cache = CubeSelectionCacheSingleton.Instance;
            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
        }

        /// <summary>
        /// Method used to plot initial values of 2D TreeMap. Initializes all variables to be used for all later interactions.
        /// Pulls in data to plot highest level of the TreeMap, passes in the created DataTable to a method which consumes this data.
        /// </summary>
        public void PlotInitialGraph()
        {
            colorMin = decimal.MaxValue;
            colorMax = decimal.MinValue;

            if (tmViewModel.SelectedHierarchy.Levels[0].Caption == "(All)")
                maxLevelDepth = tmViewModel.SelectedHierarchy.Levels.Count - 1;
            else
            {
                maxLevelDepth = tmViewModel.SelectedHierarchy.Levels.Count;
            }
            
            cube = tmViewModel.CurrentlySelectedCube;

            originalCollection = new List<TreeMapGraphDataViewModel>();
            selectedFilters = new List<FilterMDXObject>();
            dataItemDictionary = new Dictionary<string, TreeMapGraphDataViewModel>();

            SelectMDXObject selectQuery = new SelectMDXObject(GetMeasures(), GetInitialAxes(),
                                                                  cube.CubeName);        
            queryExecuter = new DefaultQueryExecuter(cube.ParentConnection.Connection);
            DataTable queryResults = queryExecuter.ExecuteReader(selectQuery.ToString());
            
            CreateTreeMapGraphDataObject(queryResults, 0);
        }

        /// <summary>
        /// Method which converts the data of the 2D treemap into a converter object passed on to be used by the 3D graph visualisation algorithm and view model.
        /// Two events are published - one sending the converter object across, another notifying the module main visualisation controller of a change in views.
        /// </summary>
        public void TreeMapConvertTo3DAction()
        {
            cache.Add3DHierarchyInfo(tmViewModel.SelectedHierarchy);
            cache.Add3DHierarchyInfo(tmViewModel.SelectedDate);
            cache.Add3DHierarchyInfo(tmViewModel.SelectedHierarchyFilter);
            cache.Add3DMeasureInfo(tmViewModel.SelectedColor);
            cache.Add3DMeasureInfo(tmViewModel.SelectedSize);

            eventAggregator.GetEvent<Hierarchy3DSelectedEvent>().Publish(true);
            eventAggregator.GetEvent<Measure3DSelectedEvent>().Publish(true);

            /**
             * Levels are set by setting the level number.
             * TreeMap has either a selected level for filter elements (Y & Z), or a level number for the currently maximised item (sliced) (X).
             * 
             * (X)
             * Top-level hierarchy: tmViewModel.SelectedHierarchy;
             * Current Level: ?
             * -if tmViewModel.MaximisedItem != null
             * Unique Name for filter: tmViewModel.MaximisedItem.UniqueName; (sliced data if any)
             * Level Number for filter: tmViewModel.MaximisedItem.LevelNumber;
             * -else
             * Use top level, Level Number = 1;
             * 
             * (Y)
             * Top-level hierarchy: tmViewModel.SelectedHierarchyFilter;
             * Current Level: tmViewModel.SelectedHierarchyFilterLevel;
             * Unique Name for filter: tmViewModel.SelectedHierarchyFilterLevel.UniqueName; (sliced data if any)
             * Level Number for filter: ? tmViewModel.SelectedHierarchyFilterLevel.LevelNumber; (from model directly)
             * 
             * (Z)
             * Top-level hierarchy: tmViewModel.SelectedDate;
             * Current Level: tmViewModel.SelectedDateLevel;
             * Unique Name for filter: tmViewModel.SelectedDateLevel.UniqueName; (sliced data if any)
             * Level Number for filter: ? tmViewModel.SelectedDateLevel.LevelNumber (from model directly)
             * 
             * (M1 & M2)
             * tmViewModel.SelectedColor;
             * tmViewModel.SelectedSize;
             */

            ConverterObject2D3D objectToConvert = new ConverterObject2D3D();

            objectToConvert.XDimension = tmViewModel.SelectedHierarchy;
            objectToConvert.YDimension = tmViewModel.SelectedHierarchyFilter;
            objectToConvert.ZDimension = tmViewModel.SelectedDate;
            objectToConvert.MeasureOne = tmViewModel.SelectedColor;
            objectToConvert.MeasureTwo = tmViewModel.SelectedSize;

            if (tmViewModel.MaximisedItem != null)
            {
                objectToConvert.XLevelNumber = tmViewModel.MaximisedItem.LevelNumber + 2;
                objectToConvert.XFilterToApply = tmViewModel.MaximisedItem.UniqueName;
            }

            if (tmViewModel.SelectedHierarchyFilterLevel != null)
            {
                objectToConvert.YLevelNumber = tmViewModel.SelectedHierarchyFilterLevel.LevelNumber;
                objectToConvert.YFilterToApply = tmViewModel.CurrentHierarchyFilterUniqueName;
            }

            if (tmViewModel.SelectedDateLevel != null)
            {
                objectToConvert.ZLevelNumber = tmViewModel.SelectedDateLevel.LevelNumber;
                objectToConvert.ZFilterToApply = tmViewModel.CurrentHierarchyFilterUniqueName;
            }

            eventAggregator.GetEvent<Convert2D3DEvent>().Publish(objectToConvert);
            eventAggregator.GetEvent<MainViewSelectedEvent>().Publish("Cube3DView");
        }

        /// <summary>
        /// 
        /// </summary>
        public void TreeMapItemLevelUpAction()
        {
            TreeMapGraphDataViewModel currentParent = tmViewModel.Collection.First().Parent;

            if (currentParent.Parent != null)
                TreeMapItemMaximizeAction(currentParent.Parent);
            else
            {
                tmViewModel.MaximisedItem = null;
                tmViewModel.BreadCrumb = tmViewModel.SelectedHierarchy.Caption;
                tmViewModel.LevelUpEnabled = false;
                tmViewModel.Collection = originalCollection;
            }
        }

        /// <summary>
        /// Method that handles a recursion event (drill-down). A specific filter for the current element being drilled-down needs to be applied.
        /// (This way only the required data gets pulled, as part of the ad hoc query process of the graph). The results are populated in a datatable.
        /// They are passed on to be consumed to the CreateTreeMapGraphDataObject method.
        /// </summary>
        /// <param name="data">The data view model being drilled-down into.</param>
        public void TreeMapItemRecursedAction(TreeMapGraphDataViewModel data)
        { 
            var memberFilterString = data.UniqueName;
            data.Collection.Clear();

            FilterMDXObject filterQuery = new FilterMDXObject(memberFilterString, MDXFilterTypes.Equal);
            // The data levelnumber needs to be +2, since the counting is offset by 1, as well as a need to pull the next level of the hierarchy.
            SelectMDXObject selectQuery = new SelectMDXObject(GetMeasures(), GetSpecificAxes(data.LevelNumber + 2),
                                                                  cube.CubeName, new List<FilterMDXObject>(tmViewModel.Filters.ToList()) { filterQuery });
            queryExecuter = new DefaultQueryExecuter(cube.ParentConnection.Connection);
            DataTable queryResults = queryExecuter.ExecuteReader(selectQuery.ToString());

            CreateTreeMapGraphDataObject(queryResults, data.LevelNumber + 1);
        }


        /// <summary>
        /// Method which serves to maximize a selected graph data element. It updates the breadcrumb at the top of the graph and updates
        /// the main bindable collection which appears on the view.
        /// </summary>
        /// <param name="data"></param>
        public void TreeMapItemMaximizeAction(TreeMapGraphDataViewModel data)
        {
            TreeMapGraphDataViewModel temp = data;

            if(temp.IsNotLeaf)
            {
                string BreadCrumb = "";

                // Use a stack to find the way up to the top level, pop off the stack to generate the breadcrumb.
                Stack<string> breadcrumbStack = new Stack<string>();

                while (temp != null)
                {
                    breadcrumbStack.Push(temp.Name);
                    temp = temp.Parent;
                }

                BreadCrumb = tmViewModel.SelectedHierarchy.Caption;
                while (breadcrumbStack.Count > 0)
                {
                    BreadCrumb += " / " + breadcrumbStack.Pop();
                }

                tmViewModel.LevelUpEnabled = true;
                tmViewModel.BreadCrumb = BreadCrumb;
                tmViewModel.MaximisedItem = data;

                if(!data.IsRecursed) data.IsRecursed = true;

                tmViewModel.Collection = data.Collection;
            }
           
        }

        /// <summary>
        /// Method which serves to filter the 2D treemap graph on a list of specified filter objects.
        /// </summary>
        public void TreeMapFilterAction(IList<FilterMDXObject> filters)
        {
            if (filters == null) selectedFilters = null;
            else
            {
                selectedFilters = filters.ToList();
            }
            
            originalCollection = new List<TreeMapGraphDataViewModel>();

            int levelNumber = 0;

            // A new select query is created and executed to pull and create the top level data based on the filters passed in.
            SelectMDXObject selectQuery = new SelectMDXObject(GetMeasures(), GetSpecificAxes(levelNumber),
                                                                  cube.CubeName, selectedFilters);
            queryExecuter = new DefaultQueryExecuter(cube.ParentConnection.Connection);
            DataTable queryResults = queryExecuter.ExecuteReader(selectQuery.ToString());

            CreateTreeMapGraphDataObject(queryResults, levelNumber);

            if (queryResults.Rows.Count == 0)
            {
                tmViewModel.MaximisedItem = null;
                tmViewModel.LevelUpEnabled = false;
            }

            // Since some items might not exist after the filter has been applied, a pass through all the items held in the item dictionary is done.
            // If an item is recursed, it is added to a queue, each item's parent is set to null removing the hierarchical structure of the data, to be redone at a second pass.
            // This saves any state (in terms of recursed nodes)
            var dataItemQueue = new ConcurrentQueue<TreeMapGraphDataViewModel>();
            foreach(TreeMapGraphDataViewModel item in dataItemDictionary.Values)
            {
                if(item.IsRecursed)
                {
                    dataItemQueue.Enqueue(item);
                    item.Collection = new List<TreeMapGraphDataViewModel>();
                }
                item.Parent = null;
            }

            TreeMapGraphDataViewModel queuedItem;

            // All nodes marked as recursed are passed to the recursed item method.
            while (dataItemQueue.TryDequeue(out queuedItem))
            {
                TreeMapItemRecursedAction(queuedItem);         
            }

            // A check to see if the previously maximised item is still part of the new data - hence needs to be maximised again.
            // If it isn't a view of the top level of the treemap is what the user sees.
            if(tmViewModel.MaximisedItem != null && ((tmViewModel.MaximisedItem.Parent != null) || (tmViewModel.MaximisedItem.Parent == null && tmViewModel.MaximisedItem.LevelNumber == 0)))
                TreeMapItemMaximizeAction(tmViewModel.MaximisedItem);
        }

        /// <summary>
        /// Creates the data objects used by the treemap graph based on a datatable result set and level number specified.
        /// </summary>
        /// <param name="queryResults"></param>
        /// <param name="levelNumber"></param>
        private void CreateTreeMapGraphDataObject(DataTable queryResults, int levelNumber)
        {
            // Checks if there are any results from the query.
            if (queryResults.Rows.Count != 0)
            {
                // Modifier used to retrieve the correct column indexes of the data needed to be created.
                // When a lower level data is pulled, all previous level labels are included in the result set as well.
                // e.g. Beverly Hills (county in California, which is in USA) - result set will be:
                // USA | USA_UniqueName | California | California_UniqueName | BeverlyHills | BeverlyHills_UniqueName | Value1 | Value2
                // level needed is 2 (0 is top level, California - 1, BeverlyHills - 2), so modifier will be 4.
                int modifiedLevelNumber = levelNumber * 2;
                int modifiedMaxLevelDepth = maxLevelDepth * 2;

                string colorType = tmViewModel.SelectedColor.GetPropertyValue("DEFAULT_FORMAT_STRING").ToString();
                string sizeType = tmViewModel.SelectedSize.GetPropertyValue("DEFAULT_FORMAT_STRING").ToString();

                int colorColumnIndex = 0;
                int sizeColumnIndex = 0;
                // Find correct value indices.
                for (int index = queryResults.Columns.Count - 1; index > queryResults.Columns.Count - 3; index--)
                {
                    if (queryResults.Columns[index].ColumnName == tmViewModel.SelectedColor.UniqueName) colorColumnIndex = index;
                    if (queryResults.Columns[index].ColumnName == tmViewModel.SelectedSize.UniqueName) sizeColumnIndex = index;
                }

                // Loop through rows of data set, creating or updating the data objects of the treemap.
                foreach (DataRow row in queryResults.Rows)
                {
                    // Retrieve measure values.
                    decimal colorTemp = decimal.Parse(row[colorColumnIndex].ToString());
                    decimal sizeTemp = decimal.Parse(row[sizeColumnIndex].ToString());

                    TreeMapGraphDataViewModel data;

                    // Check if item to be added already exists (use the unique name).
                    if (dataItemDictionary.ContainsKey(row[modifiedLevelNumber + 1].ToString()))
                    {
                        // If it eists, update values and based on its level number call a method to add it into the correct place of the collection.
                        data = dataItemDictionary[row[modifiedLevelNumber + 1].ToString()];
                        data.Colour = colorTemp;
                        data.Size = sizeTemp;
                        if (levelNumber == 0)
                            UpdateCollection(data);
                        else
                        {
                            UpdateCollection(data, row[modifiedLevelNumber - 1].ToString());
                        }
                    }
                    else
                    {
                        // If it doesn't exist, check if last level of hierarchy and create an object which is either a leaf or it isnt.
                        if (modifiedLevelNumber + 2 < modifiedMaxLevelDepth)
                        {
                            data = new TreeMapGraphDataViewModel(row[modifiedLevelNumber].ToString(), row[modifiedLevelNumber + 1].ToString(), colorTemp,
                                                        sizeTemp, colorType, sizeType, levelNumber, false, null, this);
                        }
                        else
                        {
                            data = new TreeMapGraphDataViewModel(row[modifiedLevelNumber].ToString(), row[modifiedLevelNumber + 1].ToString(), colorTemp,
                                                        sizeTemp, colorType, sizeType, levelNumber, true, null, this);
                        }

                        // Update the collection based on the level number of the item.
                        if (levelNumber > 0)
                        {
                            dataItemDictionary.Add(row[modifiedLevelNumber + 1].ToString(), data);
                            UpdateCollection(data, row[modifiedLevelNumber - 1].ToString());
                        }
                        else
                        {
                            dataItemDictionary.Add(row[modifiedLevelNumber + 1].ToString(), data);
                            UpdateCollection(data);
                        }
                    }

                    // Update Min & Max values.
                    if (colorTemp < colorMin)
                        colorMin = decimal.Parse(row[colorColumnIndex].ToString());
                    if (colorTemp > colorMax)
                        colorMax = decimal.Parse(row[colorColumnIndex].ToString());
                }
            }

            // In the case of the top level being populated, update the collection consumed by the UI and the breadcrumb string.
            if(levelNumber == 0)
            {
                tmViewModel.Collection = originalCollection;
                tmViewModel.BreadCrumb = tmViewModel.SelectedHierarchy.Caption;
            }
            
            // Set the colours used by the treemap graph.
            tmViewModel.ColorMin = colorMin;
            tmViewModel.ColorMax = colorMax;
            tmViewModel.ColorLogMin = (decimal)Math.Log((double)colorMin);
            tmViewModel.ColorLogMax = (decimal)Math.Log((double)colorMax);
        }

        /// <summary>
        /// Method which updates the collection bound to the UI. Elements are appropriately allocated to their respective places in the hierarchy.
        /// </summary>
        /// <param name="data"></param>
        private void UpdateCollection(TreeMapGraphDataViewModel data, string parentCaption = null)
        {
            if(parentCaption == null)
            {
                originalCollection.Add(data);
            }
            else
            {
                TreeMapGraphDataViewModel parent = dataItemDictionary[parentCaption];
                data.Parent = parent;
                parent.Collection.Add(data);
            }
        }

        /// <summary>
        /// Method to retrieve the unique names of the selected measures by the user.
        /// </summary>
        /// <returns></returns>
        private List<string> GetMeasures()
        {
            return new List<string>()
                       {
                           tmViewModel.SelectedColor.UniqueName,
                           tmViewModel.SelectedSize.UniqueName
                       };
        }

        /// <summary>
        /// Method to retrieve the initial axis of the selected hierarchy by the user.
        /// </summary>
        /// <returns></returns>
        private List<string> GetInitialAxes()
        {
            return new List<string>()
                       {
                           GetAxisLevelName(tmViewModel.SelectedHierarchy, 0) + ".ALLMEMBERS"
                       };
        }

        /// <summary>
        /// Method to retrieve the unique name of an axis of a specific level number of the selected hierarchy by the user.
        /// </summary>
        /// <param name="levelNumber"></param>
        /// <returns></returns>
        private List<string> GetSpecificAxes(int levelNumber)
        {
            return new List<string>()
                       {
                           GetAxisLevelName(tmViewModel.SelectedHierarchy, levelNumber) + ".ALLMEMBERS"
                       };
        }

        /// <summary>
        /// Method to retrieve the unique name of the level of the axis of a specific hierarchy and level number.
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private string GetAxisLevelName(HierarchyInfo hierarchy, int level)
        {
            if (hierarchy.Levels[level].Caption != "(All)")
                return hierarchy.Levels[level].UniqueName;
            else
            {
                return hierarchy.Levels[level + 1].UniqueName;
            }
        }


    }
}
