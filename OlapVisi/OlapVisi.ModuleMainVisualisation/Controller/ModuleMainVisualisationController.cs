﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using OlapVisi.Infrastructure;
using OlapVisi.Infrastructure.Events;
using OlapVisi.ModuleMainVisualisation.Views;

namespace OlapVisi.ModuleMainVisualisation
{
    /// <summary>
    /// A class that controls which view model to be shown on the main visualisation screen. 2D or 3D. (or others).
    /// </summary>
    public class ModuleMainVisualisationController
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _manager;
        private readonly EventAggregator eventAggregator;

        /// <summary>
        /// Initializes the container, region manager and event aggregator, subscribing to an event notification related to the change of view model.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="manager"></param>
        public ModuleMainVisualisationController(IUnityContainer container, IRegionManager manager)
        {
            _container = container;
            _manager = manager;

            eventAggregator = ServiceLocator.Current.GetInstance<EventAggregator>();
            eventAggregator.GetEvent<MainViewSelectedEvent>().Subscribe(SelectedView, true);
        }

        private void SelectedView(string viewName)
        {
            DisplayView(viewName);
        }

        /// <summary>
        /// Method retrieves the main region view based on the specified name. The selected view is activated.
        /// </summary>
        /// <param name="viewName"></param>
        private void DisplayView(string viewName)
        {
            object view;
            IRegion mainRegion = _manager.Regions[RegionNames.MainRegion];

            view = mainRegion.GetView(viewName);

            // Activate the view.
            mainRegion.Activate(view);
        }
    }
}
