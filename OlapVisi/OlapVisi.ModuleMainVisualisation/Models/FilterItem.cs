﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using OlapVisi.DataLayer.MDX;
using OlapVisi.DataLayer.Metadata;
using OlapVisi.Infrastructure.Tools;

namespace OlapVisi.ModuleMainVisualisation.Models
{
    public class FilterItem : NotificationObject
    {
        public FilterItem()
        {
            SelectedFilterHierarchyNotNullOrEmpty = false;
            memberOne = "test";
        }

        private string memberOne;
        public string MemberOne
        {
            get { return this.memberOne; }
            set
            {
                this.memberOne = value;
                RaisePropertyChanged(() => MemberOne);
            }
        }
        private string memberTwo;
        public string MemberTwo
        {
            get { return this.memberTwo; }
            set
            {
                this.memberTwo = value;
                RaisePropertyChanged(() => MemberTwo);
            }
        }

        private string selectedFilterOperator;
        public string SelectedFilterOperator
        {
            get { return this.selectedFilterOperator; }
            set
            {
                this.selectedFilterOperator = value;
                RaisePropertyChanged(() => SelectedFilterOperator);
            }
        }

        private HierarchyInfo selectedFilterHierarchy;
        public HierarchyInfo SelectedFilterHierarchy
        {
            get { return this.selectedFilterHierarchy; }
            set
            {
                this.selectedFilterHierarchy = value;
                RaisePropertyChanged(() => SelectedFilterHierarchy);
                SelectedFilterHierarchyNotNullOrEmpty = true;
                UpdateLevelCollection();
            }
        }

        private List<LevelInfo> levelList;
        public List<LevelInfo> LevelList
        {
            get { return this.levelList; }
            set
            {
                this.levelList = value;
                RaisePropertyChanged(() => LevelList);
            }
        }

        private List<string> operatorList;
        public List<string> OperatorList
        {
            get { return this.operatorList; }
            set
            {
                this.operatorList = value;
                RaisePropertyChanged(() => OperatorList);
            }
        }

        private LevelInfo selectedFilterLevel;
        public LevelInfo SelectedFilterLevel
        {
            get { return this.selectedFilterLevel; }
            set
            {
                this.selectedFilterLevel = value;
                RaisePropertyChanged(() => SelectedFilterLevel);
                SelectedFilterLevelNotNullOrEmpty = true;
                UpdateOperatorCollection();
            }
        }

        private bool selectedFilterLevelNotNullOrEmpty;
        public bool SelectedFilterLevelNotNullOrEmpty
        {
            get { return this.selectedFilterLevelNotNullOrEmpty; }
            set
            {
                this.selectedFilterLevelNotNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedFilterLevelNotNullOrEmpty);
            }
        }

        private bool selectedFilterHierarchyNotNullOrEmpty;
        public bool SelectedFilterHierarchyNotNullOrEmpty
        {
            get { return this.selectedFilterHierarchyNotNullOrEmpty; }
            set
            {
                this.selectedFilterHierarchyNotNullOrEmpty = value;
                RaisePropertyChanged(() => SelectedFilterHierarchyNotNullOrEmpty);
            }
        }

        private FilterMDXObject filterObject;
        public FilterMDXObject FilterObject
        {
            get { return this.filterObject; }
        }

        private void UpdateLevelCollection()
        {
            List<LevelInfo> temporaryCollection = new List<LevelInfo>(SelectedFilterHierarchy.Levels);
            temporaryCollection.Remove(SelectedFilterHierarchy.Levels.FirstOrDefault());
            LevelList = temporaryCollection;
        }

        private void UpdateOperatorCollection()
        {
            OperatorList = MDXFilterTypes.GetMDXFilterTypesAsList().ToList();
        }

    }
}
