﻿using System;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using OlapVisi.Infrastructure;
using OlapVisi.ModuleMainVisualisation.ViewModels;
using OlapVisi.ModuleMainVisualisation.Views;

namespace OlapVisi.ModuleMainVisualisation
{
    [Module(ModuleName = "ModuleMainVisualisation")]
    [ModuleDependency("ModuleCubeNavigation")]
    [ModuleDependency("ModuleToolBar")]
    public class ModuleMainVisualisation : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _manager;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ModuleMainVisualisation(IUnityContainer container, IRegionManager manager)
        {
            _container = container;
            _manager = manager;
        }

        #region IModule Members

        public void Initialize()
        {
            _container.RegisterType<object,MainGraphView>("MainGraphView");
            _container.RegisterType<object,GuideView>("GuideView");
            _container.RegisterType<object,Cube3DView>("Cube3DView");
            _container.RegisterType<object, SettingsView>("SettingsView");

            object view;
           
            IRegion mainRegion = _manager.Regions[RegionNames.MainRegion];

            _manager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(GuideView));
            view = _container.Resolve<GuideView>();
            mainRegion.Add(view, "GuideView");

            _manager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(SettingsView));
            view = _container.Resolve<SettingsView>();
            mainRegion.Add(view, "SettingsView");

            _manager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(Cube3DView));
            view = _container.Resolve<Cube3DView>();
            mainRegion.Add(view, "Cube3DView");

            _manager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(MainGraphView));
            view = _container.Resolve<MainGraphView>();
            mainRegion.Add(view, "MainGraphView");

            mainRegion.Activate(view);

            log.Debug("Module Main Visualisation Loaded");

            var ViewController = new ModuleMainVisualisationController(_container, _manager);

            log.Debug("Module Main Visualisation View Controller Loaded");
        }

        #endregion
    }
}