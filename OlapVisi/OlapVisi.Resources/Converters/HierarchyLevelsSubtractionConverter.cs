﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace OlapVisi.Resources.Converters
{
    /// <summary>
    /// Converter for subtracting 1 from a given value - used for the UI directly.
    /// </summary>
    public class HierarchyLevelsSubstractionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int convertedValue = ((int)value-1);
                return convertedValue.ToString(culture);
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
