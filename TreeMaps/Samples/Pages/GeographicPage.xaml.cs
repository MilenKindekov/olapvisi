using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TreeMaps.Controls;

namespace Samples.Pages
{
  /// <summary>
  /// Interaction logic for GeographicSample.xaml
  /// </summary>

  public partial class GeographicPage : System.Windows.Controls.Page
  {
    public GeographicPage()
    {
      InitializeComponent();

      List<GeographicArea> items = new List<GeographicArea>();

      items.Add(new GeographicArea("France","../Images/Flags/flag_france.png",1035000));
      items.Add(new GeographicArea("United Kingdom","../Images/Flags/flag_great_britain.png",2564000));
      items.Add(new GeographicArea("Italy","../Images/Flags/flag_italy.png",695603));
      items.Add(new GeographicArea("USA", "../Images/Flags/flag_usa.png", 5564000));
      items.Add(new GeographicArea("Germany", "../Images/Flags/flag_germany.png", 1565000));
      items.Add(new GeographicArea("Japan", "../Images/Flags/flag_japan.png", 2564000));
      items.Add(new GeographicArea("Canada", "../Images/Flags/flag_canada.png", 1664000));

      _mode.ItemsSource = Enum.GetValues(typeof(TreeMapAlgo));
      _mode.SelectedItem = _tree.TreeMapMode;
      _mode.SelectionChanged += new SelectionChangedEventHandler(ModeSelectionChanged);
      _tree.ItemsSource = items;

    }

    private void ModeSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      _tree.TreeMapMode = (TreeMapAlgo)_mode.SelectedItem;
    }
  }

  public class GeographicArea
  {
    private ImageSource _image;
    private double _sales;
    private string _name;

    public GeographicArea(string name, string imagePath, double sales)
    {
      _name = name;
      _image = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
      _sales = sales;
    }

    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }
	
    public double Sales
    {
      get { return _sales; }
      set { _sales = value; }
    }
	
    public ImageSource Image
    {
      get { return _image; }
      set { _image = value; }
    }
	
  }
}